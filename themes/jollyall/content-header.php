<?php global $wp_query;
$theme_options = _WSH()->option();
$object = get_queried_object();?>
<section class="post-wrapper-top jt-shadow clearfix">
    <div class="container">
        <div class="col-lg-12">
            
            <h2><?php 
				if( $wp_query->is_post_type_archive && sh_set( $_GET, 'post_type' ) == 'product' ) woocommerce_page_title();
				elseif( is_tax() ) echo $object->name;
				elseif( is_category() ) echo __('Category Archive: ', SH_NAME).$object->name;
				elseif( is_author() ) echo __('Author Archive', SH_NAME);
				elseif( is_archive() ) {
					if ( is_day() ) :
							printf( __( 'Daily Archives: %s', SH_NAME ), get_the_date() );

						elseif ( is_month() ) :
							printf( __( 'Monthly Archives: %s', SH_NAME ), get_the_date( _x( 'F Y', 'monthly archives date format', SH_NAME ) ) );

						elseif ( is_year() ) :
							printf( __( 'Yearly Archives: %s', SH_NAME ), get_the_date( _x( 'Y', 'yearly archives date format', SH_NAME ) ) );

						else :
							_e( 'Archives', SH_NAME );

						endif;
				}
				elseif( $wp_query->is_posts_page ) _e( 'Blog Archives ', SH_NAME );
				else the_title(); 
			?></h2>
            
            <?php echo get_the_breadcrumb(); ?>
        </div>
    </div>
</section>