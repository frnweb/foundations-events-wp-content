<?php

get_header(); 

$meta = _WSH()->get_term_meta( '_sh_category_settings' );

_WSH()->page_settings = $meta; 

$layout = sh_set( $meta, 'layout', 'full' );
$sidebar = sh_set( $meta, 'sidebar', 'blog-sidebar' );
$view = sh_set( $meta, 'view', 'list' );

$view = sh_set( $_GET, 'view' ) ? sh_set( $_GET, 'view' ) : $view;
$classes = ( !$layout || $layout == 'full' ) ? ' col-lg-12 col-md-12' : ' col-lg-9 col-md-9';?>


<?php get_template_part( 'includes/modules/header/header', 'archive' ); ?>

<section class="white-wrapper clearfix">
	<div class="container">
		<div class="module clearfix">
    
    		<div class="row">

				<?php if( $layout == 'left' ): ?>
		
					<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="sidebar">
						<?php dynamic_sidebar( $sidebar ); ?>
					</div>
		
				<?php endif; ?>
		
				<div class="<?php echo $classes; ?> col-sm-12 col-xs-12" id="post-content">
				
					<?php while( have_posts() ): the_post(); ?>
							
												
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                        	
							<?php if( $view == 2 ) get_template_part( 'blog', 'style' );
							else get_template_part( 'blog' ); ?>
							<!-- end blog-item -->
                        </article>
	
					<?php endwhile; ?>
						
					<div class="clearfix"></div>
					
					<?php _the_pagination(); ?>
					
				</div>
        
				<?php if( $layout == 'right' ): ?>
		
					<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="sidebar">        
						<?php dynamic_sidebar( $sidebar ); ?>
					</div>
		
				<?php endif; ?>
			</div>
    	</div>
    </div>
</section>

<?php echo do_shortcode( '[sh_brands_section num=10 order="ASC"]' ); ?>

<?php get_footer(); ?>