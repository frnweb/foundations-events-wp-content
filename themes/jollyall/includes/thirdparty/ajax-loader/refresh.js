function AAPL_reload_code(html) {
	//This file is generated from the admin panel - dont edit here! 
	(function($) {
	 "use strict"
	 	
		
		/** Load Visual Composer scripts */
		vc_twitterBehaviour();
		vc_toggleBehaviour();
		vc_tabsBehaviour();
		vc_accordionBehaviour();
		vc_teaserGrid();
		vc_carouselBehaviour();
		vc_slidersBehaviour();
		vc_prettyPhoto();
		vc_googleplus();
		vc_pinterest();
		vc_progress_bar();
		vc_plugin_flexslider();
		vc_google_fonts();
		/** End of Visual composer scripts */		
		
		// DM Top
		jQuery(window).scroll(function(){
			if (jQuery(this).scrollTop() > 1) {
				jQuery('.dmtop').css({bottom:"25px"});
			} else {
				jQuery('.dmtop').css({bottom:"-100px"});
			}
		});
		jQuery('.dmtop').click(function(){
			jQuery('html, body').animate({scrollTop: '0px'}, 800);
			return false;
		});
		
		// Page Preloader
		$(window).load(function() {
			$(".loader").delay(300).fadeOut();
			$(".animationload").delay(600).fadeOut("slow");
		});
	
		// Count
			function count($this){
			var current = parseInt($this.html(), 10);
			current = current + 1; /* Where 50 is increment */    
			$this.html(++current);
				if(current > $this.data('count')){
					$this.html($this.data('count'));
				} else {    
					setTimeout(function(){count($this)}, 50);
				}
			}            
			$(".stat-count").each(function() {
			  $(this).data('count', parseInt($(this).html(), 10));
			  $(this).html('0');
			  count($(this));
			});
			
	// TOOLTIP
		$('.social-icons, .bs-example-tooltips').tooltip({
		  selector: "[data-toggle=tooltip]",
		  container: "body"
		})
	
	// RIGHT MENU
		/*$("#showbutton").click(function() {
			$('#slidediv').toggle('slide', { direction: 'right' }, 500);
		});
		$("#hidebutton").click(function(){
			$("#slidediv").hide( "slide", { direction: "right"  }, 500 );
		});*/
	
	// HEADER MENU
	   // Add slideup & fadein animation to dropdown
	   $('.dropdown').on('show.bs.dropdown', function(e){
		  var $dropdown = $(this).find('.dropdown-menu');
		  var orig_margin_top = parseInt($dropdown.css('margin-top'));
		  $dropdown.css({'margin-top': (orig_margin_top + 10) + 'px', opacity: 0}).animate({'margin-top': orig_margin_top + 'px', opacity: 1}, 250, function(){
			 $(this).css({'margin-top':''});
		  });
	   });
	   // Add slidedown & fadeout animation to dropdown
	   $('.dropdown').on('hide.bs.dropdown', function(e){
		  var $dropdown = $(this).find('.dropdown-menu');
		  var orig_margin_top = parseInt($dropdown.css('margin-top'));
		  $dropdown.css({'margin-top': orig_margin_top + 'px', opacity: 1, display: 'block'}).animate({'margin-top': (orig_margin_top + 10) + 'px', opacity: 0}, 250, function(){
			 $(this).css({'margin-top':'', display:''});
		  });
	   });
	
		/*$("#header-container").affix({
			offset: {
				top: 100, 
				bottom: function () {
				return (this.bottom = $('#copyrights').outerHeight(true))
				}
			}
		})*/
		
	// Parallax
		$(window).bind('body', function() {
			parallaxInit();
		});
		
		function parallaxInit() {
			$('.fullscreen').parallax("30%", 0.1);
			$('#two-parallax').parallax("30%", 0.1);
			$('#three-parallax').parallax("30%", 0.1);
			$('#four-parallax').parallax("30%", 0.4);
			$('#five-parallax').parallax("30%", 0.4);
			$('#six-parallax').parallax("30%", 0.4);
			$('#seven-parallax').parallax("30%", 0.4);
		}
	
		
	
		
	// Accordion Toggle Items
	   var iconOpen = 'fa fa-minus',
			iconClose = 'fa fa-plus';
	
		$(document).on('show.bs.collapse hide.bs.collapse', '.accordion', function (e) {
			var $target = $(e.target)
			  $target.siblings('.accordion-heading')
			  .find('em').toggleClass(iconOpen + ' ' + iconClose);
			  if(e.type == 'show')
				  $target.prev('.accordion-heading').find('.accordion-toggle').addClass('active');
			  if(e.type == 'hide')
				  $(this).find('.accordion-toggle').not($target).removeClass('active');
		});
		
	
		
	})(jQuery);
	
	
	jQuery(document).ready(function($) {
		"use strict";
		if( $('#rotateme', html).length ) {
			
			try {
			$("#rotateme").rotateme({ 
				text : ['UNBELIVABLE', 'Rocking', 'Fantastic'], 
				interval : 3000  // in miliseconds
			});	
			} catch (e) {
			}
		}
		
		if( $('.flexslider', html).length ) {
			$('.flexslider').flexslider({
				animation: "slide",
				controlNav: false,
				animationLoop: false,
				slideshow: false,
				directionNav: true 
			});
		}
	});
	
	
	if( jQuery('#testimonial-carousel', html).length )
	{
		// Carousel
		(function($) {
		"use strict";
		$(document).ready(function() {
		var owl = $("#testimonial-carousel");
		owl.owlCarousel({
		items : 2, //10 items above 1000px browser width
		itemsDesktop : [1000,2], //5 items between 1000px and 901px
		itemsDesktopSmall : [900,2], // betweem 900px and 601px
		itemsTablet: [600,1], //2 items between 600 and 0
		navigation : false,
		pagination : true,
		});
		 
		// Custom Navigation Events
		$(".next").click(function(){
		owl.trigger('owl.next');
		})
		$(".prev").click(function(){
		owl.trigger('owl.prev');
		})
		});
		})(jQuery);
	}
	
	
	if( jQuery('#clients_carousel', html).length ) {

		// Carousel
		(function($) {
		"use strict";
		$(document).ready(function() {
		var owl = $("#clients_carousel");
		owl.owlCarousel({
		items : 4, //10 items above 1000px browser width
		itemsDesktop : [1000,4], //5 items between 1000px and 901px
		itemsDesktopSmall : [900,2], // betweem 900px and 601px
		itemsTablet: [600,2], //2 items between 600 and 0
		navigation : false,
		pagination : false,
		autoPlay : true
		});  
		});
		})(jQuery);
		
	}
	
	
	if( jQuery('#services-carousel', html).length ) {
		// Carousel
		(function($) {
		"use strict";
		$(document).ready(function() {
		var owl = $("#services-carousel");
		owl.owlCarousel({
		items : 1, //10 items above 1000px browser width
		itemsDesktop : [1000,1], //5 items between 1000px and 901px
		itemsDesktopSmall : [900,1], // betweem 900px and 601px
		itemsTablet: [600,1], //2 items between 600 and 0
		navigation : false,
			 autoPlay : true,
		pagination : true,
		});
		 
		// Custom Navigation Events
		$(".next").click(function(){
		owl.trigger('owl.next');
		})
		$(".prev").click(function(){
		owl.trigger('owl.prev');
		})
		});
		})(jQuery);
	}
	
	
	if( jQuery('#testimonials', html).length ) {
		
		(function($) {
		"use strict";
		$(window).load(function() {
		  // The slider being synced must be initialized first
		  $('#testimonials').flexslider({
			animation: "slide",
			controlNav: false,
			animationLoop: false,
			slideshow: false,
			directionNav: false,    
			asNavFor: '#slider'
		  });
		   
		  $('#slider').flexslider({
			animation: "slide",
			controlNav: false,
			directionNav: false,    
			animationLoop: false,
			slideshow: false,
			sync: "#carousel"
		  });
		});
		})(jQuery);
	}
	
	if( jQuery('#services_carousel', html).length ) {
			// Carousel
			(function($) {
				"use strict";
				$(document).ready(function() {
					var owl = $("#services_carousel");
					try {
					owl.owlCarousel({
						items : 3, //10 items above 1000px browser width
						itemsDesktop : [1000,3], //5 items between 1000px and 901px
						itemsDesktopSmall : [900,3], // betweem 900px and 601px
						itemsTablet: [600,1], //2 items between 600 and 0
						navigation : false,
						pagination : true,
						autoPlay : true
					}); 
					} catch( e ) {
						console.log( 'error' );
					}
				});
			})(jQuery);
	}
	
	
	jQuery(document).ready(function($) {
		$('a[data-gal]', html).each(function() {
			$(this).attr('rel', $(this).data('gal'));
		});
		$("a[data-gal^='prettyPhoto']").prettyPhoto({animationSpeed:'slow',slideshow:false,overlay_gallery: false,theme:'light_square',social_tools:false,deeplinking:false});
	});


	// Portfolio Masonry and isotope
	if( jQuery('.portfolio_wrapper', html ).length ) {
		(function($) {
		"use strict";
		var $container = $('.portfolio_wrapper'),
			$items = $container.find('.portfolio_item'),
			portfolioLayout = 'fitRows';
			
			if( $container.hasClass('portfolio-centered') ) {
				portfolioLayout = 'masonry';
			}
					
			$container.isotope({
				filter: '*',
				animationEngine: 'best-available',
				layoutMode: portfolioLayout,
				animationOptions: {
				duration: 750,
				easing: 'linear',
				queue: false
			},
			masonry: {
			}
			}, refreshWaypoints());
			
			function refreshWaypoints() {
				setTimeout(function() {
				}, 1000);   
			}
					
			$('nav.portfolio-filter ul a').on('click', function() {
					var selector = $(this).attr('data-filter');
					$container.isotope({ filter: selector }, refreshWaypoints());
					$('nav.portfolio-filter ul a').removeClass('active');
					$(this).addClass('active');
					return false;
			});
			
			function getColumnNumber() { 
				var winWidth = $(window).width(), 
				cols = $container.data('cols'),
				columnNumber = 1;
				
				cols = ( cols ) ? cols : 4;
				
				if (winWidth > 1200) {
					columnNumber = cols;
				} else if (winWidth > 950) {
					columnNumber = cols;
				} else if (winWidth > 600) {
					columnNumber = ( cols > 3 ) ? 3 : cols;
				} else if (winWidth > 400) {
					columnNumber = 2;
				} else if (winWidth > 250) {
					columnNumber = 1;
				}
					return columnNumber;
				}       
				
				function setColumns() {
					var winWidth = $(window).width(), 
					columnNumber = getColumnNumber(), 
					itemWidth = Math.floor(winWidth / columnNumber);
					
					$container.find('.portfolio_item').each(function() { 
						$(this).css( { 
						width : itemWidth + 'px' 
					});
				});
			}
			
			function setPortfolio() { 
				setColumns();
				$container.isotope('reLayout');
			}
				
			$container.imagesLoaded(function () { 
				setPortfolio();
			});
			
			$(window).on('resize', function () { 
			setPortfolio();          
		});
	})(jQuery);
	}
	
	
	// jQuery fitvids
	if( jQuery( '.media-object', html ).length ) {
		(function($) {
			"use strict";
			$(document).ready(function(){
				// Target your .container, .wrapper, .post, etc.
				$(".media-object").fitVids();
			});
		})(jQuery);
	}
	
	// Masonry wrapper 
	if( jQuery('.masonry_wrapper', html ).length ) {
		
		(function ($) {
			var $container = $('.masonry_wrapper'),
			cols = $container.data('col'),
			
				colWidth = function () {
					cols = ( cols ) ? cols : 4;
					var w = $container.width(), 
						columnNum = 1,
						columnWidth = 0;
					if (w > 1200) {
						columnNum  = cols;
					} else if (w > 900) {
						columnNum  = ( cols > 4 ) ? 4 : cols;
					} else if (w > 600) {
						columnNum  = 3;
					} else if (w > 300) {
						columnNum  = 2;
					}
					columnWidth = Math.floor(w/columnNum);
					$container.find('.item').each(function() {
						var $item = $(this),
							multiplier_w = $item.attr('class').match(/item-w(\d)/),
							multiplier_h = $item.attr('class').match(/item-h(\d)/),
							width = multiplier_w ? columnWidth*multiplier_w[1]-4 : columnWidth-4,
							height = multiplier_h ? columnWidth*multiplier_h[1]*0.5-4 : columnWidth*0.5-4;
						$item.css({
							width: width,
							height: height
						});
					});
					return columnWidth;
				}
							
				function refreshWaypoints() {
					setTimeout(function() {
					}, 1000);   
				}
							
				$('nav.portfolio-filter ul a').on('click', function() {
					var selector = $(this).attr('data-filter');
					$container.isotope({ filter: selector }, refreshWaypoints());
					$('nav.portfolio-filter ul a').removeClass('active');
					$(this).addClass('active');
					return false;
				});
					
				function setPortfolio() { 
					setColumns();
					$container.isotope('reLayout');
				}
		
				isotope = function () {
					$container.isotope({
						resizable: true,
						itemSelector: '.item',
						masonry: {
							columnWidth: colWidth(),
							gutterWidth: 0
						}
					});
				};
			isotope();
			$(window).smartresize(isotope);
		}(jQuery));
	}

}

function AAPL_click_code(thiss) {
//This file is generated from the admin panel - dont edit here! 
// highlight the current menu item
jQuery('ul.menu li').each(function() {
	jQuery(this).removeClass('current-menu-item');
});
jQuery(thiss).parents('li').addClass('current-menu-item');
}

function AAPL_data_code(dataa) {
//This file is generated from the admin panel - dont edit here! 

}