<?php

//About Us
class SH_About_us extends WP_Widget
{
	
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'SH_Abous_us', /* Name */__('Jolly Abous Us',SH_NAME), array( 'description' => __('Show the information about company', SH_NAME )) );
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		
		echo $before_widget;?>
      
            <?php //echo $before_title.$title.$after_title; ?>
			<div class="widget first">
			
            	<img alt="<?php echo esc_attr( $title ); ?>" src="<?php echo esc_url( $instance['logo'] ); ?>" class="padding-top">
				<br /><br />
				<p><?php echo $instance['content']; ?></p>
			
			</div><!-- end widget -->
			
		<?php
		
		echo $after_widget;
	}
	
	
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

		$instance['title'] = strip_tags($new_instance['title']);
		$instance['logo'] = strip_tags($new_instance['logo']);
		$instance['content'] = $new_instance['content'];
	
	return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$title = ($instance) ? esc_attr($instance['title']) : 'Jollyall';
		$logo = ($instance) ? esc_attr($instance['logo']) : '';
		$content = ($instance) ? esc_attr($instance['content']) : '';?>
        
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', SH_NAME); ?></label>
            <input placeholder="JollyAny" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('logo'); ?>"><?php _e('Logo:', SH_NAME); ?></label>
            <input placeholder="http://jollythemes.com/html/jollyall/images/logo.png" class="widefat" id="<?php echo $this->get_field_id('logo'); ?>" name="<?php echo $this->get_field_name('logo'); ?>" type="text" value="<?php echo esc_attr($logo); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('content'); ?>"><?php _e('Content:', SH_NAME); ?></label>
            <textarea class="widefat" id="<?php echo $this->get_field_id('content'); ?>" name="<?php echo $this->get_field_name('content'); ?>" ><?php echo $content; ?></textarea>
        </p>
               
        <?php 
	}
	
}

// twitter
class SH_Twitter extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'SH_Twitter', /* Name */__('Jolly Tweets',SH_NAME), array( 'description' => __('Grab the latest tweets from twitter', SH_NAME )) );
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		
		echo $before_widget;?>
        
		<?php echo $before_title.$title.$after_title; ?>
		<?php $number = (sh_set($instance, 'number') ) ? esc_attr(sh_set($instance, 'number')) : 2; ?>

		<script type="text/javascript"> jQuery(document).ready(function($) {$('#twitter_update').tweets({screen_name: '<?php echo $instance['twitter_id']; ?>', number: <?php echo $number; ?>});});</script>
		<ul id="twitter_update" class="twitter_feed"></ul>
        
		<?php
		
		echo $after_widget;
	}
	
	
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

		$instance['title'] = strip_tags($new_instance['title']);
		$instance['twitter_id'] = $new_instance['twitter_id'];
		$instance['number'] = $new_instance['number'];

		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$title = ($instance) ? esc_attr($instance['title']) : __('Recent Tweets', SH_NAME);
		$twitter_id = ($instance) ? esc_attr($instance['twitter_id']) : 'wordpress';
		$number = ( $instance ) ? esc_attr($instance['number']) : '';?>
        
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('twitter_id'); ?>"><?php _e('Twitter ID:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('twitter_id'); ?>" name="<?php echo $this->get_field_name('twitter_id'); ?>" type="text" value="<?php echo esc_attr($twitter_id); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('Number of Tweets:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo esc_attr($number); ?>" />
        </p>
        
                
		<?php 
	}
}

//Keep In Touch
class SH_Keep_intouch extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'SH_Keep_intouch', /* Name */__('jolly keep In Touch',SH_NAME), array( 'description' => __('Show the information about the company', SH_NAME )) );
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		
		echo $before_widget;?>
      
            <?php echo $before_title.$title.$after_title; ?>

            <ul class="keep_in_touch clearfix">
                
                <?php if( $instance['address'] ): ?>
                	<li><span><i class="fa fa-map-marker"></i></span> <?php echo $instance['address']; ?> </li>
				<?php endif; ?>
				
				<?php if( $instance['phone'] ): ?>
                	<li><span><i class="fa fa-mobile-phone"></i></span> <?php _e('Phone:', SH_NAME); ?> <?php if(strpos($instance['phone'],"frn_phone")!==false) echo do_shortcode($instance['phone']); else echo $instance['phone']; ?></li>
				<?php endif; ?>
				
				<?php if( $instance['email'] ): ?>
                	<li><span><i class="fa fa-envelope-o"></i></span> <a href="mailto:<?php echo sanitize_email($instance['email']); ?>"><?php echo sanitize_email($instance['email']); ?></a></li>
				<?php endif; ?>
                
			</ul>
			<?php if ( $instance['social_icons'] ):?>
				<div class="social-icons">
					<?php echo sh_get_social_icons();?>
				</div>
			<?php endif; ?>
		<?php
		
		echo $after_widget;
	}
	
	
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

		$instance['title'] = strip_tags($new_instance['title']);
		$instance['address'] = $new_instance['address'];
		$instance['phone'] = $new_instance['phone'];
		$instance['email'] = $new_instance['email'];
		$instance['social_icons'] = $new_instance['social_icons'];
		
		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$title = ($instance) ? esc_attr($instance['title']) : __('Keep in Touch', SH_NAME);
		$address = ($instance) ? esc_attr($instance['address']) : '';
		$phone = ( $instance ) ? esc_attr($instance['phone']) : '';
		$email = ( $instance ) ? esc_attr($instance['email']) : '';
		$social_icons = ( $instance ) ? esc_attr($instance['social_icons']) : '';?>
        
        
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('address'); ?>"><?php _e('Address:', SH_NAME); ?></label>
            <textarea class="widefat" id="<?php echo $this->get_field_id('address'); ?>" name="<?php echo $this->get_field_name('address'); ?>" ><?php echo $address; ?></textarea>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('phone'); ?>"><?php _e('Phone Number:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('phone'); ?>" name="<?php echo $this->get_field_name('phone'); ?>" type="text" value="<?php echo $phone; ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('email'); ?>"><?php _e('Email ID:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('email'); ?>" name="<?php echo $this->get_field_name('email'); ?>" type="text" value="<?php echo $email; ?>" />
        </p>
        
        <p>
			<?php $checked = ($social_icons) ? ' checked="checked"': '';?>
            <label for="<?php echo $this->get_field_id('social_icons'); ?>"><?php _e('Show Social icons:', SH_NAME); ?></label>
            <input class="widefat" <?php echo $checked;?> id="<?php echo $this->get_field_id('social_icons'); ?>" name="<?php echo $this->get_field_name('social_icons'); ?>" type="checkbox" value="true" />
        </p>
       
                
		<?php 
	}
}

/// Posts Tabber
class SH_Tabber extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'SH_Tabber', /* Name */__('Jolly Post Tabs ',SH_NAME), array( 'description' => __('Fetch the latest , Popular posts in tabber form', SH_NAME )) );
	}
 

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$number_of_latest_posts = $instance['number_of_latest_posts'];
		$number_of_popular_posts = $instance['number_of_popular_posts'];
		$number_of_comments_posts = $instance['number_of_comments_posts'];
		
		echo $before_widget;
		
		echo $before_title.$instance['main_title'].$after_title;?>
		
		<div id="tabbed_widget" class="tabbable">

			<ul class="nav nav-tabs">
				<li class="active"><a href="#popular_blog_posts" data-toggle="tab"><?php echo $instance['label1']; ?></a></li>
				<li><a href="#recent_blog_posts" data-toggle="tab"><?php echo $instance['label2']; ?></a></li>
				<li><a href="#comments_post" data-toggle="tab"><?php echo $instance['label3']; ?></a></li>
			</ul>
		
			<div class="tab-content">
			   
				<div class="tab-pane fade in active" id="popular_blog_posts">
					<?php query_posts('meta_key=_sh_post_views&orderby=meta_value&order=DESC&posts_per_page='.$number_of_popular_posts ) ; 
					$this->posts();
					wp_reset_query(); ?>
				</div>
				
				<div class="tab-pane fade" id="recent_blog_posts">
					<?php query_posts('posts_per_page='.$number_of_latest_posts);
					$this->posts();
					wp_reset_query(); ?>
					
				</div>
				 
				<div class="tab-pane fade" id="comments_post">
					<?php query_posts('orderby=comment_count&order=DESC&posts_per_page='.$number_of_latest_posts);
					$this->posts();
					wp_reset_query(); ?>
					
				</div>
				
			</div>
		</div>
		<?php echo $after_widget;
	}
 
 
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;
		
		$instance['main_title'] = $new_instance['main_title'];
		$instance['number_of_latest_posts'] = $new_instance['number_of_latest_posts'];
		$instance['number_of_popular_posts'] = $new_instance['number_of_popular_posts'];
		$instance['number_of_comments_posts'] = $new_instance['number_of_comments_posts'];
		$instance['label1'] = $new_instance['label1'];
		$instance['label2'] = $new_instance['label2'];
		$instance['label3'] = $new_instance['label3'];
		
		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$main_title = ( $instance ) ? esc_attr($instance['main_title']) : __('Post Tabs', SH_NAME);
		$number_of_latest_posts = ( $instance ) ? esc_attr($instance['number_of_latest_posts']) : 4;
		$number_of_popular_posts = ( $instance ) ? esc_attr($instance['number_of_popular_posts']) : 4;
		$number_of_comments_posts = ( $instance ) ? esc_attr($instance['number_of_comments_posts']) : 4;
		$label1 = ( $instance ) ? esc_attr($instance['label1']) : __('Popular', SH_NAME);
		$label2 = ( $instance ) ? esc_attr($instance['label2']) : __('Recent', SH_NAME);
		$label3 = ( $instance ) ? esc_attr($instance['label3']) : __('Comments', SH_NAME);?>
			
        <p>
            <label for="<?php echo $this->get_field_id('main_title'); ?>"><?php _e('Main Title:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('main_title'); ?>" name="<?php echo $this->get_field_name('main_title'); ?>" type="text" value="<?php echo esc_attr($main_title); ?>" />
        </p>
		<p>
            <label for="<?php echo $this->get_field_id('label1'); ?>"><?php _e('Label for Popular Tab:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('label1'); ?>" name="<?php echo $this->get_field_name('label1'); ?>" type="text" value="<?php echo esc_attr($label1); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('number_of_popular_posts'); ?>"><?php _e('No. of Popular Posts:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('number_of_popular_posts'); ?>" name="<?php echo $this->get_field_name('number_of_popular_posts'); ?>" type="text" value="<?php echo esc_attr($number_of_popular_posts); ?>" />
        </p>
        
        <p>
            <label for="<?php echo $this->get_field_id('label2'); ?>"><?php _e('Label for Recent Tab:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('label2'); ?>" name="<?php echo $this->get_field_name('label2'); ?>" type="text" value="<?php echo esc_attr($label2); ?>" />
        </p>
        
        <p>
            <label for="<?php echo $this->get_field_id('number_of_latest_posts'); ?>"><?php _e('No. of Latest Posts:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('number_of_latest_posts'); ?>" name="<?php echo $this->get_field_name('number_of_latest_posts'); ?>" type="text" value="<?php echo esc_attr($number_of_latest_posts); ?>" />
        </p>
		
		<p>
            <label for="<?php echo $this->get_field_id('label3'); ?>"><?php _e('Label for Comments Tab:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('label3'); ?>" name="<?php echo $this->get_field_name('label3'); ?>" type="text" value="<?php echo esc_attr($label3); ?>" />
        </p>
        
        <p>
            <label for="<?php echo $this->get_field_id('number_of_comments_posts'); ?>"><?php _e('No. of Comments Posts:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('number_of_comments_posts'); ?>" name="<?php echo $this->get_field_name('number_of_comments_posts'); ?>" type="text" value="<?php echo esc_attr($number_of_comments_posts); ?>" />
        </p>
        
		<?php 
	}
	
	function posts()
	{
		
		if( have_posts() ):?>
        
            <ul class="recent_posts_widget">
                
                <?php while( have_posts() ): the_post(); ?>
                    
					<li>
						<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
							<?php the_post_thumbnail('600x600'); ?>
                            <?php the_title(); ?>
                        </a>
						<a href="<?php the_permalink(); ?>" class="readmore" title="<?php the_title(); ?>"><?php echo get_the_date(); ?></a>
					   <div class="rating">
							<?php _sh_star_rating( true ); ?>
						</div><!-- rating -->
					</li>
					
                <?php endwhile; ?>
                
            </ul>
            
		<?php endif;
    }
}

/// recent posts
class SH_Recent_Post extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'SH_Recent_Post', /* Name */__('Jolly Recent Posts ',SH_NAME), array( 'description' => __('Show the recent posts with images', SH_NAME )) );
	}
 

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		
		echo $before_widget;
		
		echo $before_title.$title.$after_title; 
		
		$query_string = 'posts_per_page='.$instance['number'];
		if( $instance['cat'] ) $query_string .= '&cat='.$instance['cat'];
		query_posts( $query_string ); 
	
		$this->posts();
		wp_reset_query(); 
		
		echo $after_widget;
	}
 
 
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;
		
		$instance['title'] = $new_instance['title'];
		$instance['number'] = $new_instance['number'];
		$instance['cat'] = $new_instance['cat'];
		
		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$title = ( $instance ) ? esc_attr($instance['title']) : __('Recent Posts', SH_NAME);
		$number = ( $instance ) ? esc_attr($instance['number']) : 4;
		$cat = ( $instance ) ? esc_attr($instance['cat']) : '';?>
			
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title: ', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('No. of Posts:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo $number; ?>" />
        </p>
       
        <p>
            <label for="<?php echo $this->get_field_id('cat'); ?>"><?php _e('Category', SH_NAME); ?></label>
            <?php wp_dropdown_categories( array('show_option_all'=>__('All Categories', SH_NAME), 'selected'=>$cat, 'class'=>'widefat', 'name'=>$this->get_field_name('cat')) ); ?>
        </p>
        
		<?php 
	}
	
	function posts()
	{
		
		if( have_posts() ):?>
        
            <ul class="footer_post">
                
                <?php while( have_posts() ): the_post(); ?>
                    <li>
                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
							<?php the_post_thumbnail('81x69'); ?>
                        </a>
                    </li>
                <?php endwhile; ?>
                
            </ul>
            
		<?php endif;
    }
}

/// recent posts
class SH_Recent_Post_rating extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'SH_Recent_Post_rating', /* Name */__('Jolly Recent Posts Rating',SH_NAME), array( 'description' => __('Show the recent posts with images and rating', SH_NAME )) );
	}
 

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		
		echo $before_widget;
		
		echo $before_title.$title.$after_title; 
		
		$query_string = 'posts_per_page='.$instance['number'];
		if( $instance['cat'] ) $query_string .= '&cat='.$instance['cat'];
		query_posts( $query_string ); 
	
		$this->posts();
		wp_reset_query(); 
		
		echo $after_widget;
	}
 
 
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;
		
		$instance['title'] = $new_instance['title'];
		$instance['number'] = $new_instance['number'];
		$instance['cat'] = $new_instance['cat'];
		
		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$title = ( $instance ) ? esc_attr($instance['title']) : __('Recent Posts', SH_NAME);
		$number = ( $instance ) ? esc_attr($instance['number']) : 4;
		$cat = ( $instance ) ? esc_attr($instance['cat']) : '';?>
			
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title: ', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('No. of Posts:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo $number; ?>" />
        </p>
       
        <p>
            <label for="<?php echo $this->get_field_id('cat'); ?>"><?php _e('Category', SH_NAME); ?></label>
            <?php wp_dropdown_categories( array('show_option_all'=>__('All Categories', SH_NAME), 'selected'=>$cat, 'class'=>'widefat', 'name'=>$this->get_field_name('cat')) ); ?>
        </p>
        
		<?php 
	}
	
	function posts()
	{
		
		if( have_posts() ):?>
        
            <ul class="recent_posts_widget">
                
                <?php while( have_posts() ): the_post(); ?>
                    
					<li>
						<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
							<?php the_post_thumbnail('600x600'); ?>
                            <?php the_title(); ?>
                        </a>
						<a href="<?php the_permalink(); ?>" class="readmore" title="<?php the_title(); ?>"><?php echo get_the_date(); ?></a>
					   <div class="rating">
							<?php _sh_star_rating( true ); ?>
						</div><!-- rating -->
					</li>
					
                <?php endwhile; ?>
                
            </ul>
            
		<?php endif;
    }
}

/// recent posts
class SH_Recent_Post_img extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'SH_Recent_Post_img', /* Name */__('Jolly Recent Posts Images',SH_NAME), array( 'description' => __('Show the recent posts images', SH_NAME )) );
	}
 

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		
		echo $before_widget;
		
		echo $before_title.$title.$after_title; 
		
		$query_string = 'posts_per_page='.$instance['number'];
		if( $instance['cat'] ) $query_string .= '&cat='.$instance['cat'];
		query_posts( $query_string ); 
	
		$this->posts();
		wp_reset_query(); 
		
		echo $after_widget;
	}
 
 
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;
		
		$instance['title'] = $new_instance['title'];
		$instance['number'] = $new_instance['number'];
		$instance['cat'] = $new_instance['cat'];
		
		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$title = ( $instance ) ? esc_attr($instance['title']) : __('Recent Posts', SH_NAME);
		$number = ( $instance ) ? esc_attr($instance['number']) : 6;
		$cat = ( $instance ) ? esc_attr($instance['cat']) : '';?>
			
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title: ', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('No. of Posts:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo $number; ?>" />
        </p>
       
        <p>
            <label for="<?php echo $this->get_field_id('cat'); ?>"><?php _e('Category', SH_NAME); ?></label>
            <?php wp_dropdown_categories( array('show_option_all'=>__('All Categories', SH_NAME), 'selected'=>$cat, 'class'=>'widefat', 'name'=>$this->get_field_name('cat')) ); ?>
        </p>
        
		<?php 
	}
	
	function posts()
	{
		
		if( have_posts() ): ?>
        
            <div class="module-widget">
			<ul class="footer_post">
                
                <?php while( have_posts() ): the_post(); ?>
                    <li class="entry">
                        <?php the_post_thumbnail('89x87'); ?>
						<div class="magnifier">
                        	<div class="buttons">
                            	<a class="st" rel="bookmark" href="<?php the_permalink(); ?>"><i class="fa fa-plus"></i></a>
                            </div><!-- end buttons -->
                      	</div><!-- end magnifier -->  
					</li>
                <?php endwhile; ?>
                
            </ul>
            </div><!-- end widget -->
		<?php endif;
    }
}

// Subscribe to our mailing list
class SH_feedburner extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'SH_subscribe_mail_list', /* Name */__('Jolly Subscribe to Mailing List',SH_NAME), array( 'description' => __('create account on http://feedburner.com and allow users to subscribe', SH_NAME )) );
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		
		echo $before_widget;?>
        
        <?php echo $before_title . $title . $after_title ; ?>
		
		<div class="subscribe_widget">
			<?php if( $instance['text'] ) echo '<p>'.$instance['text'].'</p>'; ?>
			<form target="popupwindow" method="post" id="subscribe" action="http://feedburner.google.com/fb/a/mailverify" accept-charset="utf-8" class="newsletter_form">
				<input class="form-control" type="text" name="email" value="" id="email" placeholder="<?php _e("Enter your email address" , SH_NAME) ; ?>">
				<input type="hidden" id="uri" name="uri" value="<?php echo $instance['ID']; ?>">
				<input type="hidden" value="en_US" name="loc">
				<input class="btn btn-sm btn-primary pull-right" type="submit" id="submit" name="submit" value="<?php _e("Subscribe" , SH_NAME); ?>">
				
			</form>
		</div>
		<?php
		
		echo $after_widget;
	}
	
	
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

		$instance['title'] = strip_tags($new_instance['title']);
		$instance['ID'] = $new_instance['ID'];
		$instance['text'] = $new_instance['text'];
		
		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$title = ($instance) ? esc_attr($instance['title']) : __('Subscribe to Our Mailing List', SH_NAME);
		$ID = ($instance) ? esc_attr($instance['ID']) : 'themeforest';
		$text = ($instance) ? esc_attr($instance['text']) : '';?>
        
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
       
        <p>
            <label for="<?php echo $this->get_field_id('ID'); ?>"><?php _e('Feedburner ID:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('ID'); ?>" name="<?php echo $this->get_field_name('ID'); ?>" type="text" value="<?php echo esc_attr($ID); ?>" />
        </p>
		<p>
            <label for="<?php echo $this->get_field_id('text'); ?>"><?php _e('Text:', SH_NAME); ?></label>
            <textarea class="widefat" id="<?php echo $this->get_field_id('text'); ?>" name="<?php echo $this->get_field_name('text'); ?>" ><?php echo $text; ?></textarea>
        </p>
		<?php 
	}
}

// Flicker Gallery
class SH_Flickr extends WP_Widget
{
	function __construct()
	{
		parent::__construct( /* Base ID */'SH_Flickr', /* Name */__('Genuine Flickr Feed',SH_NAME), array( 'description' => __('Fetch the latest feed from Flickr', SH_NAME )) );
	}
	
	function widget($args, $instance)
	{
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );

		$flickr_id = $instance['flickr_id'];
		$number = $instance['number'];
	
		echo $before_widget;

		echo $before_title.$title.$after_title;
		
		$limit = ( $number ) ? $number : 8;?>
            <div class="flickr-image">
               <ul class="flickr flickr-images">
			   <script type="text/javascript">
					jQuery(document).ready(function($) {
						$('.flickr-images').jflickrfeed({
							limit: <?php echo $limit;?> ,
							qstrings: {id: '<?php echo $flickr_id ?>'},
							itemTemplate: '<li><a href="{{link}}" title="{{title}}"><img src="{{image_s}}" alt="{{title}}" /></a></li>'
						});
					});
               </script>
               </ul>
            </div><?php
			
		echo $after_widget;
	}
	
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;
		
		$instance['title'] = strip_tags($new_instance['title']);

		$instance['flickr_id'] = $new_instance['flickr_id'];
		$instance['number'] = $new_instance['number'];
		
		return $instance;
	}
	
	function form($instance)
	{
		wp_enqueue_script('flickrjs');
		$title = ($instance) ? esc_attr($instance['title']) : __('Flicker', SH_NAME);
		$flickr_id = ($instance) ? esc_attr($instance['flickr_id']) : '';
		$number = ( $instance ) ? esc_attr($instance['number']) : 8;?>
		  
        <p>
            <label for="<?php echo $this->get_field_id('title');?>"><?php _e('Title:', SH_NAME);?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title');?>" name="<?php echo $this->get_field_name('title');?>" type="text" value="<?php echo $title;?>" />
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('flickr_id');?>"><?php _e('Flickr ID:', SH_NAME);?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('flickr_id');?>" name="<?php echo $this->get_field_name('flickr_id');?>" type="text" value="<?php echo $flickr_id;?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('number');?>"><?php _e('Number of Images:', SH_NAME);?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('number');?>" name="<?php echo $this->get_field_name('number');?>" type="text" value="<?php echo $number;?>" />
        </p>
        <?php 
	}
}

// contact us
class SH_ContactUs extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'SH_ContactUs', /* Name */__('Jolly Contact us',SH_NAME), array( 'description' => __('Contact us form', SH_NAME )) );
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		
		echo $before_widget;
		?>
		
		<?php wp_enqueue_script( array( 'jquery-jigowatt' ) ); ?>
		<?php echo $before_title.$title.$after_title;?>
		
		 <div class="contact_form row clearfix">
		 	<div id="respond_message1"></div>
			  <form id="contactform1" action="<?php echo admin_url('admin-ajax.php'); ?>?action=_sh_ajax_callback&amp;subaction=contact_form_submit" name="contactform"  method="post">
				
				  <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
				  	<input type="text" name="name" id="contact_name1" class="form-control" placeholder="<?php _e("Name" , SH_NAME);?>"> 
                  </div>
				  <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  	<input type="text" name="email" id="contact_email1" class="form-control" placeholder="<?php _e("Email Address" , SH_NAME);?>">
                  </div>
				  <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  	<input type="text" name="website" id="contact_website1" class="form-control" placeholder="<?php _e("Website" , SH_NAME);?>"> 
                  </div>
				  <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
                  	<textarea class="form-control active" name="comments" id="contact_message1" rows="6" placeholder="<?php _e("Message" , SH_NAME);?>"></textarea>
                    <button type="submit" value="<?php _e("SEND" , SH_NAME);?>" id="contact_submit1" class="btn btn-primary pull-right">Submit</button>
					<img class="form_loader1" alt="" src="<?php echo get_template_directory_uri();?>/images/ajaxLoader.gif" style="visibility:hidden;">
                  </div>					
			  </form>
			
			</div>
		
		<?php
		
		echo $after_widget;
	}
	
	
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

		$instance['title'] = strip_tags($new_instance['title']);
		$instance['email'] = $new_instance['email'];

		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$title = ($instance) ? esc_attr($instance['title']) : __('Contact Form', SH_NAME);
		$email = ( $instance ) ? esc_attr($instance['email']) : 'contact@yourdomain.com';?>
        
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('email'); ?>"><?php _e('Email ID:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('email'); ?>" name="<?php echo $this->get_field_name('email'); ?>" type="text" value="<?php echo esc_attr( $email ); ?>" />
        </p>
        
                
		<?php 
	}
}




