<?php ob_start(); 

wp_enqueue_script( array('jquery-flexslider' ) );?>

					<div class="module-widget clearfix">
						
						<?php if( $title ): ?>
						<div class="title">
							<h3><?php echo $title; ?></h3>
						</div>
						<?php endif; ?>
						<?php $exp_images = explode( ',', $images ); ?>
						
						<?php if( $exp_images ): ?>
						
						<!-- Place somewhere in the <body> of your page -->
						<div id="slider" class="flexslider">
							<ul class="slides">
								
								<?php foreach( $exp_images as $k=>$e_img ): ?>
									<li> <?php echo wp_get_attachment_image( $e_img, 'full', '', array('class'=>'img-responsive' ) ); ?> </li>
								<?php endforeach; ?>

							</ul>
						</div>
						
						<?php endif; ?>
					</div>

<?php return ob_get_clean();

