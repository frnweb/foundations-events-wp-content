<?php 
$query_args = array('post_type' => 'sh_faq' , 'showposts' => $num ,'order_by' => $sort , 'order' => $order );

if( $cat) $query_args['faq_category'] = $cat;
$query = new WP_Query($query_args) ; $count = 1; 

$col_md = ( $image ) ? 'col-lg-6 col-md-6 col-sm-6 ' : 'col-lg-12 col-md-12 col-sm-12 ';

ob_start(); ?>

							<?php if( $image ):?>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            
							<?php echo wp_get_attachment_image( $image, 'full', '', array('class'=>'img-responsive') ); ?>
                            
							</div><!-- end col -->
                            <?php endif; ?>
							
							<div class="<?php echo $col_md; ?>col-xs-12">
								<?php if( $title ): ?>
									<div class="title">
										<h3><?php echo $title; ?></h3>
									</div><!-- end title -->
								<?php endif; ?>
								
                                <?php echo do_shortcode( $contents ); ?>
                                
                                <?php if( $query->have_posts() ): ?>
								
								<div id="accordion-first" class="clearfix">
                                    
									<div class="accordion" id="accordion2">
                                      
									  <?php while( $query->have_posts() ): $query->the_post(); ?>
									  
									  <div class="accordion-group">
                                        <div class="accordion-heading">
                                          <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne<?php the_ID(); ?>">
                                            <em class="fa fa-plus icon-fixed-width"></em><?php the_title(); ?>
                                          </a>
                                        </div>
                                        <div id="collapseOne<?php the_ID(); ?>" class="accordion-body collapse">
                                          <div class="accordion-inner">
                                            <?php the_content(); ?>
                                          </div>
                                        </div>
                                      </div>
                                      
									  <?php endwhile; ?>
									  
									  
                                    
									</div><!-- end accordion -->
                                </div><!-- end accordion first -->      
                            	
								<?php endif; ?>
								
							</div><!-- end col-lg-6 -->
							

<?php wp_reset_query();
return ob_get_clean(); ?>


<div class="white-wrapper">
	<div class="container">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="widget">
				
				<?php if( $title ): ?>
				<h3><?php echo $title; ?></h3>
				<?php endif; ?>
				
				<div class="about_tabbed">
					<div class="panel-group" id="accordion2">
						
						<?php while( $query->have_posts() ): $query->the_post(); ?>
						
						<div class="panel panel-default">
							<div class="panel-heading active">
								<h4 class="panel-title"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse<?php the_ID(); ?>"><?php the_title(); ?></a> </h4>
							</div>
							<!-- end panel-heading -->
							<div id="collapse<?php the_ID(); ?>" class="panel-collapse collapse<?php if( $query->current_post == 0 ) echo ' in'; ?>">
								<div class="panel-body">
									<?php the_content(); ?>
								</div>
								<!-- end panel-body --> 
							</div>
							<!-- end collapseOne --> 
						</div>
						<!-- end panel -->
						
						<?php endwhile; ?>
						
					</div>
					<!-- end panel-group --> 
				</div>
				<!-- end about tabbed --> 
			</div>
			<!-- end widget --> 
		</div>
		<!-- end col-lg-12 --> 
	</div>
	<!-- end container --> 
</div>
<!-- end grey-wrapper -->

<?php $output = ob_get_contents();
ob_end_clean();
return $output;