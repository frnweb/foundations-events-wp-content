<?php 
$count = 1; 
$query_args = array('post_type' => 'sh_services' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);

if( $cat ) $query_args['services_category'] = $cat;
$query = new WP_Query($query_args) ; 

$with_border = ( $with_border ) ? ' with-border' : '';

ob_start();
?>

	<div class="container">
		<div class="module clearfix">
			<div class="row">
			
				<?php if($query->have_posts()): while($query->have_posts()): $query->the_post();
					  global $post ; 
					  $services_meta = _WSH()->get_meta() ; 
					  $icon = sh_set($services_meta , 'fontawesome');?>
				
						<div itemscope itemtype="https://schema.org/Service" class="col-md-4 col-sm-4 col-xs-12">
							<div class="service-box">
								<div class="icon-container<?php echo $with_border; ?>">
									<i class="fa <?php echo $icon; ?>"></i>
								</div><!-- icon-container -->
								<div class="title">
									
									<?php if( $link = sh_set( $services_meta, 'link' ) ): ?>
										<h3 itemprop="name"><a href="<?php echo esc_url( $link ); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
									<?php else: ?>
										<h3 itemprop="name"><?php the_title(); ?></h3>
									<?php endif; ?>
								
								</div><!-- end title -->
								
								<div class="desc">
									<p itemprop="text"><?php echo _sh_trim( get_the_excerpt(), $limit ); ?></p>
								</div><!-- end desc -->
							</div><!-- end service-box -->
						</div><!-- end col -->
						
				<?php endwhile; endif; ?>
				
				<?php if( $more_link ): ?>
				
				<div class="col-md-12 button-wrapper clearfix text-center">
					<a class="btn btn-primary btn-dark" title="<?php echo esc_attr($more_text); ?>" href="<?php echo esc_url($more_link); ?>"><?php echo $more_text; ?></a>
				</div><!-- end button-wrapper -->             
				
				<?php endif; ?>
			</div><!-- end row -->
		</div><!-- end content --> 
	</div>
	
	<div class="clearfix"></div>

<?php 
$output = ob_get_contents(); 
ob_end_clean(); 
return $output ; ?>
   