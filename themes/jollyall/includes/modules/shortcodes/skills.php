<?php //wp_enqueue_script( array('jquery-easypiechart-min') ); 
//wp_enqueue_script( 'jquery-easypiechart', get_template_directory_uri(). '/js/jquery.easypiechart.min.js' );
ob_start(); ?>

<?php $bg_img =  ($bg ) ? ' style="background-image: url( '.wp_get_attachment_url( $bg ).' );" ' : ''; ?>
	<div class="halfscreen parallax"<?php echo $bg_img; ?> data-stellar-background-ratio="0.6" data-stellar-vertical-offset="20">
		
		<div class="<?php echo ($bg) ? 'overlay dark-version' : 'no-overlay'; ?>">
        	
			<div class="container">
            	<div class="module clearfix">
                    
					<?php if( $title ): ?>
					
					<div class="title wow zoomIn clearfix">
                        <h2><?php echo $title; ?></h2>
                        <hr>
                    </div><!-- end title -->
					
					<?php endif; ?>
					
                    <div class="row text-center">
                    	<?php echo do_shortcode( $contents ); ?>
					</div>
					
			
            
                </div><!-- end module -->
            </div><!-- end container -->
    	</div><!-- end overlay -->
    </div><!-- end transparent-bg -->

	<script type="text/javascript" src="<?php echo  get_template_directory_uri(). '/js/jquery.easypiechart.min.js'; ?>"></script> 
<?php return ob_get_clean(); ?>


