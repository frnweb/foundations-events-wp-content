<?php ob_start(); 
wp_enqueue_script( array('text-rotator'));
?>

<?php if( $bg ): 
$attachment = wp_get_attachment_url($bg);?>
<div id="two-parallax" class="parallax" style="background-image: url('<?php echo $attachment; ?>');" data-stellar-background-ratio="0.6" data-stellar-vertical-offset="20">
<div class="overlay">
<?php else: ?>
<div class="white-wrapper">
<?php endif; ?>
    <div class="container">
        <div class="messagebox">
            <h2>
			<?php echo $title1 ; ?>  
			
			<?php if( $rotating_words ): ?>
			<mark class="rotate"><?php echo $rotating_words ; ?></mark>
			<?php endif; ?>
			
			<?php echo $title2 ; ?>
			</h2>
            <p class="lead"><?php echo $text ; ?></p>
            
			<?php if( $btn1_text ): ?>
			<a class="btn btn-primary btn-lg" href="<?php echo $btn1_link; ?>"><?php echo $btn1_text ; ?></a> 
			<?php endif;
			
			if( $btn2_text ): ?>
			<a class="btn btn-dark btn-lg" href="<?php echo $btn2_link; ?>"><?php echo $btn2_text; ?></a>
			<?php endif; ?>
			
        </div><!-- end messagebox -->
    </div><!-- end container -->
</div>
<?php if( $bg ): ?>
</div>
<?php endif; ?>

<?php $output = ob_get_contents(); 
	  ob_end_clean(); 
	  return $output ; ?>