<?php 
$query_args = array('post_type' => 'sh_testimonials' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
//if( $cat ) $query_args['tax_query'] = array(array('taxonomy' => 'testimonials_category','field' => 'id','terms' => $cat));
if( $cat ) $query_args['testimonials_category'] = $cat;
$query = new WP_Query($query_args) ; 
$bg_src = wp_get_attachment_url( $bg );
wp_enqueue_script( array( 'jquery-flexslider' ) ) ; 
ob_start();

?>

<?php if( $bg ): ?>
<div class="halfscreen parallax" style="background-image: url('<?php echo $bg_src; ?>');" data-stellar-background-ratio="0.6" data-stellar-vertical-offset="20">
	<div class="overlay dark-version">
<?php else: ?>
<div class="white-wrapper">
	<div class="white-testimonial">
<?php endif; ?>

		<div class="container">
			<div class="module">
				
				<?php if( $title ): ?>
					<div class="title">
						<h2><?php echo $title; ?></h2>
						<hr>
					</div><!-- end title -->
				<?php endif; ?>
				
				<div id="testimonials" class="flexslider">
					<ul class="slides">
						<?php while( $query->have_posts() ): $query->the_post(); ?>
							<li><?php the_post_thumbnail('thumbnail', array('class'=>'img-responsive') ); ?></li>
						<?php endwhile; ?>
					</ul>
				</div><!-- #testimonials -->
				
				<div id="slider" class="flexslider">
					<ul class="slides text-center">
						
						<?php while( $query->have_posts() ): $query->the_post(); 
						$meta = _WSH()->get_meta();?>
						<li>
							<h3><?php the_title(); ?></h3>
							<h4><?php echo sh_set( $meta, 'designation' ); ?> @ <?php echo sh_set( $meta, 'company' ); ?></h4>
							<p> &ldquo; <?php echo _sh_trim( get_the_excerpt(), $limit ); ?>&rdquo;</p>
						</li>
						<?php endwhile; ?>
					</ul>
				</div><!-- #slider -->
			</div><!-- end module --> 
		</div><!-- end container -->
	</div><!-- end overlay -->
</div><!-- end transparent-bg -->

<?php wp_reset_query();
return ob_get_clean();