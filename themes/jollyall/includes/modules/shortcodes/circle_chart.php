<?php ob_start(); ?>



							<div class="module_widget clearfix">
                                
								<?php if( $title ): ?>
									<div class="title">
										<h3><?php echo $title; ?></h3>
									</div><!-- end title -->
								<?php endif; ?>
								
								<?php if( $text ): ?>
									<div class="desc">
										<p><?php echo $text; ?></p>
									</div><!-- end desc -->
								<?php endif; ?>
								<br>
                                
                                <?php if( $contents ): 
									
									$exp_contents = explode( "\n", $contents );
									
									if( $exp_contents ): ?>
										
										<?php foreach( $exp_contents as $exp_con ): ?>
											<?php $values = explode( '|', $exp_con ); ?>
											
											<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<div class="skill">
													<span class="chart" data-bar-color="#fff" data-percent="<?php echo (int)sh_set( $values, 1 ); ?>"><span class="percent"></span></span>
													<h3><?php echo sh_set($values, 0); ?></h3>
												</div>
											</div>
											
										<?php endforeach; ?>
								<?php endif; 
								
								endif;?>
                                
                                 
                            </div><!-- end module widget -->

<script type="text/javascript" src="<?php echo  get_template_directory_uri(). '/js/jquery.easypiechart.min.js'; ?>"></script> 

<?php return ob_get_clean();