<?php $options = _WSH()->option();
ob_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
  
  <meta http-equiv="content-type" content="text/html; charset=UTF-8"> 

  <title><?php echo get_bloginfo('name').' - '.get_bloginfo('description' ); ?></title>

  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <?php echo ( sh_set( $options, 'site_favicon' ) ) ? '<link rel="icon" type="image/png" href="'.sh_set( $options, 'site_favicon' ).'">': '';?>    
  
  <!-- Bootstrap Styles -->
  <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.css" rel="stylesheet">
  
  <!-- Styles -->
  <link href="<?php echo get_template_directory_uri(); ?>/style.css" rel="stylesheet">
  
  <!-- Carousel Slider -->
  <link href="<?php echo get_template_directory_uri(); ?>/css/owl-carousel.css" rel="stylesheet">
  
  <!-- CSS Animations -->
  <link href="<?php echo get_template_directory_uri(); ?>/css/animate.min.css" rel="stylesheet">
  
  <!-- Google Fonts -->
  <!-- Google Font -->
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300,600,700,800' rel='stylesheet' type='text/css'>

  <!-- SLIDER ROYAL CSS SETTINGS -->
  <link href="<?php echo get_template_directory_uri(); ?>/royalslider/royalslider.css" rel="stylesheet">
  <link href="<?php echo get_template_directory_uri(); ?>/royalslider/skins/default-inverted/rs-default-inverted.css" rel="stylesheet">
  
  <!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/rs-plugin/css/settings.css" media="screen" />
        
  <!-- Support for HTML5 -->
  <!--[if lt IE 9]>
    <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->

  <!-- Enable media queries on older bgeneral_rowsers -->
  <!--[if lt IE 9]>
    <script src="<?php echo get_template_directory_uri(); ?>/js/respond.min.js"></script>
  <![endif]-->
  
</head>
<?php $bg_url = ( $bg ) ? ' style="background-image:url('.wp_get_attachment_url($bg).');"' : ''; ?>
<body id="maintenance"<?php echo $bg_url; ?>>

	<div class="animationload">
    <div class="loader">Loading...</div>
	</div>
	
	<?php $logo_img = wp_get_attachment_image($logo , 'full'); ?>
 	<div class="overlayss">
		<div class="container">
			<div class="mmode text-center">
                <div class="title">
					<div class="logo">
                	<a href="<?php echo home_url(); ?>" title="<?php bloginfo('name' ); ?>"><?php echo $logo_img ; ?></a>
					</div>
                    <h1><?php echo $heading; ?></h1>
                    
                </div>
                
				<div id="countdown" class="clearfix">
					<div class="stat f-container">
                        <div class="f-element col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="milestone-counter">
                                <span class="days stat-count highlight">211</span>
                                <div class="milestone-details"><?php _e("Days" , SH_NAME); ?></div>
                            </div>
                        </div>
                        <div class="f-element col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="milestone-counter">
                                <span class="hours stat-count highlight">113</span>
                                <div class="milestone-details"><?php _e("Hours" , SH_NAME); ?></div>
                            </div>
                        </div>
                        <div class="f-element col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="milestone-counter">
                                <span class="minutes stat-count highlight">431</span>
                                <div class="milestone-details"><?php _e("Minutes" , SH_NAME); ?></div>
                            </div>
                        </div>
                        <div class="f-element col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="milestone-counter">
                                <span class="seconds stat-count highlight">2113</span>
                                <div class="milestone-details"><?php _e("Seconds" , SH_NAME); ?></div>
                            </div>
                        </div>
					</div><!-- stat -->
                </div><!-- end countdown -->
				
				<?php if( $subscribe ): ?>
				
					<div class="clearfix">
						<div class="button-wrapper text-center">
							<a href="<?php echo esc_url($subscribe); ?>" title="<?php bloginfo('name'); ?>" class="btn btn-primary btn-transparent"><?php _e('Subscribe Us', SH_NAME); ?></a>			
						</div>
					</div>
				
				<?php endif; ?>
				
                <div class="clearfix text-center">
                        <div class="social-icons">
                            <?php echo sh_get_social_icons(); ?>
                        </div><!-- end social icons -->
                
                </div><!-- end clear -->
				
			</div><!-- mmode -->
		</div><!-- container -->
	</div>
        
	<div class="dmtop"><?php _e("Scroll to Top" , SH_NAME); ?></div>
       
  <!-- Main Scripts-->
  <script src="<?php echo home_url(); ?>/wp-includes/js/jquery/jquery.js"></script>
  <script src="<?php echo home_url(); ?>/wp-includes/js/jquery/jquery-migrate.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/js/countdown.js"></script>
  <script>
	// countdown script
		jQuery("#countdown").countdown({
			date: "<?php echo date('d F Y h:i:s', strtotime( $date ) ); ?>", // add your date here.
				format: "on"
		},
		function() { 
			alert("done!") 
		});	
  </script>
  
  <script>
		(function($) {
		  "use strict";
		// Page Preloader
		$(window).load(function() {
			$(".loader").delay(300).fadeOut();
			$(".animationload").delay(600).fadeOut("slow");
		});
	})(jQuery);
  </script>
         
  <!-- Demo Switcher JS -->
<?php //wp_footer(); ?>    
</body>
</html>

<?php $output = ob_get_contents(); 
	  ob_end_clean(); 
	  return $output ; ?>