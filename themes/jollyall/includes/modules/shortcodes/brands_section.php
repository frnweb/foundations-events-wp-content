<?php $count = 1; 
wp_enqueue_script( array( 'owl.carousel.min' ) ) ;
$options = _WSH()->option();
$get_brands = sh_set(sh_set($options , 'brand_or_client') , 'brand_or_client' ) ;

if( !$get_brands ) return;

/** remove extra key tocopy */

foreach( $get_brands as $k => $v ) {
	if( sh_set( $v, 'tocopy' ) ) unset( $get_brands[$k] );
}

$white = ( $white ) ? 'white-wrapper' : 'grey-wrapper';
$get_brands = ( $order == 'DESC' ) ? array_reverse( (array)$get_brands ) : $get_brands;

$brands = ( $num ) ? array_slice( $get_brands, 0, ( $num + 1 ) ) : $get_brands;

ob_start();

?>

<div class="<?php echo $white; ?> clearfix">
	<div class="container">
		<div class="client-module">
			<div id="clients_carousel">
				
				<?php foreach( $brands as $c ): ?>
				<div class="client">
				
					<a href="<?php echo esc_url(sh_set($c , 'link')); ?>" title="<?php echo esc_attr(sh_set($c , 'title')); ?>" target="_blank">
					<img src="<?php echo esc_url(sh_set($c , 'brand_img')); ?>" class="img-responsive" alt="<?php echo esc_attr(sh_set($c , 'title')); ?>">
					</a>

				</div><!-- end client -->
				<?php endforeach; ?>
				
			</div><!-- end row -->
		</div><!-- end module --> 
	</div><!-- end container -->
</div><!-- end grey-wrapper -->

<?php return ob_get_clean(); ?>
