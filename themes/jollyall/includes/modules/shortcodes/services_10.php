<?php $query_args = array('post_type' => 'sh_services' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
if( $cat ) $query_args['services_category'] = $cat;
$query = new WP_Query($query_args) ; 

ob_start();?>

	<div class="white-wrapper clearfix">
        <div class="container">
            <div class="module">

                <?php if( $title ): ?>
					<div class="title wow zoomIn text-center clearfix">
						<h2><?php echo $title; ?></h2>
						
						<?php if( $tagline ): ?>
							<hr>
							<p class="lead new-lead"><?php echo $tagline; ?></p>
						<?php endif; ?>
					</div><!-- end title -->
				<?php endif; ?>
				
                <div class="row margin-top">
                    
					<?php while( $query->have_posts() ) : $query->the_post(); 
					
						$meta = _WSH()->get_meta(); ?>
					
						<div itemscope itemtype="https://schema.org/Service" class="col-md-4 col-sm-4 col-xs-12">
							<div class="service-box">
								<div class="icon-container wow zoomIn">
									<i class="<?php echo sh_set( $meta, 'fontawesome' ); ?>"></i>
								</div><!-- icon-container -->
								<div class="title">
									<?php if( $link = sh_set( $meta, 'link' ) ): ?>
										<h3 itemprop="name"><a href="<?php echo esc_url( $link ); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
									<?php else: ?>
										<h3 itemprop="name"><?php the_title(); ?></h3>
									<?php endif; ?>
								</div><!-- end title -->
								<div itemprop="text" class="desc">
									<?php echo _sh_trim( get_the_excerpt(), $limit, '.' ); ?>
								</div><!-- end desc -->
							</div><!-- end service-box -->
						</div><!-- end col -->
                    	
						<?php if( ( $query->current_post + 1 ) % 3 == 0 && ( $query->current_post + 1 ) !== $query->post_count ): ?>
							<div class="clearfix"></div>
                    		<hr>
						<?php endif; ?>
					<?php endwhile; ?>
					
					<?php if( $link ): ?>
					
						<div class="col-md-12 button-wrapper clearfix text-center">
							<a href="<?php echo esc_url( $link ); ?>" title="<?php echo $title; ?>" class="btn btn-primary btn-dark"><i class="fa fa-search"></i> <?php _e('View All Services', SH_NAME); ?></a>			
						</div><!-- end button-wrapper -->             
                	
					<?php endif; ?>
				</div><!-- end row -->
            </div><!-- end content --> 
        </div><!-- end container -->
    </div>


<?php wp_reset_query();
return ob_get_clean();