<?php ob_start(); ?>
<section class="make-bg-full">
    <div class="calloutbox-full-mini nocontainer" style="background-color:<?php echo $bg_color; ?>">
        <h2><?php echo $heading ; ?></h2>
    </div>
</section>

<?php $output = ob_get_contents(); 
	  ob_end_clean(); 
	  return $output ; ?>