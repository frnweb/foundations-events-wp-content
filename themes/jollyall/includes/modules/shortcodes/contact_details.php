<?php ob_start(); ?>

<?php if( $bg ): ?>
<div class="halfscreen parallax" data-stellar-vertical-offset="20" data-stellar-background-ratio="0.6" style="background-image: url('<?php echo wp_get_attachment_url($bg); ?>');">				
		<div class="overlay dark-version">
<?php endif; ?>

<div class="container">
	<div class="module clearfix">
		<div class="contact_details">
			<div class="col-lg-4 col-sm-4 col-md-6 col-xs-12">
				<div class="text-center">
					<div class="wow swing contant_details">
						<div class="contact-icon">
							<a href="#" class=""> <i class="fa fa-map-marker fa-3x"></i> </a>
						</div><!-- end dm-icon-effect-1 -->
						 <p><?php echo $address ; ?></p>
					</div><!-- end service-icon -->
				</div><!-- end miniboxes -->
			</div><!-- end col-lg-4 -->
			
			<div class="col-lg-4 col-sm-4 col-md-6 col-xs-12">
				<div class="text-center">
					<div class="wow swing contant_details">
						<div class="contact-icon">
							<a href="#" class=""> <i class="fa fa-phone fa-3x"></i> </a>
						</div><!-- end dm-icon-effect-1 -->
						 <p><strong><?php _e("Phone:" , SH_NAME); ?></strong> <?php echo $ph1; ?><br>
						<strong><?php _e("Phone:" , SH_NAME) ; ?></strong> <?php echo $ph2 ; ?></p>
					</div><!-- end service-icon -->
				</div><!-- end miniboxes -->
			</div><!-- end col-lg-4 -->  
	
			<div class="col-lg-4 col-sm-4 col-md-6 col-xs-12">
				<div class="text-center">
					<div class="wow swing contant_details">
						<div class="contact-icon">
							<a href="#" class=""> <i class="fa fa-envelope-o fa-3x"></i> </a>
						</div>
						 <p>
							 <?php if( $email1 ): ?>
								<strong><?php _e("Email:" , SH_NAME); ?></strong> <?php echo $email1 ; ?>
							 <?php endif; ?>
							 
							 <?php if( $email2 ): ?>
								<br /><strong><?php _e("Email 2:" , SH_NAME); ?></strong> <?php echo $email2 ; ?>
							 <?php endif; ?>
							 
							<?php if( $skype ): ?>
								<br /><strong><?php _e("Skype:" , SH_NAME); ?></strong> <?php echo $skype ; ?>
							 <?php endif; ?>

						</p>
					</div>
				</div>
			</div>                  
		</div>
	</div>
</div>

<div class="clearfix"></div>
<br />
<br />

<?php if( $bg ): ?>
	</div>
</div>
<?php endif; ?>

<?php return ob_get_clean();
