<?php ob_start(); ?>
	<div class="white-wrapper module clearfix">
        	<div class="container">
            	<div class="doc">
                	<div class="clearfix">
                    	<div class="col-lg-12">
                            
							<div class="col-lg-4 col-md-4 col-sm-12">
                                <div class="title">
                                    <h3><?php _e('Last 10 Posts', SH_NAME); ?></h3>
                                </div>
                                <ul class="check">
                                    <?php wp_get_archives('type=postbypost&limit=10'); ?>	
                                </ul>
                            </div><!-- end col-4 --> 
							
							<div class="col-lg-2 col-md-2 col-sm-12">
                                <div class="title">
                                    <h3><?php _e('Pages', SH_NAME); ?></h3>
                                </div>
                                <ul class="check">
                                   <?php wp_list_pages(array('title_li'=>false, 'number' => 10)); ?>	
                                </ul>
                            </div><!-- end col-2 --> 
                            
                            <div class="col-lg-2 col-md-2 col-sm-12">
                                <div class="title">
                                    <h3><?php _e('Categories', SH_NAME); ?></h3>
                                </div>
                                <ul class="check">
                                    <?php wp_list_categories(array('title_li'=>false, 'number' => 10)); ?>
                                </ul>
                            </div><!-- end col2 -->
                            
                            <div class="col-lg-2 col-md-2 col-sm-12">
                                <div class="title">
                                    <h3><?php _e('Tags', SH_NAME); ?></h3>
                                </div>
								
								<ul class="check">
									<?php wp_list_categories(array('title_li'=>false, 'number' => 10, 'taxonomy' => 'post_tag')); ?>
								</ul>
                                
                            </div><!-- end col-2 --> 
            
                            <div class="col-lg-2 col-md-2 col-sm-12">
                                <div class="title">
                                    <h3><?php _e('Archives', SH_NAME); ?></h3>
                                </div>
                                <ul class="check">
                                    <?php wp_get_archives(); ?>
                                </ul>
                            </div><!-- end col-2 --> 
            
                            
          				</div><!-- end col-lg-12 -->
                	</div><!-- end fullwidth -->
                </div><!-- end row -->
            </div><!-- end container -->
        </div>
        
<?php $output = ob_get_contents(); 
	  ob_end_clean(); 
	  return $output ; ?>