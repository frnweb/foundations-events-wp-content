<?php 
$query_args = array('post_type' => 'post' , 'showposts' => $num ,'order_by' => $sort , 'order' => $order , 'category' => $cat);
$query = new WP_Query($query_args) ; $count = 1;  
wp_enqueue_script( array('owl.carousel.min'));
_WSH()->page_settings = array('layout'=>'full', 'view'=> 'grid', 'sidebar'=>'');
ob_start(); 
?>
<div class="widget">
    <h3><?php echo $title; ?></h3>
    <div id="owl-blog" class="owl-carousel">
    
        <?php if($query->have_posts()) : while($query->have_posts()) : $query->the_post();  global $post ; 
            
            get_template_part( 'blog', get_post_format() ); 
            
			endwhile ; endif;  wp_reset_query(); ?>
    </div>
</div>
<?php $output = ob_get_contents(); 
	  ob_end_clean(); 
	  return $output ; ?>