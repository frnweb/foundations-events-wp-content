<?php ob_start(); ?>

<?php if( $bg ): ?>
<div class="halfscreen parallax" data-stellar-vertical-offset="20" data-stellar-background-ratio="0.6" style="background-image: url('<?php echo wp_get_attachment_url($bg); ?>');">				
<?php else: ?>
<div class="wrapper<?php echo ' '.$bg_class; ?>">
<?php endif; ?>

	<div class="container">
		<div class="module clearfix">
			
			<?php if( $title ): ?>
			
			<div class="title wow zoomIn text-center clearfix">
				<h2><?php echo $title; ?></h2>
				<hr>
			</div><!-- end title -->
			
			<?php endif; ?>
			
			<div class="pricing_wrapper">
				<?php echo do_shortcode( $contents ); ?>
			</div><!-- end pricing_wrapper -->
			
		</div><!-- end module -->
		<hr />
	</div><!-- end container -->

</div>

<?php return ob_get_clean();