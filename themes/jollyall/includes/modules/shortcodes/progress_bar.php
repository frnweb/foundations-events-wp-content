<?php ob_start(); ?>



							<div class="module_widget">
                                
								<?php if( $title ): ?>
									<div class="title">
										<h3><?php echo $title; ?></h3>
									</div><!-- end title -->
								<?php endif; ?>
								
								<?php if( $text ): ?>
									<div class="desc">
										<p><?php echo $text; ?></p>
									</div><!-- end desc -->
								<?php endif; ?>
        
                                <?php if( $contents ): 
									
									$exp_contents = explode( "\n", $contents );
									
									if( $exp_contents ): ?>
									
										<div class="about_skills">
                                    		<?php foreach( $exp_contents as $exp_con ): ?>
												<?php $values = explode( '|', $exp_con ); 
												
												$num = (int)trim(str_replace("\n", '', sh_set( $values, 1 ) ) );?>
												
												<h4><?php echo sh_set($values, 0); ?> - <?php echo $num; ?>%</h4>
												
												<div class="progress">
													<div data-effect="slide-left" class="progress-bar" role="progressbar" aria-valuenow="<?php echo (int)sh_set( $values, 1 ); ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo (int)sh_set( $values, 1 ); ?>%">						
													</div>
												</div>
											<?php endforeach; ?>
										</div>
									
									<?php endif; ?>
									 
								<?php endif; ?>
							</div><!-- end module_widget -->

<?php return ob_get_clean();


