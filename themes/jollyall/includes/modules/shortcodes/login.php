<?php ob_start(); ?>

	<?php if( $title ): ?>
	<div class="title">
    	<h3><?php echo $title; ?></h3>
	</div>
    <?php endif; ?>
    
    <?php if( !is_user_logged_in() ): ?>
    
        
		
		<form action="<?php echo home_url(); ?>/wp-login.php" method="post" id="loginform" class="form form-register">
			<div class="form-group clearfix">
				<label class="col-sm-3 col-xs-12 control-label"><?php _e('Email', SH_NAME ); ?></label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" required placeholder="<?php _e('support@yoursite.com', SH_NAME); ?>" class="form-control required email" name="log">
				</div>
			</div>
			<div class="form-group clearfix">
				<label class="col-sm-3 col-xs-12 control-label"><?php _e('Password', SH_NAME ); ?></label>
				<div class="col-sm-9 col-xs-12">
					<input type="password" placeholder="<?php _e('Password', SH_NAME);?>" class="form-control required" name="pwd">
				</div>
			</div>
			<div class="form-group pull-left">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="rememberme" value="forever">
                        <?php _e('Remember me', SH_NAME); ?> </label>
                </div>
            </div>
			<div class="form-group pull-right">
				<div class="col-sm-12 col-xs-12">
					<button class="btn btn-primary btn-lg" type="submit" name="wp-submit"><?php echo $button_text; ?></button>
					<input type="hidden" value="<?php echo $redirect; ?>" name="redirect_to">
					<input type="hidden" value="1" name="testcookie">
				</div>
			</div>
		</form>
        
    <?php else: ?>
    	<a href="<?php echo wp_logout_url(home_url()); ?>" title="<?php _e('Logout', SH_NAME); ?>" class="btn btn-primary"><?php _e('Logout', SH_NAME); ?></a>				
    <?php endif; ?>
    
	<div class="clearfix"></div>
	<hr>

<?php $output = ob_get_contents();
ob_end_clean();
return $output;