<?php ob_start(); ?>

					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="skill">
                            <span class="chart" data-bar-color="#fff" data-percent="<?php echo $percentage; ?>"><span class="percent"></span></span>		
                            <h3><?php echo $title; ?></h3>
                        </div>
                    </div>

<?php return ob_get_clean(); ?>

