<?php 
$theme_options = _WSH()->option(); //printr($theme_options);
$p_skills = sh_set( sh_set( $theme_options, 'powerful_skills' ), 'powerful_skills' );

if( !$p_skills ) return;

/** remove extra key tocopy */

foreach( $p_skills as $k => $v ) {
	if( sh_set( $v, 'tocopy' ) ) unset( $p_skills[$k] );
}


$p_skills = ( $order == 'DESC' ) ? array_reverse( (array)$p_skills ) : $p_skills;

$skills = ( $num ) ? array_slice( $p_skills, 0, ( $num + 1 ) ) : $p_skills;

ob_start();?>

					<div class="module_widget">
                    	
						<?php if( $title ): ?>
						<div class="title">
                        	<h3><?php echo $title; ?></h3>
                        </div><!-- end title -->
						<?php endif; ?>
						
						<?php if( $contents ): ?>
                        <div class="desc">
                        	<?php echo apply_filters( 'the_content', $contents ); ?>
                        </div><!-- end desc -->
						<?php endif; ?>

						<?php if($skills ): ?>
						
						<div class="about_skills">
                            
							<?php foreach( $skills as $skill ): 
							if( sh_set( $skill, 'tocopy' ) ) continue; ?>
							
							<h4><?php echo sh_set( $skill, 'title' ); ?> - <?php echo sh_set( $skill, 'percent' ); ?>%</h4>
                            <div class="progress">
                                <div data-effect="slide-left" class="progress-bar" role="progressbar" aria-valuenow="<?php echo sh_set( $skill, 'percent' ); ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo sh_set( $skill, 'percent' ); ?>%">				
								</div>
                            </div>
							
							<?php endforeach; ?>
							
						</div>
						
						<?php endif; ?>
					
					</div>


<?php return ob_get_clean();





