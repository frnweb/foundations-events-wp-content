<?php ob_start(); ?>

<?php if( $bg ): ?>
<div data-stellar-vertical-offset="20" data-stellar-background-ratio="0.6" style="background-image: url('<?php echo wp_get_attachment_url( $bg ); ?>');" class="fullscreen parallax">
<?php else: ?>
<div class="no-parallax">
<?php endif; ?>

	<div class="<?php echo ( $bg ) ? 'overlay' : 'no-overlay'; ?>">
		<div class="container">
			<div class="parallax-content">
				
				<?php if( $heading ): ?>
				
				<div class="title parallax-title">
					<h2><?php echo $heading; ?></h2>
					<hr>
				</div><!-- end parallax-title -->
				
				<?php endif; ?>
				
				<?php echo str_replace("</p>", '', apply_filters( 'the_content', $contents )); ?>
				
			</div><!-- end parallax content --> 
		</div><!-- end container -->
	</div><!-- end overlay -->
</div>

<?php return ob_get_clean();

