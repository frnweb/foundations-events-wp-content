<?php 
$count = 1;
$query_args = array('post_type' => 'sh_projects' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);

if( $cat ) $query_args['projects_category'] = $cat;
$query = new WP_Query($query_args) ; 

$grey = ( $white ) ? 'white-wrapper' : 'grey-wrapper';

$terms = get_terms('projects_category');
wp_enqueue_script( array('jquery-isotope-min', 'jquery-masorny-portfolio')); 

ob_start();
$filters = array();
$content_output = '';?>

<?php while( $query->have_posts() ): $query->the_post(); ?>
	
	<?php $sizes = array(0, 6, 8, 10);
	$count = ( $query->current_post > 10 ) ? ($query->current_post-1) % 10 : $query->current_post;
	$image_class = in_array( $count, $sizes ) ? 'item-w2 ' : '' ;
	$term_classes = '';
	
	$terms = wp_get_post_terms( get_the_id(), 'projects_category' );
	foreach( $terms as $t ) :
		$filters[$t->term_id] = '<li><a class="btn btn-primary" href="#" data-filter=".'.$t->slug.'">'.$t->name.'</a></li>'; 
		$term_classes .= $t->slug.' ';
	endforeach;?>
	
					<div class="item entry item-h2 <?php echo $image_class . $term_classes; ?>">
						<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
							
							<?php if( $image_class == 'item-w2 ' ): the_post_thumbnail('534x265', array('itemprop'=>'image'));  ?>
							<?php else : the_post_thumbnail( '265x265', array('itemprop'=>'image') );
							endif; ?>
						</a>
						<div class="magnifier">
							<div class="buttons">
								<h3 itemprop="name"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
								<hr>
								<p itemprop="keywords"><?php the_terms(get_the_id(), 'projects_category', '', ', '); ?></p>
							</div><!-- end buttons -->
						</div><!-- end magnifier -->
					</div><!-- end item -->
	
<?php endwhile; 

$content_output = ob_get_clean();

ob_start();?>

<div itemscope itemtype="http://schema.org/CreativeWork" class="<?php echo $grey; ?> clearfix">
	
	<?php if( $title ): ?>
	<div class="container">
		<div class="module">
			<div class="title text-center clearfix">
				<h2 itemprop="heading"><?php echo $title; ?></h2>
				<hr>
			</div><!-- end title -->
		</div>    
	</div>   
	<?php endif; ?>
	
	<?php if( $filters ): ?>
	<div class="container">
		<div class="text-center clearfix">
			<nav class="portfolio-filter">
				<ul>
					<li><a class="btn active btn-primary" href="#" data-filter="*"><span></span><?php _e('All', SH_NAME); ?></a></li>
					<?php echo implode("\n", $filters ); ?>
				</ul>
			</nav>
		</div><!-- end text-center --> 
	</div><!-- end container -->  
	<?php endif; ?>
	
	<div class="portfolio-wrapper">
		<div class="client-module">
			<div class="custom-wrapper">
				<div class="masonry_wrapper" data-col="5">
					<?php echo $content_output; ?>
				</div><!-- end masonry_wrapper -->
				
				<?php if( $link ): ?>
				<div class="container text-center">
					<div class="button-wrapper clearfix">
						<a href="<?php echo esc_url($link); ?>" title="<?php _e('View All Works', SH_NAME); ?>" class="btn btn-primary btn-dark"><?php _e('View All Works', SH_NAME); ?></a>
					</div><!-- end button-wrapper -->    
				</div>
				<?php endif; ?>
			</div><!-- end general-wrapper -->
		</div><!-- end custom-wrapper -->
	</div><!-- end module -->
</div><!-- end grey-wrapper -->

<?php wp_reset_query();
return ob_get_clean(); ?>

