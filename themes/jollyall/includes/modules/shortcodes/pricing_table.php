<?php 
$features = explode(',' , $fetures); 

$col_class = ( $cols == 3 ) ? 'col-lg-4 col-md-4 ' : 'col-lg-3 col-md-3 ';
$bg_color = ( $bg_class == 'grey-wrapper' ) ? ' content' : ''; 
$style = ( $style ) ? $style : 1;

ob_start();?>

					
					<div class="<?php echo $col_class; ?>col-sm-6 col-xs-12<?php echo $bg_color; ?>">
                    	
						<?php if( $style == 1 ): ?>
							
							<div class="pricing-table<?php if( $selected ) echo ' dark';?> text-center">
								
								<div class="pricing-header">
									<h3><?php echo $title; ?></h3>
									<span><sup><?php echo $symbol; ?></sup><?php echo $price; ?><em><?php echo $type; ?></em></span>
								</div><!-- end pricing-header -->
								
								<div class="pricing-body">
									<ul>
										<?php foreach($features as $f): ?>
											<li><?php echo $f ; ?></li>
										 <?php endforeach ; ?> 
									</ul>
									<div class="button-wrapper clearfix">
										<a href="<?php echo esc_url($btn_link); ?>" class="btn btn-primary"><?php echo $btn_text; ?></a>				
									</div>
								</div><!-- end pricing-body -->
							
							</div><!-- end pricing-table -->
						
						<?php else: ?>
						
							<div class="pricing-box<?php if( $selected ) echo ' dark';?>">
								<div class="title"><h3><?php echo $title; ?></h3></div>
								<hr>
								<div class="price">
									<p><?php echo $symbol; ?><?php echo $price; ?><?php echo $type; ?></p>
								</div> 
								<hr>
								<ul class="pricing clearfix">
									<?php foreach($features as $f): ?>
										<li><?php echo $f ; ?></li>
									 <?php endforeach ; ?> 
								</ul>
								<hr>
								<div class="button-wrapper clearfix">
									<a class="btn btn-primary" href="<?php echo esc_url($btn_link); ?>"><?php echo $btn_text; ?></a>
								</div>
							</div>
						
                    	<?php endif; ?>
						
					</div><!-- end col -->

<?php return ob_get_clean();

