<?php
$wrapper = ( $white ) ? 'white-wrapper' : 'grey-wrapper';
$query_args = array('post_type' => 'sh_services' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);

if( $cat ) $query_args['services_category'] = $cat;

$query = new WP_Query($query_args) ;
ob_start();?>

	<div class="<?php echo $wrapper; ?>">
		<div class="container">
			<div class="module clearfix">
            	<div class="tabbed_services text-center clearfix">
					
					<?php while( $query->have_posts() ): $query->the_post(); 
					
						$meta = _WSH()->get_meta(); ?>

						<div itemscope itemtype="https://schema.org/Service" class="col-md-4 col-sm-4 col-xs-12">
							<div class="service-box rounded_edition">
								<div class="icon-container">
									<i class="<?php echo sh_set( $meta, 'fontawesome' ); ?>"></i>
								</div><!-- icon-container -->
								<div class="title">

									<?php if( $link = sh_set( $meta, 'link' ) ): ?>
										<h3 itemprop="name"><a href="<?php echo esc_url( $link ); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
									<?php else: ?>
										<h3 itemprop="name"><?php the_title(); ?></h3>
									<?php endif; ?>

								</div><!-- end title -->

								<div class="desc">
									<p itemprop="text"><?php echo _sh_trim( get_the_excerpt(), $limit ); ?></p>
								</div><!-- end desc -->

							</div><!-- end service-box -->
						</div><!-- end col -->

					<?php endwhile; ?>
					
				
				</div><!-- end tabbed_service -->
                
            </div><!-- end module -->
		</div><!-- end container -->
	</div>


<?php wp_reset_query();
return ob_get_clean();

