<?php ob_start(); ?>

<?php if( $title ): ?>
	<div class="title">
    	<h3><?php echo $title; ?></h3>
	</div>
<?php endif; ?>
    
    <?php if( !is_user_logged_in() ): ?>
    
        <form action="<?php echo home_url(); ?>/wp-login.php?action=register" name="loginform" method="post" id="loginform1" class="form form-register" >
            
            <div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label"><?php _e('Username', SH_NAME); ?></label>
                <div class="col-sm-9 col-xs-12">
                    <input type="text" required placeholder="<?php _e('Username', SH_NAME); ?>" class="form-control required" name="user_login">
                </div>
            </div>
			
            <div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label"><?php _e('Email', SH_NAME); ?></label>
                <div class="col-sm-9 col-xs-12"> 
                    <input type="email" required placeholder="<?php _e('Email', SH_NAME);?>" class="form-control" name="user_email">
                </div>
            </div>
            <div class="form-group pull-left">
                <div class="checkbox">
                    <p id="reg_passmail"><?php _e('A password will be e-mailed to you.', SH_NAME); ?></p>
                </div>
            </div>
            <div class="form-group pull-right">
                <button class="btn btn-primary btn-lg" type="submit" name="wp-submit"><?php echo $button_text; ?></button>
				
            </div>
            
        </form>
		
    <?php else: ?>
    	<a href="<?php echo wp_logout_url(home_url()); ?>" title="<?php _e('Logout', SH_NAME); ?>" class="btn btn-primary"><?php _e('Logout', SH_NAME); ?></a>				
    <?php endif; ?>
    
	<div class="clearfix"></div>
	<hr>

<?php $output = ob_get_contents();
ob_end_clean();
return $output;