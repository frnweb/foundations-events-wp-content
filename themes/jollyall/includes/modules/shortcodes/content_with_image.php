<?php ob_start(); ?>


						<div id="tab3" class="tab-pane fade active in">
                            
							<?php if( $image ): ?>
							
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <?php echo wp_get_attachment_image( $image, 'full', '', array( 'class'=> 'img-responsive' ) ); ?>
                            </div><!-- end col -->
                            
							<?php endif; ?>
							
							<?php $content_class = ( $image ) ? 'col-lg-6 col-md-6 col-sm-6' : 'col-lg-12 col-md-12 col-sm-12 '; ?>
							<div class="<?php echo $content_class; ?>col-xs-12">
                                
								<?php if( $title ): ?>
								
								<div class="title">
                                    <h3><?php echo $title; ?></h3>
                                </div><!-- end title -->
								
								<?php endif; ?>
								
                                <div class="desc">
                                    <?php echo apply_filters( 'the_content', $contents ); ?>
                                </div><!-- end desc -->
                            </div><!-- end col-lg-6 -->
                        
						</div>

<?php return ob_get_clean(); ?>

