<?php //$cols = ( $cols ) ? $cols : 3 ;
$query_args = array('post_type' => 'sh_projects' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);

if( $cat ) $query_args['projects_category'] = $cat;

$query = new WP_Query($query_args) ; 

$mas_array = array( 0, 2, 3 );
$uniqid = uniqid();

wp_enqueue_script( array('masonry.pkgd.min', 'jquery-prettyPhoto')); 
ob_start();?>

<?php if( $bg ): ?> 
<div class="halfscreen parallax dark-wrapper" style="background-image: url('<?php echo wp_get_attachment_url( $bg ); ?>');" data-stellar-background-ratio="0.6" data-stellar-vertical-offset="20">
<?php else: ?>
<div class="grey-wrapper custom_bg">
<?php endif; ?>

	<div class="container">
		<div class="module clearfix">
			
			<?php if( $title ): ?>
			<div class="title wow zoomIn text-center clearfix">
				<h2><?php echo $title; ?></h2>
				<hr>
			</div><!-- end title -->
			<?php endif; ?>
			
			
			 <div class="masonry_wrapper_<?php echo $uniqid; ?> margin-top clearfix">
			 
			 	<?php $count = 0;
				while( $query->have_posts() ): $query->the_post(); ?>
				
					<div class="item-blog col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<div itemscope itemtype="http://schema.org/CreativeWork" class="portfolio-item-second entry">
						
							<?php if( $count > 5 ) $count = 0; ?>
							
							<?php if( in_array( $count, $mas_array ) ) the_post_thumbnail('434x570', array('class'=>'img-responsive', 'itemprop'=>'image' ) );
							else the_post_thumbnail('471x414', array('class'=>'img-responsive', 'itemprop'=>'image' ) ); ?>
							
							
							<div class="magnifier">
								
								<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
									<span class="icon"><i class="fa fa-link"></i></span>
								</a>
								
								<div class="buttons">
									<span><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><i class="fa fa-link"></i></a></span>
									<span><a href="javascript:void(0);" class="like_it" title="<?php _e('I Like It!', SH_NAME); ?>" data-id="<?php the_ID(); ?>"><i class="fa fa-heart"></i></a></span>
									<span><a href="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>" data-gal="prettyPhoto" title="<?php the_title_attribute(); ?>"><i class="fa fa-search"></i></a></span>                                     
									<h4 itemprop="name"><?php the_title(); ?></h4>
								</div><!-- end buttons -->
							
							</div><!-- end magnifier -->
						</div><!-- end portfolio-item -->
					</div>
				
				<?php $count++;
				endwhile; ?>
	
			</div><!-- end portfolio-masonry -->

			<?php if( $link ): ?>
			<div class="col-md-12 button-wrapper clearfix text-center">
				<?php $link_text = ( $link_txt ) ? $link_txt : __('View All Works', SH_NAME); ?>
				<a href="<?php echo esc_url( $link ); ?>" title="<?php echo esc_attr( $link_text ); ?>" class="btn btn-primary btn-dark"><?php echo esc_attr( $link_text ); ?></a>			
			</div><!-- end button-wrapper -->      
			<?php endif; ?>
				
		</div><!-- end module -->
	</div><!-- container -->
</div><!-- end white-wrapper -->

<script>
	jQuery(document).ready(function($) {
		"use strict";
		var container = document.querySelector('.masonry_wrapper_<?php echo $uniqid; ?>');
		var msnry = new Masonry( container, {
			columnWidth: 0,
			itemSelector: '.item-blog'
		});
	});
</script>


<?php wp_reset_query();
return ob_get_clean();




