<?php
$count = 1;
$query_args = array('post_type' => 'sh_services' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
if( $cat ) $query_args['services_category'] = $cat;
$query = new WP_Query($query_args) ; 
ob_start();?>


					<div class="module_widget">
                    	
						<?php if( $title ): ?>
						<div class="title">
                        	<h3><?php echo $title; ?></h3>
                        </div><!-- end title -->
						<?php endif; ?>
						
                        <?php if( $tagline ): ?>
						<div class="desc">
                        	<p><?php echo $tagline; ?></p>
                        </div><!-- end desc -->
						<?php endif; ?>
						
                    	
						<?php while( $query->have_posts() ): $query->the_post(); ?>
						
							<?php $meta = _WSH()->get_meta(); ?>
							
							<div itemscope itemtype="https://schema.org/Service" class="service-box black-edition">
								<div class="icon-container">
									<i class="<?php echo sh_set( $meta, 'fontawesome' ); ?>"></i>
								</div><!-- icon-container -->
								<div class="title">
								
									<?php if( $link = sh_set( $meta, 'link' ) ): ?>
										<h3 itemprop="name"><a href="<?php echo esc_url( $link ); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
									<?php else: ?>
										<h3 itemprop="name"><?php the_title(); ?></h3>
									<?php endif; ?>
								
								</div><!-- end title -->
								
								<div class="desc">
									<p itemprop="text"><?php echo _sh_trim( get_the_excerpt(), $limit ); ?></p>
								</div><!-- end desc -->
							
							</div><!-- end service-box -->
						
						<?php endwhile; ?>
                        
                    </div>

<?php wp_reset_query();
return ob_get_clean();



