
	<?php $terms = get_the_terms(get_the_id(), 'projects_category' ); 
	$first_tag = current( (array) $terms );
		$args=array(
			'projects_category' => $first_tag->slug,
			'post__not_in' => array($post->ID),
			'posts_per_page'=>4,
			'caller_get_posts'=>1
		);

	$my_query = new WP_Query($args);?>
	
	<?php if( $my_query->have_posts() ): ?>	

		<section class="grey-wrapper jt-shadow">
			<div class="container">
				<div class="general-title">
					<h2><?php _e('Related Projects', SH_NAME ); ?></h2>
					<hr>
				</div><!-- end general title -->
			</div><!-- end container -->
	
			<div class="portfolio_wrapper padding-top">
				
				<?php while( $my_query->have_posts() ): $my_query->the_post(); ?>
				
					<div class="portfolio_item">
						<div class="entry">
							<?php the_post_thumbnail( '400x393', array('class'=>'img-responsive' ) ); ?>

							<div class="magnifier">
								<div class="buttons">
									<a class="st btn btn-default" rel="bookmark" title="<?php the_title_attribute(); ?>" href="<?php the_permalink(); ?>"><?php _e('View project', SH_NAME); ?></a>
									<h3><?php the_title(); ?></h3>
								</div><!-- end buttons -->
							</div><!-- end magnifier -->
						</div><!-- end entry -->
					</div><!-- end portfolio_item -->
				
				<?php endwhile; ?>
				
			</div><!-- portfolio_wrapper -->
			<div class="clearfix"></div>
				
			<div class="buttons padding-top text-center">
				<a href="<?php echo home_url(); ?>" class="btn btn-primary btn-lg" title=""><?php _e('View All Projects',  SH_NAME); ?></a>
			</div>
		</section>
	
	<?php endif; ?>

