<?php ob_start(); ?>

<div class="white-wrapper">
	<div class="container">
		<div class="module clearfix">
			<div class="calloutbox clearfix" style="background-color:<?php echo $bg_color ; ?>">
				
				<div class="col-md-10">
					<?php if( $heading ): ?>
					<h2><?php echo $heading; ?></h2>
					<?php endif; ?>
					
					<?php if( $contents ): ?>
					<p class="lead"><?php echo do_shortcode($contents); ?></p>
					<?php endif; ?>

				</div>
								
				<?php if( $btn_html ): ?>
					<div class="col-md-2">
						<?php echo urldecode( base64_decode( $btn_html ) ); ?>
					</div>
				<?php endif; ?>
			
			</div>
		</div>
	</div>
</div>

<?php return ob_get_clean();