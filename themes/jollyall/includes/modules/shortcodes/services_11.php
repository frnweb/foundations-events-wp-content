<?php $query_args = array('post_type' => 'sh_services' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
if( $cat ) $query_args['services_category'] = $cat;
$query = new WP_Query($query_args) ; 

$col_md = ( $image ) ? 'col-lg-8 col-md-8 col-sm-12' : 'col-lg-12 col-md-12 col-sm-12';

ob_start();?>

	<div class="white-wrapper clearfix">
        <div class="container">
            <div class="module">

                <?php if( $title ): ?>
					<div class="title wow zoomIn text-center clearfix">
						<h2><?php echo $title; ?></h2>
						
						<?php if( $tagline ): ?>
							<hr>
							<p class="lead new-lead"><?php echo $tagline; ?></p>
						<?php endif; ?>
					</div><!-- end title -->
				<?php endif; ?>
				
                <div class="row margin-top">
                    
					<div class="<?php echo $col_md; ?> col-xs-12">
                        
						<?php while( $query->have_posts() ) : $query->the_post(); 
						
							$meta = _WSH()->get_meta(); ?>
						
							<div itemscope itemtype="https://schema.org/Service" class="col-md-6 col-sm-6 col-xs-12">
								
								<div class="service-box-new clearfix">
									<div class="icon-container wow zoomIn">
										<i class="<?php echo sh_set( $meta, 'fontawesome' ); ?>"></i>
									</div><!-- icon-container -->
									<div class="title">
										<?php if( $link = sh_set( $meta, 'link' ) ): ?>
											<h3 itemprop="name"><a href="<?php echo esc_url( $link ); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
										<?php else: ?>
											<h3 itemprop="name"><?php the_title(); ?></h3>
										<?php endif; ?>
									</div><!-- end title -->
									
									<div itemprop="text" class="desc">
										<p><?php echo _sh_trim( get_the_excerpt(), $limit, '.' ); ?></p>
									</div><!-- end desc -->
								</div>
								
							</div><!-- end col -->
							
							
						<?php endwhile; ?>
						
					</div>
					<?php if( $image ): ?>
					
						<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
							<div class="nomargin wow fadeInUp">
							
								<?php echo wp_get_attachment_image( $image, 'full', '', array( 'class' => 'img-responsive' ) ); ?>

							</div>
						</div>
					
					<?php endif; ?>
				</div><!-- end row -->
            </div><!-- end content --> 
        </div><!-- end container -->
    </div>


<?php wp_reset_query();
return ob_get_clean();