<?php ob_start();  ?>


<div class="halfscreen parallax" style="background-image: url('<?php echo wp_get_attachment_url( $bg ); ?>');" data-stellar-background-ratio="0.6" data-stellar-vertical-offset="20">
	<div class="overlay dark-version">
		<div class="container">
			<div class="module">
				<div class="welcome_message text-center clearfix">
					
					<h2><?php echo $heading; ?></h2>
					
					<p><?php echo $tagline; ?></p>
					
					<?php if( $btn_link ): ?>
					
					<div class="button-wrapper">
						<a href="<?php echo esc_url( $btn_link ); ?>" class="btn btn-primary btn-transparent"><?php echo ( $btn_text ) ? $btn_text : __('Hire Us',SH_NAME); ?></a>
					</div><!-- end button-wrapper -->
					
					<?php endif; ?>
				
				</div><!-- end welcome_message -->
			</div><!-- end module -->
		</div><!-- end container -->
	</div><!-- end overlay -->
</div><!-- end transparent-bg -->

<?php return ob_get_clean(); ?>
