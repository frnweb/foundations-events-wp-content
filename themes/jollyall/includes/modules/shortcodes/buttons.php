<?php 
ob_start(); ?>

<div class="magnifier">
	<div class="buttons">
		<span><a href="<?php echo $link1;?>"><i class="fa fa-link"></i></a></span>
		<span><a href="<?php echo $link2;?>"><i class="fa fa-heart-o"></i></a></span>
		<span><a href="<?php echo $link3;?>"><i class="fa fa-search"></i></a></span>                   
	</div><!-- end buttons -->
</div><!-- end magnifier -->

<?php wp_reset_query();

return ob_get_clean();