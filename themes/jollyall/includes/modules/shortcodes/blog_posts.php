<?php 
$query_args = array('post_type' => 'post' , 'showposts' => $num ,'order_by' => $sort , 'order' => $order , 'category' => $cat);
$query = new WP_Query($query_args) ; 


ob_start(); ?>

	<div class="white-wrapper clearfix">
		<div class="container">
			<div class="module">
				
				<?php if( $title ): ?>
				<div class="title wow zoomIn text-center clearfix animated">
					<h2><?php echo $title; ?></h2>
					<hr>
				</div><!-- end title -->
				<?php endif; ?>
				
    		</div><!-- end module -->
        </div><!-- end container -->
        
		<div class="clearfix">
			
			<?php while( $query->have_posts() ): $query->the_post(); ?>
			
				<div class="bigger-blog col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div itemscope itemtype="http://schema.org/BlogPosting" class="module-widget first">
					
						<div class="widget-title">
							<h4 itemprop="headline"><?php the_title(); ?></h4>
							<a itemprop="url" class="btn btn-primary btn-transparent" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php _e('Read More', SH_NAME); ?></a>		
						</div><!-- end title -->
						<?php the_post_thumbnail( '811x450', array('class'=>'img-responsive', 'itemprop' => 'image') ); ?>

					</div><!-- end bigger-blog -->
				</div><!-- end col -->
			
			<?php endwhile; ?>
			
			
		</div><!-- end row -->
	</div>


<?php wp_reset_query();

return ob_get_clean();