<?php ob_start(); ?>


							<div class="module_widget clearfix">
                                <div class="title">
                                    <h3>Piechart</h3>
                                </div><!-- end title -->
                                <div class="desc">
                                    <p>Duis non lectus sit amet est imperdiet cursus ame elementum vitae eros. Etiam adipiscingmorbi vitae magna tellus.</p>
                                </div><!-- end desc -->
                                <div class="widget-main">
                                    <div id="piechart-placeholder"></div>
                                </div>
							</div>

<!-- Pie Chart Elements -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.flot.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.flot.pie.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.flot.resize.min.js"></script>
<script type="text/javascript">
      jQuery(function($) {  
        var data = [
        { label: "Green",  data: 30.7, color: "#48CA3B"},
        { label: "Blue",  data: 24.5, color: "#00BCE1"},
        { label: "Purple",  data: 10.2, color: "#4D3A7D"},
        { label: "Red",  data: 18.6, color: "#AD1D28"},
        { label: "Yellow",  data: 16, color: "#DEBB27"}
        ];
      
        var placeholder = $('#piechart-placeholder').css({'width':'70%' , 'min-height':'150px'});
        $.plot(placeholder, data, {
        
        series: {
              pie: {
                  show: true,
            tilt:0.8,
            highlight: {
              opacity: 0.25
            },
            stroke: {
              color: '#fff',
              width: 2
            },
            startAngle: 2
            
              }
          },
          legend: {
              show: true,
          position: "ne", 
            labelBoxBorderColor: null,
          margin:[-30,15]
          }
        ,
        grid: {
          hoverable: true,
          clickable: true
        },
        tooltip: true, //activate tooltip
        tooltipOpts: {
          content: "%s : %y.1",
          shifts: {
            x: -30,
            y: -50
          }
        }
        
       });
      
        var $tooltip = $("<div class='tooltip top in' style='display:none;'><div class='tooltip-inner'></div></div>").appendTo('body');
        placeholder.data('tooltip', $tooltip);
        var previousPoint = null;
      
        placeholder.on('plothover', function (event, pos, item) {
        if(item) {
          if (previousPoint != item.seriesIndex) {
            previousPoint = item.seriesIndex;
            var tip = item.series['label'] + " : " + item.series['percent']+'%';
            $(this).data('tooltip').show().children(0).text(tip);
          }
          $(this).data('tooltip').css({top:pos.pageY + 10, left:pos.pageX + 10});
        } else {
          $(this).data('tooltip').hide();
          previousPoint = null;
        }
        
       });
      
        var d1 = [];
        for (var i = 0; i < Math.PI * 2; i += 0.5) {
          d1.push([i, Math.sin(i)]);
        }
      
        var d2 = [];
        for (var i = 0; i < Math.PI * 2; i += 0.5) {
          d2.push([i, Math.cos(i)]);
        }
      
        var d3 = [];
        for (var i = 0; i < Math.PI * 2; i += 0.2) {
          d3.push([i, Math.tan(i)]);
        }
      
        
      })
    </script>

<?php return ob_get_clean(); ?>
							