<?php //$Img_ids = explode(',' , $images); 
$col_md = ( $img ) ? 'col-ld-6 col-md-6 col-sm-6 ' : 'col-ld-12 col-md-12 col-sm-12 ';
ob_start() ;?>

		<div class="container">
    		<div class="module clearfix">
            	<div class="about-wrapper">
                    
					<?php if( $img ): ?>
					
						<div class="<?php echo $col_md; ?>col-xs-12">
							<div class="about_img">
							
								<?php echo wp_get_attachment_image( $img, 'full', '', array('class' => 'img-responsive' ) ); ?>
	
								<?php if( $name ): ?>
									<div class="title">
										<h3><?php echo $name; ?></h3>
									</div><!-- end title -->
								<?php endif; ?>
								
							</div><!-- end about img -->
						</div><!-- end col -->

					<?php endif; ?>
					    
                    <div class="<?php echo $col_md; ?>col-xs-12">
                        <div class="module_widget">
							
							<?php if( $title ): ?>
								<div class="title">
									<h3><?php echo $title; ?></h3>
								</div><!-- end title -->
							<?php endif; ?>
							
							<?php if( $contents ): ?>
								<div class="desc">
									<?php echo $contents; ?>
								</div><!-- end title -->
							<?php endif; ?>
                           
							
							<?php $query = new WP_Query( array( 'post_type'=>'sh_services', 'post__in'=>explode(',', $services ) ) );  ?>
							
                            <?php while( $query->have_posts() ): $query->the_post(); 
								
								$meta = _WSH()->get_meta(); ?>
							
								<div class="service-box black-edition">
									<div class="icon-container">
										<i class="<?php echo sh_set( $meta, 'fontawesome' ); ?>"></i>
									</div><!-- icon-container -->
									<div class="title">
										<h3><?php the_title(); ?></h3>
									</div><!-- end title -->
									<div class="desc">
										<p><?php echo _sh_trim( get_the_content(), 15 ); ?></p>
									</div><!-- end desc -->
								</div><!-- end service-box -->
							
                            <?php endwhile;
							wp_reset_query(); ?>
							
							
                           
                        </div><!-- end module widget -->
                    </div><!-- end col -->
                </div><!-- end about-wrapper -->
			</div><!-- end module -->
        </div><!-- end container -->  


<?php return ob_get_clean();


