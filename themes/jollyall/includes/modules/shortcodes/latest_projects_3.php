<?php 
if( !$cols ) $cols = 4;
wp_enqueue_script( array( 'jquery-prettyPhoto', 'jquery-isotope-min', 'custom-portfolio' ) );
$query_args = array('post_type' => 'sh_projects' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
//if( $cat ) $query_args['tax_query'] = array(array('taxonomy' => 'projects_category','field' => 'id','terms' => $cat));
if( $cat ) $query_args['projects_category'] = $cat;
$query = new WP_Query($query_args) ; 

ob_start();?>


<div class="white-wrapper clearfix">
	
	<?php if( $title ): ?>
		<div class="container">
			<div class="module">
				
				<div class="title wow zoomIn text-center clearfix">
					<h2><?php echo $title; ?></h2>
					<hr>
					<p class="lead"><?php echo $slogan; ?></p>
				</div><!-- end title -->
				
			</div><!-- end module --> 
		</div><!-- end container -->
	<?php endif; ?>
	
	<div class="portfolio_wrapper" data-cols="<?php echo $cols; ?>">
		
		<?php while( $query->have_posts() ): $query->the_post(); ?>
		
		<div itemscope itemtype="http://schema.org/CreativeWork" class="portfolio_item">
			
			<div class="entry">
			
				<?php the_post_thumbnail('471x414', array('class'=>'img-responsive', 'itemprop'=>'image' ) ); ?>
				
				<div class="magnifier">
					<div class="buttons">
						<h3>
						<a itemprop="name" title="<?php the_title_attribute(); ?>" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						</h3>
						<hr>
						<p><?php the_terms(get_the_id(), 'projects_category', '', ', ' ); ?></p>
					</div><!-- end buttons -->
				</div><!-- end magnifier -->
			</div><!-- end entry -->
		
		</div><!-- end portfolio_item -->
		
		<?php endwhile; ?>

	</div><!-- portfolio_wrapper -->
</div><!-- end white-wrapper -->

<?php wp_reset_query();
return ob_get_clean();
