

	<div id="header-container">
        <header class="header container transparent">
            <div class="menu-wrapper">
                <nav id="navigation" class="navbar yamm" role="navigation">
                    <div class="navbar-inner">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <i class="fa fa-bars fa-2x"></i>
                            </button>
							
							<?php do_action('_sh_logo', $options ) ; ?>

                        </div><!-- end navbar-header -->
                        <div id="navbar-collapse-1" class="navbar-right navbar-collapse collapse">
                            <ul class="nav navbar-nav  scrollable-menu">
							
								<?php wp_nav_menu( array( 
									'theme_location' => 'main_menu', 
									'container_id' => 'navbar-collapse-1',
									'container_class'=>'sidebmenu clearfix',
									'container' => false,
									'items_wrap' => '%3$s',
									'menu_class'=>'menu clearfix',
									'fallback_cb'=>false, 
								) ); ?>
                                
							</ul>
							
							<div class="custom-menu">
                                <?php echo sh_get_social_icons(); ?>
                                <span><a title="Search" href="#" data-toggle="modal" data-target=".search_modal"><i class="fa fa-search"></i></a></span>
                                
								<?php if( class_exists('woocommerce') ): ?>
								<span><a class="cart" title="Cart" href="#" data-toggle="modal" data-target=".cart_modal"><i class="fa fa-shopping-cart"></i></a></span>
                                <?php endif; ?>
								
								<?php if( sh_set( $options, 'header_sidebar' ) ): ?>
								<span><a class="right-menu" id="showbutton"><i class="fa fa-bars"></i></a></span>
                                <?php endif; ?>
								
							</div><!-- end custom-menu --> 
                        </div><!-- end navbar-callopse -->
                    </div><!-- end navbar-inner -->
                </nav><!-- end navigation -->
            </div><!-- menu wrapper -->
        </header><!-- end header -->
    </div><!-- end header container -->