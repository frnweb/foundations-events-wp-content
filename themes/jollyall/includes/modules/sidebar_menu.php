<?php $boxed = sh_set( $options, 'boxed_layout' );

if( is_page() ) {
	$meta = _WSH()->get_meta();//printr($meta);
	$boxed = sh_set( $meta, 'boxed' ) ? true : $boxed;
}

if( $boxed ) echo '<div class="container-fluid">';

if( sh_set( $options, 'header_sidebar' ) ): ?>

		<div id="contentdiv">
			<div id="slidediv">
				<a class="close-button" id="hidebutton"><i class="fa fa-times"></i></a>
				<div class="side-brand">
					<?php if( $sidebar_logo = sh_set( $options, 'sh_sidebar_logo' ) ): ?>
						<a href="<?php echo home_url(); ?>" title="<?php bloginfo('name'); ?>">
							<img src="<?php echo $sidebar_logo; ?>" alt="<?php bloginfo('name'); ?>">
						</a>
					<?php endif; ?>
				</div>
				<div class="sidebmenu clearfix">
				
					<?php wp_nav_menu( array( 
						'theme_location' => 'sidebar_menu', 
						'container_id' => 'navbar-collapse-1',
						'container_class'=>'sidebmenu clearfix',
						'container' => false,
						'menu_class'=>'menu clearfix',
						'fallback_cb'=>false, 
					) ); ?>
					
					<div class="custom-menu pull-left clearfix">
						<?php if( sh_set( $options, 'sh_sidebar_social_icons' ) ) 
							echo sh_get_social_icons(); ?>
						
						<?php if( sh_set( $options, 'sh_sidebar_search_icon' ) ): ?>
							<span><a title="<?php _e('Search', SH_NAME); ?>" href="#" data-toggle="modal" data-target=".search_modal"><i class="fa fa-search"></i></a></span>
						<?php endif; ?>
						
						<?php if( sh_set( $options, 'sh_sidebar_cart_icon' ) ): ?>
							<span><a class="cart" title="<?php _e('Cart', SH_NAME); ?>" href="#" data-toggle="modal" data-target=".cart_modal"><i class="fa fa-shopping-cart"></i></a></span>
						<?php endif; ?>
					
					</div><!-- end custom-menu -->
				</div><!-- end widget -->   
			</div>
		</div>
	
	<?php endif; ?>
	
	
	<div id="searhmodal" class="modal fade search_modal" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form class="searchform" id="searchform" role="form" method="get" action="<?php echo home_url(); ?>">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<input type="text" name="s" class="form-control" placeholder="<?php _e('What you are looking for?', SH_NAME); ?>" value="<?php echo get_search_query(); ?>">
				</form>
			</div>
		</div>
	</div>
	
	
	<div id="cartmodal" class="modal fade cart_modal" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content clearfix">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                
				<?php if( function_exists('WC') )
				if( WC()->cart->get_cart() ): ?>
				
					<div class="wc-header-cart">
						
					</div>
					
					<?php $cart_page = get_option( 'woocommerce_cart_page_id' ); ?>
					
					<?php if( $cart_page ): ?>
					<div class="buttons pull-right">
						<a class="btn btn-dark btn-sm" href="<?php echo get_permalink( $cart_page ); ?>" title="<?php echo get_the_title( $cart_page ); ?>"><?php _e('CHECKOUT', SH_NAME); ?></a>				
					</div>
					<?php endif; ?>
				<?php else: ?>
					<p><?php _e('There is no item in your cart', SH_NAME ); ?></p>
				<?php endif; ?>
			</div>
		</div>
	</div>