<?php
$options = array();


$options[] = array(
	'id'          => '_sh_seo_settings',
	'types'       => array('post', 'page', 'product', 'sh_services', 'sh_team', 'sh_projects'),
	'title'       => __('SEO Settings', SH_NAME),
	'priority'    => 'low',
	'template'    => 
			array(
					
					array(
						'type' => 'toggle',
						'name' => 'seo_status',
						'label' => __('Enable SEO', SH_NAME),
						'description' => __('Enable / disable seo settings for this post', SH_NAME),
					),
					array(
						'type' => 'textbox',
						'name' => 'title',
						'label' => __('Meta Title', SH_NAME),
						'description' => __('Enter meta title or leave it empty to use default title', SH_NAME),
					),
					
					array(
						'type' => 'textarea',
						'name' => 'description',
						'label' => __('Meta Description', SH_NAME),
						'default' => '',
						'description' => __('Enter meta description', SH_NAME),
					),
					array(
						'type' => 'textarea',
						'name' => 'keywords',
						'label' => __('Meta Keywords', SH_NAME),
						'default' => '',
						'description' => __('Enter meta keywords', SH_NAME),
					),
				),

); /** SEO fields for custom posts and pages */


$options[] = array(
	'id'          => '_sh_layout_settings',
	'types'       => array('post', 'page', 'product', 'sh_services', ),
	'title'       => __('Layout Settings', SH_NAME),
	'priority'    => 'high',
	'template'    => 
			array(
					
					array(
						'type' => 'radioimage',
						'name' => 'layout',
						'label' => __('Page Layout', SH_NAME),
						'description' => __('Choose the layout for blog pages', SH_NAME),

						'items' => array(
							array(
								'value' => 'left',
								'label' => __('Left Sidebar', SH_NAME),
								'img' => get_template_directory_uri().'/includes/vafpress/public/img/2cl.png',
							),

							array(
								'value' => 'right',
								'label' => __('Right Sidebar', SH_NAME),
								'img' => get_template_directory_uri().'/includes/vafpress/public/img/2cr.png',
							),
							array(
								'value' => 'full',
								'label' => __('Full Width', SH_NAME),
								'img' => get_template_directory_uri().'/includes/vafpress/public/img/1col.png',
							),
							
						),
					),
					
					array(
						'type' => 'select',
						'name' => 'sidebar',
						'label' => __('Sidebar', SH_NAME),
						'default' => '',
						'items' => sh_get_sidebars(true)	
					),
				),

);

$options[] = array(
	'id'          => '_sh_header_settings',
	'types'       => array('post', 'page', 'sh_portfolio', 'sh_team', 'download'),
	'title'       => __('Header Settings', SH_NAME),
	'priority'    => 'high',
	'template'    => 
			array(
					
					array(
						'type' => 'upload',
						'name' => 'bg_image',
						'label' => __('Header Background Image', SH_NAME),
						'description' => __('Choose the header background image', SH_NAME),
					),
					array(
						'type' => 'textbox',
						'name' => 'header_title',
						'label' => __('Header Title', SH_NAME),
						'description' => __('Enter header title', SH_NAME),
					),
					array(
						'type' => 'textbox',
						'name' => 'tagline',
						'label' => __('Header Tagline', SH_NAME),
						'default' => 'Swedish Wedding, photography | January 22, 2014',
						'description' => __('show small text under header main title', SH_NAME)
					),
				),

);

$options[] =  array(
	'id'          => _WSH()->set_meta_key('post'),
	'types'       => array('post'),
	'title'       => __('Post Settings', SH_NAME),
	'priority'    => 'high',
	'template'    => 
			array(
					 
					array(
						'type' => 'textarea',
						'name' => 'video',
						'label' => __('Video Embed Code', SH_NAME),
						'default' => '',
						'description' => __('If post format is video then this embed code will be used in content', SH_NAME)
					),
					array(
						'type' => 'textarea',
						'name' => 'audio',
						'label' => __('Audio Embed Code', SH_NAME),
						'default' => '',
						'description' => __('If post format is AUDIO then this embed code will be used in content', SH_NAME)
					),
					array(
						'type' => 'textarea',
						'name' => 'quote',
						'label' => __('Quote', SH_NAME),
						'default' => '',
						'description' => __('If post format is quote then the content in this textarea will be displayed', SH_NAME)
					),
							
					
			),
);

/* Page options */
$options[] =  array(
	'id'          => _WSH()->set_meta_key('page'),
	'types'       => array('page'),
	'title'       => __('Page Settings', SH_NAME),
	'priority'    => 'high',
	'template'    => 
			array(
					
					array(
						'type' => 'toggle',
						'name' => 'is_home',
						'label' => __('Don\'t Show Breadcrumb', SH_NAME),
						'default' => '',
						'description' => __('Dont\'t show breadcrumb banner area', SH_NAME)
					),
					array(
						'type' => 'toggle',
						'name' => 'boxed',
						'label' => __('Boxed', SH_NAME),
						'default' => '',
						'description' => __('Check to make the page boxed version', SH_NAME)
					),
					array(
						 'type' => 'upload',
						'name' => 'bg_pattorns',
						'label' => __( 'Upload Patorn Image', SH_NAME ),
						'item_max_height' => '150',
						'item_max_width' => '400',
						'dependency' => array(
							 'field' => 'boxed',
							'function' => 'vp_dep_boolean' 
						) 
					),
					
			),
);


/** Team Options*/
$options[] =  array(
	'id'          => _WSH()->set_meta_key('sh_team'),
	'types'       => array('sh_team'),
	'title'       => __('Team Options', SH_NAME),
	'priority'    => 'high',
	'template'    => array(
	
						
				array(
					'type' => 'textbox',
					'name' => 'designation',
					'label' => __('Designation', SH_NAME),
					'default' => '',
				),
				
				array(
					'type' => 'textbox',
					'name' => 'email',
					'label' => __('Email', SH_NAME),
					'default' => '',
					
				),
				array(
					'type' => 'sorter',
					'name' => 'services',
					'label' => __('Services', SH_NAME),
					'default' => array(),
					'items' => vp_get_posts_array( 'sh_services' )
					
				),
				array(
					'type' => 'textbox',
					'name' => 'skills_title',
					'label' => __('Skills Section Title', SH_NAME),
					'default' => __('My Skills', SH_NAME),
					'description' => __('Enter skill section title', SH_NAME)
				),
				array(
					'type' => 'upload',
					'name' => 'skill_bg',
					'label' => __('Skill Section Background', SH_NAME),
					'default' => '',
					'description' => __('Choose background parallax image fox skill section', SH_NAME)
					
				),
				array(
					'type' => 'textarea',
					'name' => 'skills',
					'label' => __('Skills', SH_NAME),
					'default' => 'Designing|68',
					'description' => __('Enter one skill per line. ( one skill is SKILL_NAME|PERCENT )', SH_NAME)
					
				),
				
				array(
					'type'      => 'group',
					'repeating' => true,
					'length'    => 1,
					'name'      => 'sh_team_social',
					'title'     => __('Social Profile', SH_NAME),
					'fields'    => array(
						
						array(
							'type' => 'fontawesome',
							'name' => 'social_icon',
							'label' => __('Social Icon', SH_NAME),
							'default' => '',
						),
						
						array(
							'type' => 'textbox',
							'name' => 'social_link',
							'label' => __('Link', SH_NAME),
							'default' => '',
							
						),
						
						
					),
				),

	),
);

/** Testimonial Options*/
$options[] =  array(
	'id'          => _WSH()->set_meta_key('sh_testimonials'),
	'types'       => array('sh_testimonials'),
	'title'       => __('Testimonials Options', SH_NAME),
	'priority'    => 'high',
	'template'    => array(
				array(
					'type' => 'textbox',
					'name' => 'designation',
					'label' => __('Designation', SH_NAME),
					'default' => '',
				),
				array(
					'type' => 'textbox',
					'name' => 'link',
					'label' => __('URL', SH_NAME),
					'default' => 'http://example.com',
				),
				array(
					'type' => 'textbox',
					'name' => 'company',
					'label' => __('Company', SH_NAME),
					'default' => 'Envato',
				)
	),
);



/** Projects Options*/
$options[] =  array(
	'id'          => _WSH()->set_meta_key('sh_projects'),
	'types'       => array('sh_projects'),
	'title'       => __('Projects Options', SH_NAME),
	'priority'    => 'high',
	'template'    => array(
				array(
					'type' => 'textarea',
					'name' => 'customer',
					'label' => __('Customer', SH_NAME),
					'default' => '',
					'description' => 'One customer per line e.g Smashing Magazine|http://smashingmagazine.com'
				),
				array(
					'type' => 'textbox',
					'name' => 'live_demo',
					'label' => __('Demo Link', SH_NAME),
					'default' => 'http://example.com',
				),
				array(
					'type' => 'textarea',
					'name' => 'skills',
					'label' => __('Skills (Comma seperated) ', SH_NAME),
					'default' => '',
				),
				array(
					'type' => 'toggle',
					'name' => 'full',
					'label' => __('Fullwidth', SH_NAME),
					'default' => '',
				),
				array(
					'type' => 'date',
					'name' => 'date',
					'label' => __('Launch Date', SH_NAME),
					'default' => '',
				),
				array(
					'type' => 'select',
					'name' => 'project_type',
					'label' => __('Type', SH_NAME),
					'default' => '',
					'items' => array(
									array(
										'value' => 'img',
										'label' => __('Image', SH_NAME),
									),
									array(
										'value' => 'gal',
										'label' => __('Gallery', SH_NAME),
									),
									array(
										'value' => 'audio',
										'label' => __('Audio', SH_NAME),
									),
									array(
										'value' => 'video',
										'label' => __('Video', SH_NAME),
									),
									
								),
					),
					
					array(
						'type' => 'textarea',
						'name' => 'video',
						'label' => __('Video Embed Code', SH_NAME),
						'default' => '',
						'description' => __('If Project Type is video then this embed code will be used in content', SH_NAME),
						/*'dependency' => array(
										'field' => 'project_type',
										'function' => 'vp_dep_is_video',
									),*/	
					),
					array(
						'type' => 'textarea',
						'name' => 'audio',
						'label' => __('Audio Embed Code', SH_NAME),
						'default' => '',
						'description' => __('If Project Type is AUDIO then this embed code will be used in content', SH_NAME),
						/*'dependency' => array(
										'field' => 'project_type',
										'function' => 'vp_dep_is_audio',
									),*/
					),	
									
	),
);



$options[] =  array(
	'id'          => _WSH()->set_meta_key('sh_gallery'),
	'types'       => array('sh_gallery'),
	'title'       => __('Image Gallery Settings', SH_NAME),
	'priority'    => 'high',
	'template'    => array(
		array(
			'name'  => 'use_image',
			'label' => 'Use Image Gallery',
			'type'  => 'toggle',
		),
		
				array(
					'type'      => 'group',
					'repeating' => true,
					'sortable'  => true,
					'name'      => 'column',
					'title'     => __('Images', SH_NAME),
					'fields'    => array(
						array(
							'type'  => 'textbox',
							'name'  => 'image_title',
							'label' => __('Title', SH_NAME),
						),
						array(
							'type'                       => 'upload',
							'label'                      => __('Image', SH_NAME),
							'name'                       => 'image',
							'use_external_plugins'       => 1,

							'validation'                 => 'required',
						),
						array(
							'type'                       => 'textarea',
							'label'                      => __('Content', SH_NAME),
							'name'                       => 'content',
							'use_external_plugins'       => 1,
							'disabled_externals_plugins' => 'vp_sc_button',
							'disabled_internals_plugins' => '',
							'validation'                 => 'required',
						),
					),
				
		),
	),
);

 $options[] =  array(
	'id'          => _WSH()->set_meta_key('sh_gallery'),
	'types'       => array('sh_gallery'),
	'title'       => __('Video Gallery Settings', SH_NAME),
	'priority'    => 'high',
	'template'    => array(
		array(
			'name'  => 'use_video',
			'label' => 'Use Videos',
			'type'  => 'toggle',
		),
		
				array(
					'type'      => 'group',
					'repeating' => true,
					'sortable'  => true,
					'name'      => 'column',
					'title'     => __('Video', SH_NAME),
					'fields'    => array(
						array(
							'type'  => 'textbox',
							'name'  => 'video_title',
							'label' => __('Title', SH_NAME),
						),
						array(
							'type'                       => 'textbox',
							'label'                      => __('Video Link', SH_NAME),
							'name'                       => 'link',
							'use_external_plugins'       => 1,

							'validation'                 => 'required',
						),
						array(
							'type'                       => 'textarea',
							'label'                      => __('Content', SH_NAME),
							'name'                       => 'v_content',
							'use_external_plugins'       => 1,
							'disabled_externals_plugins' => 'vp_sc_button',
							'disabled_internals_plugins' => '',
							'validation'                 => 'required',
						),
					),
				
		),
	),
);
$options[] =  array(
	'id'          => _WSH()->set_meta_key('sh_services'),
	'types'       => array( 'sh_services' ),
	'title'       => __('Post Settings', SH_NAME),
	'priority'    => 'high',
	'template'    => 

			array(
				
				array(
					'type' => 'fontawesome',
					'name' => 'fontawesome',
					'label' => __('Service Icon', SH_NAME),
					'default' => '',
				),
				array(
					'type' => 'textbox',
					'name' => 'link',
					'label' => __('Service Link', SH_NAME),
					'default' => '',
					'description' => __('Enter link to detail page', SH_NAME)
				),
				array(
					'type' => 'textbox',
					'name' => 'hire_link',
					'label' => __('Hire Link', SH_NAME),
					'default' => '',
					'description' => __('Enter link to Hiring page', SH_NAME)
				),
			),
);

/**
 * EOF
 */
 
 
 return $options;
 
 
 
 
 
 
 
 