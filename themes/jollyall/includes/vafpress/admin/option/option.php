<?php
return array(
     'title' => __( 'JollyAll Theme Options', SH_NAME ),
    'logo' => get_template_directory_uri() . '/images/dark_logo.png',
    'menus' => array(

        // General Settings
         array(
             'title' => __( 'General Settings', SH_NAME ),
            'name' => 'general_settings',
            'icon' => 'font-awesome:fa fa-cogs',
            'menus' => array(
                 
				 array(
                     'title' => __( 'General Settings', SH_NAME ),
                    'name' => 'general_settings',
                    'icon' => 'font-awesome:fa fa-cogs',
                    'controls' => array(
                         array(
                             'type' => 'section',
                            'repeating' => true,
                            'sortable' => true,
                            'title' => __( 'Color Scheme', SH_NAME ),
                            'name' => 'color_schemes',
                            'description' => __( 'This section is used for theme color settings', SH_NAME ),
                            'fields' => array(
                                 
                                array(
                                    'type' => 'color',
                                    'name' => 'custom_color_scheme',
                                    'label' => __( 'Color Scheme', SH_NAME ),
                                    'description' => __( 'Choose the Custom color scheme for the theme.', SH_NAME ),
                                    'default' => '',
                                    /*'dependency' => array(
                                         'field' => 'color_selection',
                                        'function' => 'vp_dep_is_custom_color' 
                                    ) */
                                ),
                                  
                                array(
                                    'type' => 'toggle',
                                    'name' => 'boxed_layout',
                                    'label' => __( 'Boxed Layout', SH_NAME ),
                                    'description' => __( 'Turn Boxed Layout On.', SH_NAME ) 
                                ),
								
								array(
                                    'type' => 'upload',
                                    'name' => 'boxed_bg',
                                    'label' => __( 'Boxed Pattern', SH_NAME ),
                                    'description' => __( 'Choose boxed pattern.', SH_NAME ),
									'default' => get_template_directory_uri().'/images/bg.png' 
                                ),
                                
                                
                            ) 
                        ),
						
						/*array(
							 'type' => 'toggle',
							'name' => 'ajax_loader',
							'label' => __( 'Enable Ajax Loader', SH_NAME ),
							'description' => __( 'Turn ajax loader on.', SH_NAME ) 
						),
						array(
                             'type' => 'select',
                            'name' => 'ajax_loader_images',
                            'label' => __( 'Choose Loading Icon', SH_NAME ),
                            'description' => __( 'Choose loading icon from the dropdown.', SH_NAME ),
                            'deafault' => 'Grey Circle Ball.gif',
                            'items' => array(
								 'data' => array(
									 array(
										 'source' => 'function',
										'value' => 'vp_get_loader_images' 
									) 
								) 
							),
                            'dependency' => array(
                                'field' => 'ajax_loader',
                                'function' => 'vp_dep_boolean' 
                            ) 
                        ),*/
						/*array(
                             'type' => 'section',
                            'repeating' => true,
                            'sortable' => true,
                            'title' => __( 'Purchase Information', SH_NAME ),
                            'name' => 'purchase_information',
                            'description' => __( 'To get the auto theme updates provide purchase information', SH_NAME ),
                            'fields' => array(
                                 
                                array(
                                    'type' => 'textbox',
                                    'name' => 'sh_purchase_code',
                                    'label' => __( 'Purchase Code', SH_NAME ),
                                    'description' => __( 'To find the purchase code to TF downloads tab click on Download then choose "License and Purchase code"', SH_NAME ),
                                    'default' => '',
                                ),
								array(
                                    'type' => 'textbox',
                                    'name' => 'sh_purchase_user',
                                    'label' => __( 'Themeforest Username', SH_NAME ),
                                    'description' => __( 'Enter your themeforest username', SH_NAME ),
                                    'default' => '',
                                ),
                                
                            ) 
                        ),*/
						
						array(
                            'type' => 'toggle',
                            'name' => 'sticky_menu',
                            'label' => __( 'Enable Sticky Menu', SH_NAME ),
                            'description' => __( 'Enable or disable sticky menu', SH_NAME ),
                            'default' => 0 
                        ),
						
						array(
                             'type' => 'toggle',
                            'name' => 'preloader',
                            'label' => __( 'Enable Preloader', SH_NAME ),
                            'description' => __( 'Enable or disable preloader', SH_NAME ),
                            'default' => 0 
                        ),
						
						
                    ) 
                ),
                
				/** Submenu for heading settings */
                array(
                     'title' => __( 'Header Settings', SH_NAME ),
                    'name' => 'header_settings',
                    'icon' => 'font-awesome:fa fa-dashboard',
                    'controls' => array(
                        array(
                             'type' => 'upload',
                            'name' => 'site_favicon',
                            'label' => __( 'Favicon', SH_NAME ),
                            'description' => __( 'Upload the favicon, should be 16x16', SH_NAME ),
                            'default' => '' 
                        ),
                        array(
                            'type' => 'toggle',
                            'name' => 'header_sidebar',
                            'label' => __( 'Sidebar Menu', SH_NAME ),
                            'description' => __( 'Enable / Disable sidebar menu', SH_NAME ),
                            'default' => 0 
                        ),
						array(
                            'type' => 'section',
                            'title' => __( 'Sidebar Menu Settings', SH_NAME ),
                            'name' => 'sidebar_menu_section',
                            'description' => __( 'Sidebar menu section', SH_NAME ),
							'dependency' => array(
                                'field' => 'header_sidebar',
                                'function' => 'vp_dep_boolean' 
                            ),
                            'fields' => array(
                                 
                                array(
                                    'type' => 'upload',
                                    'name' => 'sh_sidebar_logo',
                                    'label' => __( 'Sidebar Logo', SH_NAME ),
                                    'description' => __( 'choose the sidebar logo', SH_NAME ),
                                    'default' => get_template_directory_uri().'/images/side-logo.png',
                                ),
								array(
                                    'type' => 'toggle',
                                    'name' => 'sh_sidebar_social_icons',
                                    'label' => __( 'Social Icons', SH_NAME ),
                                    'description' => __( 'enable / disable social icons', SH_NAME ),
                                    'default' => 0,
                                ),
								array(
                                    'type' => 'toggle',
                                    'name' => 'sh_sidebar_search_icon',
                                    'label' => __( 'Search Icon', SH_NAME ),
                                    'description' => __( 'enable / disable search icon', SH_NAME ),
                                    'default' => 0,
                                ),
								
								array(
                                    'type' => 'toggle',
                                    'name' => 'sh_sidebar_cart_icon',
                                    'label' => __( 'Cart Icon', SH_NAME ),
                                    'description' => __( 'enable / disable cart icon', SH_NAME ),
                                    'default' => 0,
                                ),
                                
                            ) 
                        ),
                                                
                        
						
                        array(
                             'type' => 'section',
                            'title' => __( 'Logo with Image', SH_NAME ),
                            'name' => 'logo_with_image',
                            
                            'fields' => array(
                                 array(
                                     'type' => 'upload',
                                    'name' => 'logo_image',
                                    'label' => __( 'Logo Image', SH_NAME ),
                                    'description' => __( 'Upload the logo image', SH_NAME ),
                                    'default' => get_template_directory_uri() . '/images/logo.png' 
                                ),
                                array(
                                     'type' => 'slider',
                                    'name' => 'logo_width',
                                    'label' => __( 'Logo Width', SH_NAME ),
                                    'description' => __( 'choose the logo width', SH_NAME ),
                                    'default' => '238',
                                    'min' => 20,
                                    'max' => 400 
                                ),
                                array(
                                     'type' => 'slider',
                                    'name' => 'logo_height',
                                    'label' => __( 'Logo Height', SH_NAME ),
                                    'description' => __( 'choose the logo height', SH_NAME ),
                                    'default' => '56',
                                    'min' => 20,
                                    'max' => 400 
                                ) 
                                
                            ) 
                        ),
						
                        
						 array(
							 'type' => 'radioimage',
							'name' => 'custom_header',
							'label' => __( 'Choose Header', SH_NAME ),
							'item_max_height' => '150',
							'item_max_width' => '400',
							'items' => array(
								 array(
									 'value' => 'default',
									'label' => __( 'Default Header', SH_NAME ),
									'img' => SH_URL . '/images/header1.png' 
								),
								array(
									 'value' => 'center_logo',
									'label' => __( 'Centered Logo', SH_NAME ),
									'img' => SH_URL . '/images/header3.png' 
								),
								
							),
							'default' => 'default'
						), 
                                
                           
                        // Custom HEader Style End
                        array(
                             'type' => 'codeeditor',
                            'name' => 'header_css',
                            'label' => __( 'Header CSS', SH_NAME ),
                            'description' => __( 'Write your custom css to include in header.', SH_NAME ),
                            'theme' => 'github',
                            'mode' => 'css' 
                        ) 
                    ) 
                    
                ),
                
                /** Submenu for footer area */
                array(
                     'title' => __( 'Footer Settings', SH_NAME ),
                    'name' => 'sub_footer_settings',
                    'icon' => 'font-awesome:fa fa-edit',
                    'controls' => array(
                        
                        array(
                             'type' => 'toggle',
                            'name' => 'show_footer_1',
                            'label' => __( 'Show Footer 1', SH_NAME ),
							'default' => 1,
							'description' => __('show or hide the footer 1 area', SH_NAME)
							
                        ),
						
						array(
                             'type' => 'toggle',
                            'name' => 'show_footer_2',
                            'label' => __( 'Show Footer 2', SH_NAME ),
							'default' => 1,
							'description' => __('show or hide the footer 2 area', SH_NAME)
							
                        ),
                        
                        array(
                             'type' => 'textarea',
                            'name' => 'copyright_text',
                            'label' => __( 'Copyright Text', SH_NAME ),
                            'description' => __( 'Enter the Copyright Text', SH_NAME ),
                            'default' => 'Copyright &copy; 2014 - Designed by Jolly Themes' 
                        ),
                        array(
                             'type' => 'codeeditor',
                            'name' => 'footer_analytics',
                            'label' => __( 'Footer Analytics / Scripts', SH_NAME ),
                            'description' => __( 'In this area you can put Google Analytics Code or any other Script that you want to be included in the footer before the Body tag.', SH_NAME ),
                            'theme' => 'twilight',
                            'mode' => 'javascript' 
                        ) 
                        
                        
                        
                    ) 
                ), //End of submenu
                
                
				array(
                     'title' => __( 'Twitter Settings', SH_NAME ),
                    'name' => 'sub_twitter_settings',
                    'icon' => 'font-awesome:fa fa-twitter',
                    'controls' => array(
                        
                         array(
                             'type' => 'textbox',
                            'name' => 'twitter_api',
                            'label' => __( 'API', SH_NAME ),
                            'description' => __( 'Enter Twitter API key Here. to get twitter api <a href="http://dev.twitter.com">Go To</a> and create a new app', SH_NAME ),
                            'default' => 'EAVuZPOFDqh6YJRoIUn4danY8' 
                        ),
                        
                        array(
                             'type' => 'textbox',
                            'name' => 'twitter_api_secret',
                            'label' => __( 'API Secret', SH_NAME ),
                            'description' => __( 'Enter Twitter API Secret Here.', SH_NAME ),
                            'default' => 'HZ5lBxAooSWbLIyva9SioNbzLoPfzk9yKxLscMUGRwGt5XzIyv' 
                        ),
                        array(
                             'type' => 'textbox',
                            'name' => 'twitter_token',
                            'label' => __( 'Token', SH_NAME ),
                            'description' => __( 'Enter Twitter Token here.', SH_NAME ),
                            'default' => '2595337447-sQiBf41a0BYokzTyULmP6LDpC28MU6ajCtllgHq' 
                        ),
                        array(
                             'type' => 'textbox',
                            'name' => 'twitter_token_Secret',
                            'label' => __( 'Token Secret', SH_NAME ),
                            'description' => __( 'Enter Token Secret', SH_NAME ),
                            'default' => 'tjeQG0UiRZLJLua9phO0eVMv5ZpQRvx4Z0dS4b3dwEpk7' 
                        ) 
                    ) 
                ), //End of submenu
                
                array(
                     'title' => __( 'Permalinks Settings', SH_NAME ),
                    'name' => 'permalinks_settings',
                    'icon' => 'font-awesome:fa fa-link',
                    'controls' => array(
                         array(
                             'type' => 'section',
                            'name' => 'post_type_permalink_section',
                            'title' => 'Post Type Permalinks',
                            'fields' => array(
                                 array(
                                     'type' => 'textbox',
                                    'name' => 'team_permalink',
                                    'label' => __( 'Team Permalink', SH_NAME ),
                                    'description' => __( 'Enter Slug for Team Post Type.', SH_NAME ),
                                    'default' => '' 
                                ),
                                
                                array(
                                     'type' => 'textbox',
                                    'name' => 'services_permalink',
                                    'label' => __( 'Services Permalink', SH_NAME ),
                                    'description' => __( 'Enter Slug for Services Post Type.', SH_NAME ),
                                    'default' => '' 
                                ),
                                array(
                                     'type' => 'textbox',
                                    'name' => 'projects_permalink',
                                    'label' => __( 'Projects Permalink', SH_NAME ),
                                    'description' => __( 'Enter Permalink for Projects Post Type.', SH_NAME ),
                                    'default' => '' 
                                ),
                                array(
                                     'type' => 'textbox',
                                    'name' => 'gallery_permalink',
                                    'label' => __( 'Gallery Permalink', SH_NAME ),
                                    'description' => __( 'Enter Slug for Gallery Post Type.', SH_NAME ),
                                    'default' => '' 
                                ),
                                array(
                                     'type' => 'textbox',
                                    'name' => 'testimonial_permalink',
                                    'label' => __( 'Testimonial Permalink', SH_NAME ),
                                    'description' => __( 'Enter Permalink for Testimonial Post Type.', SH_NAME ),
                                    'default' => '' 
                                ) 
                            ) 
                        ),
                        array(
                             'type' => 'section',
                            'name' => 'category_permalink_section',
                            'title' => 'Category Permalinks',
                            'fields' => array(
                                 array(
                                     'type' => 'textbox',
                                    'name' => 'team_category_permalink',
                                    'label' => __( 'Team Category Permalink', SH_NAME ),
                                    'description' => __( 'Enter Slug for Team Taxonomy.', SH_NAME ),
                                    'default' => '' 
                                ),
                                
                                array(
                                     'type' => 'textbox',
                                    'name' => 'services_category_permalink',
                                    'label' => __( 'Services Category Permalink', SH_NAME ),
                                    'description' => __( 'Enter Slug for Services Taxonomy.', SH_NAME ),
                                    'default' => '' 
                                ),
                                array(
                                     'type' => 'textbox',
                                    'name' => 'projects_category_permalink',
                                    'label' => __( 'Projects Category Permalink', SH_NAME ),
                                    'description' => __( 'Enter Permalink for Projects Taxonomy.', SH_NAME ),
                                    'default' => '' 
                                ),
                                
                                array(
                                     'type' => 'textbox',
                                    'name' => 'gallery_category_permalink',
                                    'label' => __( 'Gallery Category Permalink', SH_NAME ),
                                    'description' => __( 'Enter Slug for Gallery Taxonomy.', SH_NAME ),
                                    'default' => '' 
                                ),
                                array(
                                     'type' => 'textbox',
                                    'name' => 'testimonial_category_permalink',
                                    'label' => __( 'Testimonial Category Permalink', SH_NAME ),
                                    'description' => __( 'Enter Permalink for Testimonial Taxonomy.', SH_NAME ),
                                    'default' => '' 
                                ) 
                            ) 
                        ) 
                    ) 
                ) //End of submenu
            ) 
        ),
        
		// SEO General settings Settings
        array(
             'title' => __( 'SEO Settings', SH_NAME ),
            'name' => 'seo_settings',
            'icon' => 'font-awesome:fa fa-search',
			
			'controls' => array(
				
				array(
					 'type' => 'toggle',
					'name' => '_enable_seo',
					'label' => __( 'Enable SEO', SH_NAME ),
					'description' => __( 'Enable or disable seo settings', SH_NAME ),
					'default' => 1 
				),
				array( 
				 		'type' => 'section',
						'title' => __( 'Homepage Settings', SH_NAME ),
						'name' => '_seo_homepage_settings',
						'description' => __( 'homepage meta title, meta description and meta keywords', SH_NAME ),
						'fields' => array(
								array(
									 'type' => 'textbox',
									'name' => '_seo_home_meta_title',
									'label' => __( 'Meta Title', SH_NAME ),
									'description' => __( 'Enter the Title you want to show on home page', SH_NAME ),
									'default' => ''
								),
								array(
									'type' => 'textarea',
									'name' => '_seo_home_meta_description',
									'label' => __( 'Meta Description', SH_NAME ),
									'description' => __( 'Enter the meta description for homepage', SH_NAME ),
									'default' => ''
								),
								array(
									'type' => 'textarea',
									'name' => '_seo_home_meta_keywords',
									'label' => __( 'Meta Keywords', SH_NAME ),
									'description' => __( 'Enter the comma separated keywords for homepage', SH_NAME ),
									'default' => ''
								),
						),
				 ), /** End of homepage seo settings */
				 
				 array( 
				 		'type' => 'section',
						'title' => __( 'Archive Pages Settings', SH_NAME ),
						'name' => '_seo_archive_settings',
						'description' => __( 'archive pages meta title, meta description and meta keywords', SH_NAME ),
						'fields' => array(
								array(
									 'type' => 'textbox',
									'name' => '_seo_archive_meta_title',
									'label' => __( 'Meta Title', SH_NAME ),
									'description' => __( 'Enter the Title you want to show on home page', SH_NAME ),
									'default' => ''
								),
								array(
									'type' => 'textarea',
									'name' => '_seo_archive_meta_description',
									'label' => __( 'Meta Description', SH_NAME ),
									'description' => __( 'Enter the meta description for homepage', SH_NAME ),
									'default' => ''
								),
								array(
									'type' => 'textarea',
									'name' => '_seo_archive_meta_keywords',
									'label' => __( 'Meta Keywords', SH_NAME ),
									'description' => __( 'Enter the comma separated keywords for homepage', SH_NAME ),
									'default' => ''
								),
						),
				 ),/** End of archive page settings */
				
			), /** End of controls */
				
		), /** End of seo page settings */
		
		
		// WP Login Page Settings
        array(
             'title' => __( 'WP Login', SH_NAME ),
            'name' => 'wp_login_settings',
            'icon' => 'font-awesome:fa fa-search',
			
			'controls' => array(
				
				array(
					 'type' => 'toggle',
					'name' => '_enable_wp_login',
					'label' => __( 'Enable Custom WP Login', SH_NAME ),
					'description' => __( 'Enable custom styling of wp login page', SH_NAME ),
					'default' => 0 
				),
				array( 
				 		'type' => 'section',
						'title' => __( 'WP Login Page Settings', SH_NAME ),
						'name' => '_wp_login_page_settings',
						'description' => __( 'homepage meta title, meta description and meta keywords', SH_NAME ),
						'fields' => array(
								array(
									 'type' => 'textbox',
									'name' => '_wp_login_page_title',
									'label' => __( 'Page Logo Title', SH_NAME ),
									'description' => __( 'Enter logo title for login page', SH_NAME ),
									'default' => get_bloginfo( 'name' )
								),
								array(
									'type' => 'upload',
									'name' => '_wp_login_logo',
									'label' => __( 'Logo', SH_NAME ),
									'description' => __( 'Choose the logo', SH_NAME ),
									'default' => get_template_directory_uri().'/images/mlogo.png'
								),
								array(
									'type' => 'upload',
									'name' => '_wp_login_bg',
									'label' => __( 'Background Image', SH_NAME ),
									'description' => __( 'Choose the background image', SH_NAME ),
									'default' => get_template_directory_uri().'/images/mbg_01.png'
								),
						),
				 ), /** End of wp login page settings */
				 
				 
				
			), /** End of controls */
				
		), /** End of login page settings */
		
		// Client and brand settings
        array(
             'title' => __( 'Clients', SH_NAME ),
            'name' => 'brand_or_client',
            'icon' => 'font-awesome:fa fa-star',
            'controls' => array(
                 array(
                     'type' => 'builder',
                    'repeating' => true,
                    'sortable' => true,
                    'label' => __( 'Clients', SH_NAME ),
                    'name' => 'brand_or_client',
                    'description' => __( 'Add as many clients as you want', SH_NAME ),
                    'fields' => array(
                         array(
                             'type' => 'textbox',
                            'name' => 'title',
                            'label' => __( 'Name', SH_NAME ),
                            'description' => __( 'Enter the name of brand or client.', SH_NAME ),
                            'default' => '' 
                         ),
						 array(
                             'type' => 'upload',
                            'name' => 'brand_img',
                            'label' => __( 'Logo', SH_NAME ),
                            'description' => __( 'choose the brand logo.', SH_NAME ),
                            'default' => '' 
                         ),
						 array(
                             'type' => 'textbox',
                            'name' => 'link',
                            'label' => __( 'URL', SH_NAME ),
                            'description' => __( 'Enter brand link e.g http://example.com .', SH_NAME ),
                            'default' => '' 
                         ),
                    ) 
                ) 
            ) 
        ),
		
		// Ads settings
        /*array(
             'title' => __( 'Ads Settings', SH_NAME ),
            'name' => 'ads_settings',
            'icon' => 'font-awesome:fa fa-thumbs-up',
            'controls' => array(
                 array(
                     'type' => 'builder',
                    'repeating' => true,
                    'sortable' => true,
                    'label' => __( 'Ads Management', SH_NAME ),
                    'name' => 'ads_management',
                    'description' => __( 'create your ads here', SH_NAME ),
                    'fields' => array(
                         array(
                             'type' => 'textbox',
                            'name' => 'title',
                            'label' => __( 'Name', SH_NAME ),
                            'description' => __( 'Enter the name of brand or client.', SH_NAME ),
                            'default' => '' 
                         ),
						 array(
							'type' => 'select',
							'name' => 'ad_type',
							'label' => __( 'Ad type', SH_NAME ),
							'description' => __( 'Choose what the type of ad', SH_NAME ),
							'items' => array(
								 array(
									 'value' => 'image',
									'label' => __( 'Image', SH_NAME ),
								),
								array(
									 'value' => 'script',
									'label' => __( 'Custom Script / Text', SH_NAME ),
								) 
							),
							'default' => 'image' 
						),
						 array(
                             'type' => 'upload',
                            'name' => 'brand_img',
                            'label' => __( 'Logo', SH_NAME ),
                            'description' => __( 'choose the brand logo.', SH_NAME ),
                            'default' => '',
							
                         ),
						 array(
                             'type' => 'textbox',
                            'name' => 'link',
                            'label' => __( 'URL', SH_NAME ),
                            'description' => __( 'Enter brand link e.g http://example.com .', SH_NAME ),
                            'default' => '',
							
                         ),
						 array(
                            'type' => 'textarea',
                            'name' => 'custom_script',
                            'label' => __( 'Custom Script', SH_NAME ),
                            'description' => __( 'Enter custom or you can use html tags', SH_NAME ),
                            'default' => '',
							
                         ),
                    ) 
                ) 
            ) 
        ),*/
		
		// Client and brand settings
        array(
            'title' => __( 'Skills', SH_NAME ),
            'name' => 'our_skills',
            'icon' => 'font-awesome:fa fa-star',
            'controls' => array(
                 array(
                     'type' => 'builder',
                    'repeating' => true,
                    'sortable' => true,
                    'label' => __( 'Skills', SH_NAME ),
                    'name' => 'powerful_skills',
                    'description' => __( 'company powerful skills', SH_NAME ),
                    'fields' => array(
                         array(
                             'type' => 'textbox',
                            'name' => 'title',
                            'label' => __( 'Skill Name', SH_NAME ),
                            'description' => __( 'Enter the name of skill.', SH_NAME ),
                            'default' => '' 
                         ),
						 array(
                             'type' => 'slider',
                            'name' => 'percent',
                            'label' => __( 'Skill Percentage', SH_NAME ),
                            'description' => __( 'choose the skill percentage.', SH_NAME ),
                            'default' => 50,
							'min' => 20,
                            'max' => 400 
                         ),
						 
                    ) 
                ) 
            ) 
        ),
		
		// Pages , Blog Pages Settings
        array(
             'title' => __( 'Page Settings', SH_NAME ),
            'name' => 'general_settings',
            'icon' => 'font-awesome:fa fa-file',
            'menus' => array(
                
                // Search Page Settings 
                 array(
                     'title' => __( 'Search Page', SH_NAME ),
                    'name' => 'search_page_settings',
                    'icon' => 'font-awesome:fa fa-search',
                    'controls' => array(
                         array(
                             'type' => 'textbox',
                            'name' => 'search_page_title',
                            'label' => __( 'Page Title', SH_NAME ),
                            'description' => __( 'Enter the Title you want to show on search page', SH_NAME ),
                            'default' => 'Search Results' 
                        ),
						array(
                             'type' => 'upload',
                            'name' => 'search_page_header_img',
                            'label' => __( 'Header image', SH_NAME ),
                            'description' => __( 'Choose the image you want to show on search page', SH_NAME ),
                            //'default' => 'Search Results' 
                        ),
						array(
							'type' => 'radioimage',
							'name' => 'search_page_view',
							'label' => __('Page View', SH_NAME),
							'description' => __('Choose whether images in left or top', SH_NAME),
							'items' => array(
								array(
									'value' => 'style1',
									'label' => __('Images in Left', SH_NAME),
									'img' => get_template_directory_uri().'/includes/vafpress/public/img/grid-alt.png',
								),
								array(
									'value' => 'style2',
									'label' => __('Images at Top', SH_NAME),
									'img' => get_template_directory_uri().'/includes/vafpress/public/img/list3.png',
								),
								
							),
							'default' => 'style2'
						),
                        array(
                             'type' => 'select',
                            'name' => 'search_page_sidebar',
                            'label' => __( 'Sidebar', SH_NAME ),
                            'items' => array(
                                 'data' => array(
                                     array(
                                         'source' => 'function',
                                        'value' => 'sh_get_sidebars_2' 
                                    ) 
                                ) 
                            ),
                            'default' => array(
                                 '{{first}}' 
                            ) 
                        ),
                        array(
                             'type' => 'radioimage',
                            'name' => 'sear_page_layout',
                            'label' => __( 'Page Layout', SH_NAME ),
                            'description' => __( 'Choose the layout for blog pages', SH_NAME ),
                            
                            'items' => array(
                                 array(
                                     'value' => 'left',
                                    'label' => __( 'Left Sidebar', SH_NAME ),
                                    'img' => get_template_directory_uri() . '/includes/vafpress/public/img/2cl.png' 
                                ),
                                
                                array(
                                     'value' => 'right',
                                    'label' => __( 'Right Sidebar', SH_NAME ),
                                    'img' => get_template_directory_uri() . '/includes/vafpress/public/img/2cr.png' 
                                ),
                                array(
                                     'value' => 'full',
                                    'label' => __( 'Full Width', SH_NAME ),
                                    'img' => get_template_directory_uri() . '/includes/vafpress/public/img/1col.png' 
                                ) 
                                
                            ) 
                        ),
                    ) 
                ), // End of submenu
                
                // Archive Page Settings 
                array(
                     'title' => __( 'Archive Page', SH_NAME ),
                    'name' => 'archive_page_settings',
                    'icon' => 'font-awesome:fa fa-archive',
                    'controls' => array(
                         array(
                             'type' => 'textbox',
                            'name' => 'archive_page_title',
                            'label' => __( 'Page Title', SH_NAME ),
                            'description' => __( 'Enter the Title you want to show on Archive page', SH_NAME ),
                            'default' => 'Archive' 
                        ),
						array(
                             'type' => 'upload',
                            'name' => 'archive_page_header_img',
                            'label' => __( 'Header image', SH_NAME ),
                            'description' => __( 'Choose the image you want to show on archive / Index page', SH_NAME ),
                            //'default' => 'Search Results' 
                        ),
						array(
							'type' => 'radioimage',
							'name' => 'search_page_view',
							'label' => __('Page View', SH_NAME),
							'description' => __('Choose whether images in left or top', SH_NAME),
							'items' => array(
								array(
									'value' => 'style1',
									'label' => __('Images in Left', SH_NAME),
									'img' => get_template_directory_uri().'/includes/vafpress/public/img/grid-alt.png',
								),
								array(
									'value' => 'style2',
									'label' => __('Images at Top', SH_NAME),
									'img' => get_template_directory_uri().'/includes/vafpress/public/img/list3.png',
								),
								
							),
							'default' => 'style2'
						),
                        array(
                             'type' => 'select',
                            'name' => 'archive_page_sidebar',
                            'label' => __( 'Sidebar', SH_NAME ),
                            'items' => array(
                                 'data' => array(
                                     array(
                                         'source' => 'function',
                                        'value' => 'sh_get_sidebars_2' 
                                    ) 
                                ) 
                            ),
                            'default' => array(
                                 '{{first}}' 
                            ) 
                        ),
                        array(
                             'type' => 'radioimage',
                            'name' => 'archive_page_layout',
                            'label' => __( 'Page Layout', SH_NAME ),
                            'description' => __( 'Choose the layout for blog pages', SH_NAME ),
                            
                            'items' => array(
                                 array(
                                     'value' => 'left',

                                    'label' => __( 'Left Sidebar', SH_NAME ),
                                    'img' => get_template_directory_uri() . '/includes/vafpress/public/img/2cl.png' 
                                ),
                                
                                array(
                                     'value' => 'right',
                                    'label' => __( 'Right Sidebar', SH_NAME ),
                                    'img' => get_template_directory_uri() . '/includes/vafpress/public/img/2cr.png' 
                                ),
                                array(
                                     'value' => 'full',
                                    'label' => __( 'Full Width', SH_NAME ),
                                    'img' => get_template_directory_uri() . '/includes/vafpress/public/img/1col.png' 
                                ) 
                                
                            ) 
                        ) 
                        
                        
                    ) 
                ),
                
                // Author Page Settings 
                array(
                     'title' => __( 'Author Page', SH_NAME ),
                    'name' => 'author_page_settings',
                    'icon' => 'font-awesome:fa fa-user',
                    'controls' => array(
                         array(
                             'type' => 'textbox',
                            'name' => 'author_page_title',
                            'label' => __( 'Page Title', SH_NAME ),
                            'description' => __( 'Enter the Title you want to show on Author page', SH_NAME ),
                            'default' => 'Author Posts' 
                        ),
						array(
                            'type' => 'upload',
                            'name' => 'author_page_header_img',
                            'label' => __( 'Header image', SH_NAME ),
                            'description' => __( 'Choose the image you want to show on author page', SH_NAME ),
                            //'default' => 'Search Results' 
                        ),
						array(
							'type' => 'radioimage',
							'name' => 'search_page_view',
							'label' => __('Page View', SH_NAME),
							'description' => __('Choose whether images in left or top', SH_NAME),
							'items' => array(
								array(
									'value' => 'style1',
									'label' => __('Images in Left', SH_NAME),
									'img' => get_template_directory_uri().'/includes/vafpress/public/img/grid-alt.png',
								),
								array(
									'value' => 'style2',
									'label' => __('Images at Top', SH_NAME),
									'img' => get_template_directory_uri().'/includes/vafpress/public/img/list3.png',
								),
								
							),
							'default' => 'style2'
						),
                        array(
                             'type' => 'select',
                            'name' => 'author_page_sidebar',
                            'label' => __( 'Sidebar', SH_NAME ),
                            'items' => array(
                                 'data' => array(
                                     array(
                                         'source' => 'function',
                                        'value' => 'sh_get_sidebars_2' 
                                    ) 
                                ) 
                            ),
                            'default' => array(
                                 '{{first}}' 
                            ) 
                        ),
                        array(
                             'type' => 'radioimage',
                            'name' => 'author_page_layout',
                            'label' => __( 'Page Layout', SH_NAME ),
                            'description' => __( 'Choose the layout for blog pages', SH_NAME ),
                            
                            'items' => array(
                                 array(
                                     'value' => 'left',
                                    'label' => __( 'Left Sidebar', SH_NAME ),
                                    'img' => get_template_directory_uri() . '/includes/vafpress/public/img/2cl.png' 
                                ),
                                
                                array(
                                     'value' => 'right',
                                    'label' => __( 'Right Sidebar', SH_NAME ),
                                    'img' => get_template_directory_uri() . '/includes/vafpress/public/img/2cr.png' 
                                ),
                                array(
                                     'value' => 'full',
                                    'label' => __( 'Full Width', SH_NAME ),
                                    'img' => get_template_directory_uri() . '/includes/vafpress/public/img/1col.png' 
                                ) 
                                
                            ) 
                        ) 
                        
                    ) 
                ),
                
                // 404 Page Settings 
               /* array(
                     'title' => __( '404 Page Settings', SH_NAME ),
                    'name' => '404_page_settings',
                    'icon' => 'font-awesome:fa fa-exclamation-triangle',
                    'controls' => array(
                         array(
                             'type' => 'textbox',
                            'name' => '404_page_title',
                            'label' => __( 'Page Title', SH_NAME ),
                            'description' => __( 'Enter the Title you want to show on 404 page', SH_NAME ),
                            'default' => '404 Page not Found' 
                        ),
                        array(
                             'type' => 'textbox',
                            'name' => '404_page_heading',
                            'label' => __( 'Page Heading', SH_NAME ),
                            'description' => __( 'Enter the Heading you want to show on 404 page', SH_NAME ),
                            'default' => '404 Page not Found' 
                        ),
                        array(
                             'type' => 'textbox',
                            'name' => '404_page_tag_line',
                            'label' => __( 'Page Tagline', SH_NAME ),
                            'description' => __( 'Enter the Tagline you want to show on 404 page', SH_NAME ),
                            'default' => '404 Page not Found' 
                        ),
                        array(
                             'type' => 'textarea',
                            'name' => '404_page_text',
                            'label' => __( '404 Page Text', SH_NAME ),
                            'description' => __( 'Enter the Text you want to show on 404 page', SH_NAME ),
                            'default' => '' 
                        ),
                        array(
                             'type' => 'select',
                            'name' => '404_page_sidebar',
                            'label' => __( 'Sidebar', SH_NAME ),
                            'items' => array(
                                 'data' => array(
                                     array(
                                         'source' => 'function',
                                        'value' => 'sh_get_sidebars_2' 
                                    ) 
                                ) 
                            ),
                            'default' => array(
                                 '{{first}}' 
                            ) 
                        ),
                        array(
                             'type' => 'radioimage',
                            'name' => 'layout',
                            'label' => __( 'Page Layout', SH_NAME ),
                            'description' => __( 'Choose the layout for blog pages', SH_NAME ),
                            
                            'items' => array(
                                 array(
                                     'value' => 'left',
                                    'label' => __( 'Left Sidebar', SH_NAME ),
                                    'img' => get_template_directory_uri() . '/includes/vafpress/public/img/2cl.png' 
                                ),
                                
                                array(
                                     'value' => 'right',
                                    'label' => __( 'Right Sidebar', SH_NAME ),
                                    'img' => get_template_directory_uri() . '/includes/vafpress/public/img/2cr.png' 
                                ),
                                array(
                                     'value' => 'full',
                                    'label' => __( 'Full Width', SH_NAME ),
                                    'img' => get_template_directory_uri() . '/includes/vafpress/public/img/1col.png' 
                                ) 
                                
                            ) 
                        ),
                        array(
                             'type' => 'upload',
                            'name' => '404_page_bg',
                            'label' => __( 'Background  Image', SH_NAME ),
                            'description' => __( 'Upload Image for 404 Page Background', SH_NAME ),
                            'default' => get_template_directory_uri() . '/images/logo.png' 
                        ) 
                    ) 
                ) */
            ) 
        ),
        
        // Sidebar Creator
        array(
             'title' => __( 'Sidebar Settings', SH_NAME ),
            'name' => 'sidebar-settings',
            'icon' => 'font-awesome:fa fa-bars',
            'controls' => array(
                 array(
                     'type' => 'builder',
                    'repeating' => true,
                    'sortable' => true,
                    'label' => __( 'Dynamic Sidebar', SH_NAME ),
                    'name' => 'dynamic_sidebar',
                    'description' => __( 'This section is used for theme color settings', SH_NAME ),
                    'fields' => array(
                         array(
                             'type' => 'textbox',
                            'name' => 'sidebar_name',
                            'label' => __( 'Sidebar Name', SH_NAME ),
                            'description' => __( 'Choose the default color scheme for the theme.', SH_NAME ),
                            'default' => __( 'Dynamic Sidebar', SH_NAME ) 
                        ) 
                    ) 
                ) 
            ) 
        ),
        
        // Dynamic Social Media Creator
        array(
             'title' => __( 'Social Media ', SH_NAME ),
            'name' => 'social_media_section',
            'icon' => 'font-awesome:fa fa-share-square',
            'controls' => array(
                 array(
                     'type' => 'builder',
                    'repeating' => true,
                    'sortable' => true,
                    'label' => __( 'Social Media', SH_NAME ),
                    'name' => 'social_media',
                    'description' => __( 'This section is used to add Social Media.', SH_NAME ),
                    'fields' => array(
                         array(
                             'type' => 'textbox',
                            'name' => 'title',
                            'label' => __( 'Title', SH_NAME ),
                            'description' => __( 'Enter the title of the social media.', SH_NAME ), 
                        ),
						 array(
                             'type' => 'textbox',
                            'name' => 'social_link',
                            'label' => __( 'Link', SH_NAME ),
                            'description' => __( 'Enter the Link for Social Media.', SH_NAME ),
                            'default' => '#'
                        ),
                        array(
                            'type' => 'select',
                            'name' => 'social_icon',
                            'label' => __( 'Icon', SH_NAME ),
                            'description' => __( 'Choose Icon for Social Media.', SH_NAME ),
							'items' => array(
								'data' => array(
									array(
										'source' => 'function',
										'value' => 'vp_get_social_medias',
									),
								),
							),
                        )  
                    ) 
                ) 
            ) 
        ),
        
        
        /* Font settings */
        array(
             'title' => __( 'Font Settings', SH_NAME ),
            'name' => 'font_settings',
            'icon' => 'font-awesome:fa fa-font',
            'menus' => array(
                /** heading font settings */
                 array(
                     'title' => __( 'Heading Font', SH_NAME ),
                    'name' => 'heading_font_settings',
                    'icon' => 'font-awesome:fa fa-text-height',
                    
                    'controls' => array(
                        
                         array(
                             'type' => 'toggle',
                            'name' => 'use_custom_font',
                            'label' => __( 'Use Custom Font', SH_NAME ),
                            'description' => __( 'Use custom font or not', SH_NAME ),
                            'default' => 0 
                        ),
                        array(
                            'type' => 'section',
                            'title' => __( 'H1 Settings', SH_NAME ),
                            'name' => 'h1_settings',
                            'description' => __( 'heading 1 font settings', SH_NAME ),
                            'dependency' => array(
                                 'field' => 'use_custom_font',
                                'function' => 'vp_dep_boolean' 
                            ),
                            'fields' => array(
                                 array(
                                     'type' => 'select',
                                    'label' => __( 'Font Family', SH_NAME ),
                                    'name' => 'h1_font_family',
                                    'description' => __( 'Select the font family to use for h1', SH_NAME ),
                                    'items' => array(
                                         'data' => array(
                                             array(
                                                 'source' => 'function',
                                                'value' => 'vp_get_gwf_family' 
                                            ) 
                                        ) 
                                    ) 
                                    
                                ),
                                
                                array(
                                     'type' => 'color',
                                    'name' => 'h1_font_color',
                                    'label' => __( 'Font Color', SH_NAME ),
                                    'description' => __( 'Choose the font color for heading h1', SH_NAME ),
                                    'default' => '#98ed28' 
                                ) 
                            ) 
                        ),
                        array(
                             'type' => 'section',
                            'title' => __( 'H2 Settings', SH_NAME ),
                            'name' => 'h2_settings',
                            'description' => __( 'heading h2 font settings', SH_NAME ),
                            'dependency' => array(
                                 'field' => 'use_custom_font',
                                'function' => 'vp_dep_boolean' 
                            ),
                            'fields' => array(
                                 array(
                                     'type' => 'select',
                                    'label' => __( 'Font Family', SH_NAME ),
                                    'name' => 'h2_font_family',
                                    'description' => __( 'Select the font family to use for h2', SH_NAME ),
                                    'items' => array(
                                         'data' => array(
                                             array(
                                                 'source' => 'function',
                                                'value' => 'vp_get_gwf_family' 
                                            ) 
                                        ) 
                                    ) 
                                ),
                                array(
                                     'type' => 'color',
                                    'name' => 'h2_font_color',
                                    'label' => __( 'Font Color', SH_NAME ),
                                    'description' => __( 'Choose the font color for heading h1', SH_NAME ),
                                    'default' => '#98ed28' 
                                ) 
                            ) 
                        ),
                        array(
                             'type' => 'section',
                            'title' => __( 'H3 Settings', SH_NAME ),
                            'name' => 'h3_settings',
                            'description' => __( 'heading h3 font settings', SH_NAME ),
                            'dependency' => array(
                                 'field' => 'use_custom_font',
                                'function' => 'vp_dep_boolean' 
                            ),
                            'fields' => array(
                                
                                 array(
                                     'type' => 'select',
                                    'label' => __( 'Font Family', SH_NAME ),
                                    'name' => 'h3_font_family',
                                    'description' => __( 'Select the font family to use for h3', SH_NAME ),
                                    'items' => array(
                                         'data' => array(
                                             array(
                                                 'source' => 'function',
                                                'value' => 'vp_get_gwf_family' 
                                            ) 
                                        ) 
                                    ) 
                                    
                                ),
                                array(
                                     'type' => 'color',
                                    'name' => 'h3_font_color',
                                    'label' => __( 'Font Color', SH_NAME ),
                                    'description' => __( 'Choose the font color for heading h3', SH_NAME ),
                                    'default' => '#98ed28' 
                                ) 
                            ) 
                        ),
                        
                        array(
                             'type' => 'section',
                            'title' => __( 'H4 Settings', SH_NAME ),
                            'name' => 'h4_settings',
                            'description' => __( 'heading h4 font settings', SH_NAME ),
                            'dependency' => array(
                                 'field' => 'use_custom_font',
                                'function' => 'vp_dep_boolean' 
                            ),
                            'fields' => array(
                                
                                 array(
                                     'type' => 'select',
                                    'label' => __( 'Font Family', SH_NAME ),
                                    'name' => 'h4_font_family',
                                    'description' => __( 'Select the font family to use for h4', SH_NAME ),
                                    'items' => array(
                                         'data' => array(
                                             array(
                                                 'source' => 'function',
                                                'value' => 'vp_get_gwf_family' 
                                            ) 
                                        ) 
                                    ) 
                                    
                                ),
                                array(
                                     'type' => 'color',
                                    'name' => 'h4_font_color',
                                    'label' => __( 'Font Color', SH_NAME ),
                                    'description' => __( 'Choose the font color for heading h4', SH_NAME ),
                                    'default' => '#98ed28' 
                                ) 
                            ) 
                        ),
                        
                        array(
                             'type' => 'section',
                            'title' => __( 'H5 Settings', SH_NAME ),
                            'name' => 'h5_settings',
                            'description' => __( 'heading h5 font settings', SH_NAME ),
                            'dependency' => array(
                                 'field' => 'use_custom_font',
                                'function' => 'vp_dep_boolean' 
                            ),
                            'fields' => array(
                                
                                 array(
                                     'type' => 'select',
                                    'label' => __( 'Font Family', SH_NAME ),
                                    'name' => 'h5_font_family',
                                    'description' => __( 'Select the font family to use for h5', SH_NAME ),
                                    'items' => array(
                                         'data' => array(
                                             array(
                                                 'source' => 'function',
                                                'value' => 'vp_get_gwf_family' 
                                            ) 
                                        ) 
                                    ) 
                                    
                                ),
                                array(
                                     'type' => 'color',
                                    'name' => 'h5_font_color',
                                    'label' => __( 'Font Color', SH_NAME ),
                                    'description' => __( 'Choose the font color for heading h5', SH_NAME ),
                                    'default' => '#98ed28' 
                                ) 
                            ) 
                        ),
                        
                        array(
                             'type' => 'section',
                            'title' => __( 'H6 Settings', SH_NAME ),
                            'name' => 'h6_settings',
                            'description' => __( 'heading h6 font settings', SH_NAME ),
                            'dependency' => array(
                                 'field' => 'use_custom_font',
                                'function' => 'vp_dep_boolean' 
                            ),
                            'fields' => array(
                                
                                 array(
                                     'type' => 'select',
                                    'label' => __( 'Font Family', SH_NAME ),
                                    'name' => 'h6_font_family',
                                    'description' => __( 'Select the font family to use for h6', SH_NAME ),
                                    'items' => array(
                                         'data' => array(
                                             array(
                                                 'source' => 'function',
                                                'value' => 'vp_get_gwf_family' 
                                            ) 
                                        ) 
                                    ) 
                                    
                                ),
                                array(
                                     'type' => 'color',
                                    'name' => 'h6_font_color',
                                    'label' => __( 'Font Color', SH_NAME ),
                                    'description' => __( 'Choose the font color for heading h6', SH_NAME ),
                                    'default' => '#98ed28' 
                                ) 
                            ) 
                        ) 
                    ) 
                ),
                
                /** body font settings */
                array(
                     'title' => __( 'Body Font', SH_NAME ),
                    'name' => 'body_font_settings',
                    'icon' => 'font-awesome:fa fa-text-width',
                    'controls' => array(
                         array(
                             'type' => 'toggle',
                            'name' => 'body_custom_font',
                            'label' => __( 'Use Custom Font', SH_NAME ),
                            'description' => __( 'Use custom font or not', SH_NAME ),
                            'default' => 0 
                        ),
                        array(
                             'type' => 'section',
                            'title' => __( 'Body Font Settings', SH_NAME ),
                            'name' => 'body_font_settings1',
                            'description' => __( 'body font settings', SH_NAME ),
                            'dependency' => array(
                                 'field' => 'body_custom_font',
                                'function' => 'vp_dep_boolean' 
                            ),
                            'fields' => array(
                                
                                 array(
                                     'type' => 'select',
                                    'label' => __( 'Font Family', SH_NAME ),
                                    'name' => 'body_font_family',
                                    'description' => __( 'Select the font family to use for body', SH_NAME ),
                                    'items' => array(
                                         'data' => array(
                                             array(
                                                 'source' => 'function',
                                                'value' => 'vp_get_gwf_family' 
                                            ) 
                                        ) 
                                    ) 
                                    
                                ),
                                
                                array(
                                     'type' => 'color',
                                    'name' => 'body_font_color',
                                    'label' => __( 'Font Color', SH_NAME ),
                                    'description' => __( 'Choose the font color for heading body', SH_NAME ),
                                    'default' => '#686868' 
                                ) 
                            ) 
                        ) 
                    ) 
                ) 
            ) 
        ), 
		
		// Maintainance mode settings
        array(
             'title' => __( 'Maintainance', SH_NAME ),
            'name' => 'maintainance_section',
            'icon' => 'font-awesome:fa fa-share-square',
            'controls' => array(
                 
				 array(
					 'type' => 'toggle',
					'name' => 'maintainance_status',
					'label' => __( 'Enable Maintainance Mode', SH_NAME ),
					'description' => __( 'When admin is signed in then it will not work', SH_NAME ),
					'default' => 0 
				),
				 
				array(
					'type' => 'select',
					'name' => 'maintainance_page',
					'label' => __( 'Maintainance Page', SH_NAME ),
					'description' => __( 'Choose the page to show as maintainance page.', SH_NAME ),
					'default' => '',
					'items' => array(
						 'data' => array(
							 array(
								 'source' => 'function',
								'value' => 'vp_get_pages' 
							) 
						) 
					)
				),
            ) 
        ),
		
    ) 
);

/**
 *EOF
 */