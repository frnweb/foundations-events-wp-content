<?php
$sh_sc = array();
$sh_sc['sh_rotating_words']	=	array(
					"name" => __("Rotating Words Section", SH_NAME),
					"base" => "sh_rotating_words",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Full width Rotationg Words Section.', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Title 1 ", SH_NAME),
						   "param_name" => "title1",
						   "description" => __("Enter title before Rotationg Words", SH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Title 2", SH_NAME),
						   "param_name" => "title2",
						   "description" => __("Enter title after Rotating Words.", SH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Rotating Words", SH_NAME),
						   "param_name" => "rotating_words",
						   "description" => __("Enter comma seperated Rotating Words", SH_NAME)
						),
						array(
						   "type" => "textarea",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Text", SH_NAME),
						   "param_name" => "text",
						   "description" => __("Enter text to show in this section.", SH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Button 1 Text', SH_NAME ),
						   "param_name" => "btn1_text",
						   "description" => __('Enter Text for Button 1', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Button 2 Text', SH_NAME ),
						   "param_name" => "btn2_text",
						   "description" => __('Enter Text for Button 2', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Button 1 Link', SH_NAME ),
						   "param_name" => "btn1_link",
						   "description" => __('Enter Link for Button 1', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Button 2 Link', SH_NAME ),
						   "param_name" => "btn2_link",
						   "description" => __('Enter Link for Button 2', SH_NAME )
						),
						array(
						   "type" => "attach_images",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Background Image', SH_NAME ),
						   "param_name" => "bg",
						   "description" => __('Upload Background Image.', SH_NAME )
						),
						
					)
				);
				
$sh_sc['sh_services_1']	=	array(
					"name" => __("Services (3 Columns)", SH_NAME),
					"base" => "sh_services_1",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'fa-briefcase' ,
					'description' => __('Show Service Style 1 in 3 Columns.', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', SH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of Services to Show.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Text Limit', SH_NAME ),
						   "param_name" => "limit",
						   "description" => __('Enter the text limit.', SH_NAME ),
						   'value' => 15
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', SH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)sh_get_categories( array( 'taxonomy' => 'services_category', 'hide_empty' => FALSE ), true ) ),
						   "description" => __( 'Choose Category.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", SH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", SH_NAME),
						   "param_name" => "order",
						   'value' => array_flip(array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Show More Text', SH_NAME ),
						   "param_name" => "more_text",
						   "description" => __('Enter show more text.', SH_NAME ),
						   'value' => __('Know More', SH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Show more link', SH_NAME ),
						   "param_name" => "more_link",
						   "description" => __('Enter show more link, leave blank if do not want to show', SH_NAME )
						),
						array(
						   "type" => "checkbox",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("With Border", SH_NAME),
						   "param_name" => "with_border",
						   'value' => array(__('Icons with Border', SH_NAME) => 1 ),
						   "description" => __("whether icons should be in border or not.", SH_NAME)
						),
					)
				);

$sh_sc['sh_call_to_action']	=	array(
					"name" => __("Call to Action ", SH_NAME),
					"base" => "sh_call_to_action",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'fa-briefcase' ,
					'description' => __('Centered Call to Action with background image.', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Heading Text", SH_NAME),
						   "param_name" => "heading",
						   "description" => __("Enter heading for Call of Action", SH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Tagline", SH_NAME),
						   "param_name" => "tagline",
						   "description" => __("Enter Tagline for Call of Action.", SH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Button  Text', SH_NAME ),
						   "param_name" => "btn_text",
						   "description" => __('Enter Text for Button', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Button Link', SH_NAME ),
						   "param_name" => "btn_link",
						   "description" => __('Enter Link for Button', SH_NAME )
						),
						
						array(
						   "type" => "attach_image",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Background Image', SH_NAME ),
						   "param_name" => "bg",
						   "description" => __('Choose background image.', SH_NAME )
						),
						
						
					)
				);

$sh_sc['sh_skills']	=	array(
					"name" => __("Skills", SH_NAME),
					"base" => "sh_skills",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'fa-briefcase' ,
					"as_parent" => array('only' => 'sh_skill'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
					"content_element" => true,
					"show_settings_on_create" => false,
					'description' => __('Skills Section.', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Title", SH_NAME),
						   "param_name" => "title",
						   "description" => __("Enter Title for Skills Section", SH_NAME)
						),
						array(
						   "type" => "attach_image",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Background Image", SH_NAME),
						   "param_name" => "bg",
						   "description" => __("choose the background image", SH_NAME)
						),

					),
					"js_view" => 'VcColumnView'
				);
$sh_sc['sh_skill']	=	array(
					"name" => __("Skill", SH_NAME),
					"base" => "sh_skill",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					"as_child" => array('only' => 'sh_skills'),// Use only|except attributes to limit child shortcodes (separate multiple values with comma)
					"content_element" => true,
					"show_settings_on_create" => false,
					'description' => __('Skills Section.', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Title", SH_NAME),
						   "param_name" => "title",
						   "description" => __("Enter Title for Skills Section", SH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Percentage", SH_NAME),
						   "param_name" => "percentage",
						   "description" => __("Enter Percentage for this Skill", SH_NAME)
						),

					),
				);
$sh_sc['sh_latest_projects_1']	=	array(
					"name" => __("Latest Projects (Masonary)", SH_NAME),
					"base" => "sh_latest_projects_1",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show Full Width Latest Projects (Masonary) with filters.', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', SH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter Title of this Section.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', SH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of Projects to Show.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', SH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)sh_get_categories( array( 'taxonomy' => 'projects_category', 'hide_empty' => FALSE ), true ) ),
						   "description" => __( 'Choose Category.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", SH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", SH_NAME),
						   "param_name" => "order",
						   'value' => array_flip(array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) )),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('View ALL Link', SH_NAME ),
						   "param_name" => "link",
						   "description" => __('Enter Link to Viw All Page.', SH_NAME )
						),
						array(
						   "type" => "checkbox",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("White Background", SH_NAME),
						   "param_name" => "white",
						   'value' => array(__('White Bg', SH_NAME) => 1 ),
						   "description" => __("whether the section is in white background or grey.", SH_NAME)
						),
					)
				);
$sh_sc['sh_testimonials_1']	=	array(
					"name" => __("Happy Clients", SH_NAME),
					"base" => "sh_testimonials_1",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show Testimonials with custom Background.', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', SH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter title.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', SH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of Testimonials to Show.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Text Limit', SH_NAME ),
						   "param_name" => "limit",
						   'value' => 15,						   
						   "description" => __('Enter the text limit.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', SH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)sh_get_categories( array( 'taxonomy' => 'testimonials_category', 'hide_empty' => FALSE ), true ) ),
						   "description" => __( 'Choose Category.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", SH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", SH_NAME),
						   "param_name" => "order",
						   'value' => array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ),
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "attach_image",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("BG", SH_NAME),
						   "param_name" => "bg",
						   "description" => __("Upload Background Image.", SH_NAME)
						),
					)
				);


$sh_sc['sh_services_2']	=	array(
					"name" => __("Services with Large image", SH_NAME),
					"base" => "sh_services_2",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'fa-briefcase' ,
					'description' => __('Show Services with large image in right side.', SH_NAME),
					"params" => array(
						
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', SH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter Title for this section.', SH_NAME )
						),
						
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', SH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of Services to Show.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Text Limit', SH_NAME ),
						   "param_name" => "limit",
						   'value' => 15,						   
						   "description" => __('Enter the text limit.', SH_NAME )
						),
						array(
						   "type" => "attach_image",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Right Side Image', SH_NAME ),
						   "param_name" => "image",
						   "description" => __( 'Choose image.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', SH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)sh_get_categories( array( 'taxonomy' => 'services_category', 'hide_empty' => FALSE ), true ) ),
						   "description" => __( 'Choose Category.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", SH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", SH_NAME),
						   "param_name" => "order",
						   'value' => array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
					)
				);
$sh_sc['sh_latest_projects_2']	=	array(
					"name" => __("Latest Projects (Simple)", SH_NAME),
					"base" => "sh_latest_projects_2",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show boxed width portfolio withou filteration.', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', SH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter Title of this Section.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', SH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of Projects to Show.', SH_NAME )
						),
						
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', SH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)sh_get_categories( array( 'taxonomy' => 'projects_category', 'hide_empty' => FALSE ) ) ),
						   "description" => __( 'Choose Category.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", SH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", SH_NAME),
						   "param_name" => "order",
						   'value' => array('DESC'=>__('Descending', SH_NAME), 'ASC'=>__('Ascending', SH_NAME), ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('View All Link', SH_NAME ),
						   "param_name" => "link",
						   "description" => __('Enter Link to View All Projects Page.', SH_NAME )
						),
					)
				);

$sh_sc['sh_latest_product_1']	=	array(
					"name" => __("Latest Products", SH_NAME),
					"base" => "sh_latest_product_1",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show Latest Products from your Shop.', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', SH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter Title of this Section.', SH_NAME )
						),
						
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', SH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of Projects to Show.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', SH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)sh_get_categories( array( 'taxonomy' => 'product_cat', 'hide_empty' => FALSE ), true ) ),
						   "description" => __( 'Choose Category.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", SH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", SH_NAME),
						   "param_name" => "order",
						   'value' => array_flip( array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
					)
				);
$sh_sc['sh_testimonials_2']	=	array(
					"name" => __("Testimonials (Image Caoursel)", SH_NAME),
					"base" => "sh_testimonials_2",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show Testimonials of image carousel.', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', SH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter title of testimonials or leave empty to hide.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', SH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of Testimonials to Show.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Text Limit', SH_NAME ),
						   "param_name" => "limit",
						   "description" => __('Enter the text limit.', SH_NAME ),
						   'value' => 15
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', SH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)sh_get_categories( array( 'taxonomy' => 'testimonials_category', 'hide_empty' => FALSE ) ) ),
						   "description" => __( 'Choose Category.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", SH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", SH_NAME),
						   "param_name" => "order",
						   'value' => array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "attach_image",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("BG", SH_NAME),
						   "param_name" => "bg",
						   "description" => __("Upload Background Image.", SH_NAME)
						),
						
					)
				);
$sh_sc['sh_services_3']	=	array(
					"name" => __("Services Carousel", SH_NAME),
					"base" => "sh_services_3",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show services carousel with featured image.', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', SH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter Title of this Section.', SH_NAME )
						),
						
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', SH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of Services to Show.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Text Limit', SH_NAME ),
						   "param_name" => "limit",
						   "description" => __('Enter text limit to show.', SH_NAME ),
						   "value" => 20
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', SH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)sh_get_categories( array( 'taxonomy' => 'services_category', 'hide_empty' => FALSE ), true ) ),
						   "description" => __( 'Choose Category.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", SH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", SH_NAME),
						   "param_name" => "order",
						   'value' => array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						
					)
				);

$sh_sc['sh_latest_projects_3']	=	array(
					"name" => __("Featured Works", SH_NAME),
					"base" => "sh_latest_projects_3",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show Projects in full width.', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', SH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter Title of this Section.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Slogan Text', SH_NAME ),
						   "param_name" => "slogan",
						   "description" => __('Enter slogan text.', SH_NAME )
						),
						
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', SH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of Projects to Show.', SH_NAME )
						),
						
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', SH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)sh_get_categories( array( 'taxonomy' => 'projects_category', 'hide_empty' => FALSE ), true ) ),
						   "description" => __( 'Choose Category.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", SH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", SH_NAME),
						   "param_name" => "order",
						   'value' => array_flip( array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ) ),
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Columns", SH_NAME),
						   "param_name" => "cols",
						   'value' => array_flip( array('4'=>__('4 Columns', SH_NAME),'3'=>__('3 Columns', SH_NAME),'2'=>__('2 Columns', SH_NAME) ) ),
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						
					)
				);
$sh_sc['sh_pricing_table_section']	=	array(
					"name" => __("Pricing Tables Section", SH_NAME),
					"base" => "sh_pricing_table_section",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					"as_parent" => array('only' => 'sh_pricing_table'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
					"content_element" => true,
					"show_settings_on_create" => false,
					'description' => __('Add Section of Pricing Tables.', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Title", SH_NAME),
						   "param_name" => "title",
						   "description" => __("Enter Title for this Section", SH_NAME)
						),
						array(
						   "type" => "attach_image",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Background Image", SH_NAME),
						   "param_name" => "bg",
						   "description" => __("Choose background image", SH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Background", SH_NAME),
						   "param_name" => "bg_class",
						   'value' => array(__('Grey', SH_NAME)=>'grey-wrapper',__('White', SH_NAME)=>'white-wrapper',),
						   "description" => __("Choose background", SH_NAME)
						),
					),
					"js_view" => 'VcColumnView'
				);


$sh_sc['sh_pricing_table']	=	array(
					"name" => __("Pricing Table", SH_NAME),
					"base" => "sh_pricing_table",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					"as_child" => array('only' => 'sh_pricing_table_section'),// Use only|except attributes to limit child shortcodes (separate multiple values with comma)
					"content_element" => true,
					"show_settings_on_create" => false,
					'description' => __('Add Pricing Table.', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Title", SH_NAME),
						   "param_name" => "title",
						   "description" => __("Enter Title of Pricing Table", SH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Price Symbol", SH_NAME),
						   "param_name" => "symbol",
						   "description" => __("Enter symbol", SH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Price", SH_NAME),
						   "param_name" => "price",
						   "description" => __("Enter Price", SH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Type", SH_NAME),
						   "param_name" => "type",
						   'value' => '/mo',
						   "description" => __("Enter Type", SH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Button Text", SH_NAME),
						   "param_name" => "btn_text",
						   "description" => __("Enter Text to show on Button", SH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Button Link", SH_NAME),
						   "param_name" => "btn_link",
						   "description" => __("Enter Link for Button", SH_NAME)
						),
						array(
						   "type" => "exploded_textarea",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Fetures", SH_NAME),
						   "param_name" => "fetures",
						   "description" => __("Enter One Feture per Line", SH_NAME)
						),
						array(
						   "type" => "checkbox",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Selected", SH_NAME),
						   "param_name" => "selected",
						   'value' => array(__('Selected',SH_NAME) => true),
						   "description" => __("Whether it is selected or not?", SH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Background", SH_NAME),
						   "param_name" => "bg_class",
						   'value' => array(__('Grey', SH_NAME)=>'grey-wrapper',__('White', SH_NAME)=>'white-wrapper',),
						   "description" => __("Choose background", SH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Columns", SH_NAME),
						   "param_name" => "cols",
						   'value' => array(__('4 Columns', SH_NAME)=>'4',__('3 Columns', SH_NAME)=>'3',),
						   "description" => __("Choose columns, you need to choose same columns for one row", SH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Style", SH_NAME),
						   "param_name" => "style",
						   'value' => array(__('Style 1', SH_NAME)=>'1',__('Style 2', SH_NAME)=>'2',),
						   "description" => __("Choose either style 1 or style 2", SH_NAME)
						),

					),
				);
$sh_sc['sh_testimonials_3']	=	array(
					"name" => __("Testimonial Boxes", SH_NAME),
					"base" => "sh_testimonials_3",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show testimonials in boxes.', SH_NAME),
					"params" => array(
						
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', SH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter Title of Testimonials to Show.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', SH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of Testimonials to Show.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Text Limit', SH_NAME ),
						   "param_name" => "limit",
						   "description" => __('Enter text limit.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', SH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)sh_get_categories( array( 'taxonomy' => 'testimonials_category', 'hide_empty' => FALSE ), true ) ),
						   "description" => __( 'Choose Category.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", SH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", SH_NAME),
						   "param_name" => "order",
						   'value' => array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
					)
				);
$sh_sc['sh_brands_section']	=	array(
					"name" => __("Clients and Brands Section", SH_NAME),
					"base" => "sh_brands_section",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show Brands and Clients From Theme Options.', SH_NAME),
					"params" => array(
						
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', SH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of brands to show.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", SH_NAME),
						   "param_name" => "order",
						   'value' => array(__('Ascending', SH_NAME)=>"ASC",__('Descending', SH_NAME)=>'DESC' ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						
						array(
						   "type" => "checkbox",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("White Background", SH_NAME),
						   "param_name" => "white",
						   'value' => array(__('Enable White BG', SH_NAME)=>true),
						   "description" => __("Either white bg or not", SH_NAME)
						),
						
					)
				);

$sh_sc['sh_brands_section_2']	=	array(
					"name" => __("Clients and Brands 2", SH_NAME),
					"base" => "sh_brands_section_2",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text',
					'description' => __('Show Brands and Clients From Theme Options.', SH_NAME),
					"params" => array(
						
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', SH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of brands to show.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", SH_NAME),
						   "param_name" => "order",
						   'value' => array(__('Ascending', SH_NAME)=>"ASC",__('Descending', SH_NAME)=>'DESC' ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Columns", SH_NAME),
						   "param_name" => "cols",
						   'value' => array(__('4 Columns', SH_NAME)=>"4",__('3 Columns', SH_NAME)=>'3' ),			
						   "description" => __("Choose the columns.", SH_NAME)
						),
						
					)
				);	

$sh_sc['sh_services_4']	=	array(
					"name" => __("Services (3 Columns)", SH_NAME),
					"base" => "sh_services_4",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Our Working Process, Show services 3 columns.', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', SH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter Title of this Section.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Text limit', SH_NAME ),
						   "param_name" => "limit",
						   "description" => __('Enter the text limit.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', SH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of Services to Show.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', SH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)sh_get_categories( array( 'taxonomy' => 'services_category', 'hide_empty' => FALSE ), true ) ),
						   "description" => __( 'Choose Category.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", SH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", SH_NAME),
						   "param_name" => "order",
						   'value' => array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
					)
				);				
$sh_sc['sh_fun_facts']	=	array(
					"name" => __("Fun Facts", SH_NAME),
					"base" => "sh_fun_facts",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					"as_parent" => array('only' => 'sh_fact'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
					"content_element" => true,
					"show_settings_on_create" => false,
					'description' => __('Add Fun Facts to your theme.', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Title Before", SH_NAME),
						   "param_name" => "title",
						   "description" => __("Enter title before rotating text", SH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Rotating Text", SH_NAME),
						   "param_name" => "rotating_text",
						   "description" => __("Enter comma seprated rotating text, leave blank to hide", SH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Title After", SH_NAME),
						   "param_name" => "title_after",
						   "description" => __("Enter title after rotating text", SH_NAME)
						),
						array(
						   "type" => "attach_image",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Front Image", SH_NAME),
						   "param_name" => "image",
						   "description" => __("Upload front Image", SH_NAME)
						),
						array(
						   "type" => "attach_image",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Background Image", SH_NAME),
						   "param_name" => "bg",
						   "description" => __("Upload Background Image", SH_NAME)
						),
						array(
						   "type" => "checkbox",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("White Overlay", SH_NAME),
						   "param_name" => "overlay",
						   'value' => array(__('Enable White Overlay', SH_NAME)=>true),
						   "description" => __("Either white overlay or not", SH_NAME)
						),
						array(
						   "type" => "checkbox",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Center", SH_NAME),
						   "param_name" => "center",
						   'value' => array(__('Enable Container', SH_NAME)=>true),
						   "description" => __("Whether it is full width or in container", SH_NAME)
						),

					),
					"js_view" => 'VcColumnView'
				);
$sh_sc['sh_fact']	=	array(
					"name" => __("Fact", SH_NAME),
					"base" => "sh_fact",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					"as_child" => array('only' => 'sh_fun_facts'),// Use only|except attributes to limit child shortcodes (separate multiple values with comma)
					"content_element" => true,
					"show_settings_on_create" => true,
					'description' => __('Add Fact.', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Title", SH_NAME),
						   "param_name" => "title",
						   "description" => __("Enter Title for Skills Section", SH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Number", SH_NAME),
						   "param_name" => "number",
						   "description" => __("Enter Number", SH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Icon", SH_NAME),
						   "param_name" => "icon",
						   "value" => array_flip( (array)sh_font_awesome() ),
						   "description" => __("Choose Icon for Process", SH_NAME)
						),
					),
				);
$sh_sc['sh_powerful_skills']	=	array(
					"name" => __("Powerful Skills", SH_NAME),
					"base" => "sh_powerful_skills",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show powerfull skills.', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', SH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter Title of this Section.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', SH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of News to Show.', SH_NAME )
						),
						
						array(
						   "type" => "textarea",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Text", SH_NAME),
						   "param_name" => "content",
						   "description" => __("Enter the text.", SH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", SH_NAME),
						   "param_name" => "order",
						   'value' => array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
					)
				);


$sh_sc['sh_services_5']	=	array(
					"name" => __("Services 2 Column", SH_NAME),
					"base" => "sh_services_5",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show services with 2 column, about jollyall.', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', SH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter the section title.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Slogan Text', SH_NAME ),
						   "param_name" => "slogan",
						   "description" => __('Enter slogan text.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Text Limit', SH_NAME ),
						   "param_name" => "limit",
						   "description" => __('Enter text limit to show.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', SH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of Services.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', SH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)sh_get_categories( array( 'taxonomy' => 'services_category', 'hide_empty' => FALSE ), true ) ),
						   "description" => __( 'Choose Category.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", SH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", SH_NAME),
						   "param_name" => "order",
						   'value' => array_flip(array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "attach_image",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Background Image", SH_NAME),
						   "param_name" => "bg",
						   "description" => __("Choose the background image.", SH_NAME)
						),
					)
				);
$sh_sc['sh_our_team']	=	array(
					"name" => __("Our Team", SH_NAME),
					"base" => "sh_our_team",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show team members.', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', SH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter Title of this Section.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Text Limit', SH_NAME ),
						   "param_name" => "limit",
						   'value' => 10,
						   "description" => __('Enter words limit.', SH_NAME )
						),
						
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', SH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of News to Show.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', SH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)sh_get_categories( array('taxonomy' => 'team_category' ,'hide_empty' => FALSE ), true ) ),
						   "description" => __( 'Choose Category.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", SH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", SH_NAME),
						   "param_name" => "order",
						   'value' => array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						
					)
				);


$sh_sc['sh_services_6']	=	array(
					"name" => __("Services Core Features", SH_NAME),
					"base" => "sh_services_6",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show core features accordion.', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', SH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter Title of this Section.', SH_NAME )
						),
						
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', SH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of Services to Show.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', SH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)sh_get_categories( array( 'taxonomy' => 'services_category', 'hide_empty' => FALSE ), true ) ),
						   "description" => __( 'Choose Category.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", SH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", SH_NAME),
						   "param_name" => "order",
						   'value' => array_flip(array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Text Limit', SH_NAME ),
						   "param_name" => "limit",
						   "description" => __('Enter the text limit.', SH_NAME ),
						   'default' => 15
						),
					)
				);
$sh_sc['sh_latest_projects_4']	=	array(
					"name" => __("Projects", SH_NAME),
					"base" => "sh_latest_projects_4",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show projects masonry of our latest works.', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', SH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter Title of this Section.', SH_NAME ),
						   'value' => __('Our Latest Works', SH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', SH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of Projects to Show.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', SH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)sh_get_categories( array( 'taxonomy' => 'projects_category', 'hide_empty' => FALSE ), true ) ),
						   "description" => __( 'Choose Category.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", SH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", SH_NAME),
						   "param_name" => "order",
						   'value' => array_flip(array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("View All Text", SH_NAME),
						   "param_name" => "link_txt",
						   'value' => __('View All Text', SH_NAME),			
						   "description" => __("View all text.", SH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('View All Link', SH_NAME ),
						   "param_name" => "link",
						   "description" => __('Enter Link to View All Projects Page.', SH_NAME )
						),
						array(
						   "type" => "attach_image",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Background Image', SH_NAME ),
						   "param_name" => "bg",
						   "description" => __('Choose the background image (Optional).', SH_NAME )
						),
					)
				);

$sh_sc['sh_latest_projects_5']	=	array(
					"name" => __("Projects ( Grid )", SH_NAME),
					"base" => "sh_latest_projects_5",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show projects grid with description', SH_NAME),
					"params" => array(
						
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', SH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of Projects to Show.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', SH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)sh_get_categories( array( 'taxonomy' => 'projects_category', 'hide_empty' => FALSE ), true ) ),
						   "description" => __( 'Choose Category.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", SH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", SH_NAME),
						   "param_name" => "order",
						   'value' => array_flip(array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) )),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Text Limit', SH_NAME ),
						   "param_name" => "limit",
						   "description" => __('Enter the text limit.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Columns", SH_NAME),
						   "param_name" => "cols",
						   'value' => array(__('4 Columns', SH_NAME)=>'4', __('3 Columns', SH_NAME)=>'3'),
						   "description" => __("Choose the grid columns", SH_NAME)
						),
					)
				);

$sh_sc['sh_latest_projects_6']	=	array(
					"name" => __("Projects ", SH_NAME),
					"base" => "sh_latest_projects_6",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('4 cols square boxes with filters', SH_NAME),
					"params" => array(
						
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', SH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter title.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', SH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of Projects to Show.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', SH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)sh_get_categories( array( 'taxonomy' => 'projects_category', 'hide_empty' => FALSE ), true ) ),
						   "description" => __( 'Choose Category.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", SH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", SH_NAME),
						   "param_name" => "order",
						   'value' => array_flip(array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) )),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('View More Link', SH_NAME ),
						   "param_name" => "link",
						   "description" => __('Enter view mor.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Columns", SH_NAME),
						   "param_name" => "cols",
						   'value' => array(__('4 Columns', SH_NAME)=>'4', __('3 Columns', SH_NAME)=>'3'),
						   "description" => __("Choose the grid columns", SH_NAME)
						),
					)
				);

$sh_sc['sh_latest_projects_7']	=	array(
					"name" => __("Projects 3 cols", SH_NAME),
					"base" => "sh_latest_projects_7",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('3 cols with cut edges', SH_NAME),
					"params" => array(
						
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', SH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter title.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Tagline', SH_NAME ),
						   "param_name" => "tagline",
						   "description" => __('Enter tagline.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', SH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of Projects to Show.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', SH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)sh_get_categories( array( 'taxonomy' => 'projects_category', 'hide_empty' => FALSE ), true ) ),
						   "description" => __( 'Choose Category.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", SH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", SH_NAME),
						   "param_name" => "order",
						   'value' => array_flip(array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) )),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('View More Link', SH_NAME ),
						   "param_name" => "link",
						   "description" => __('Enter view mor.', SH_NAME )
						),
						
					)
				);


$sh_sc['sh_latest_news']	=	array(
					"name" => __("Latest News", SH_NAME),
					"base" => "sh_latest_news",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show latest posts from the blog.', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', SH_NAME ),
						   "param_name" => "title",
						   "value" => __('Latest News', SH_NAME),
						   "description" => __('Enter title of this section.', SH_NAME )
						),
						
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', SH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter number of posts to show.', SH_NAME )
						),
						
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Text Limit', SH_NAME ),
						   "param_name" => "limit",
						   "description" => __('Enter text limit to show.', SH_NAME )
						),
						
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', SH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)sh_get_categories( array( 'hide_empty' => FALSE ), true ) ),
						   "description" => __( 'Choose Category.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", SH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", SH_NAME),
						   "param_name" => "order",
						   'value' => array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Columns", SH_NAME),
						   "param_name" => "cols",
						   'value' => array(__('3 Columns', SH_NAME)=>'3', __('2 Columns', SH_NAME)=>'2'),
						   "description" => __("Choose the grid columns", SH_NAME)
						),
						
					)
				);

$sh_sc['sh_blog_posts']	=	array(
					"name" => __("Blog Posts ( 2 cols )", SH_NAME),
					"base" => "sh_blog_posts",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show 2 column blog posts.', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', SH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter Title of this Section.', SH_NAME )
						),
						
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', SH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of Posts to Show.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', SH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)sh_get_categories( array('hide_empty' => FALSE ), true ) ),
						   "description" => __( 'Choose Category.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", SH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", SH_NAME),
						   "param_name" => "order",
						   'value' => array_flip(array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
					)
				);
$sh_sc['sh_call_to_action_2']	=	array(
					"name" => __("Call to Action", SH_NAME),
					"base" => "sh_call_to_action_2",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Call to action with custom bg color.', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Heading", SH_NAME),
						   "param_name" => "heading",
						   "description" => __("Enter Heading for Call of Action", SH_NAME)
						),
						
						array(
						   "type" => "textarea_raw_html",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Button HTML', SH_NAME ),
						   "param_name" => "btn_html",
						   "description" => __('Enter html for button', SH_NAME )
						),
						
						
						array(
						   "type" => "textarea_html",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Content', SH_NAME ),
						   "param_name" => "content",
						   "description" => __('Enter Content for the Section', SH_NAME )
						),
						array(
						   "type" => "colorpicker",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Background Color', SH_NAME ),
						   "param_name" => "bg_color",
						   "description" => __('Choose Background Color.', SH_NAME )
						),
						
					)
				);
$sh_sc['sh_services_7']	=	array(
					"name" => __("Services 3 col", SH_NAME),
					"base" => "sh_services_7",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('services black circle', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', SH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of Services to Show.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Text Limit', SH_NAME ),
						   "param_name" => "limit",
						   "description" => __('Enter the text limit.', SH_NAME ),
						   'value' => 15
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', SH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)sh_get_categories( array( 'taxonomy' => 'services_category', 'hide_empty' => FALSE ), true ) ),
						   "description" => __( 'Choose Category.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", SH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", SH_NAME),
						   "param_name" => "order",
						   'value' => array_flip(array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "checkbox",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("White BG", SH_NAME),
						   "param_name" => "white",
						   'value' => array_flip(array(true=>__('White BG color', SH_NAME)) ),			
						   "description" => __("Enable white bg", SH_NAME)
						),
					)
				);
$sh_sc['sh_services_8']	=	array(
					"name" => __("Services 3 col Carousel", SH_NAME),
					"base" => "sh_services_8",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Services 3 columns carousel.', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', SH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter Title of this Section.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Tagline', SH_NAME ),
						   "param_name" => "tagline",
						   "description" => __('Enter Tagline for this Section.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', SH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of Services to Show.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Text Limit', SH_NAME ),
						   "param_name" => "limit",
						   "description" => __('Enter the text limit.', SH_NAME ),
						   'value' => 15
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', SH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)sh_get_categories( array( 'taxonomy' => 'services_category', 'hide_empty' => FALSE ), true ) ),
						   "description" => __( 'Choose Category.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", SH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", SH_NAME),
						   "param_name" => "order",
						   'value' => array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
					)
				);	
$sh_sc['sh_parallax_section']	=	array(
					"name" => __("Parallax Section", SH_NAME),
					"base" => "sh_parallax_section",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('parallax section.', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Heading', SH_NAME ),
						   "param_name" => "heading",
						   "description" => __('Enter Text to show in this Section.', SH_NAME )
						),
						array(
						   "type" => "textarea_html",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Content', SH_NAME ),
						   "param_name" => "content",
						   "description" => __('enter content,.', SH_NAME )
						),
						array(
						   "type" => "attach_image",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('BackGround Image', SH_NAME ),
						   "param_name" => "bg",
						   "description" => __('Choose Background image.', SH_NAME )
						),
					)
				);
$sh_sc['sh_parallax_section_2']	=	array(
					"name" => __("Parallax Section 2", SH_NAME),
					"base" => "sh_parallax_section_2",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('parallax section.', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Heading', SH_NAME ),
						   "param_name" => "heading",
						   "description" => __('Enter Text to show in this Section.', SH_NAME )
						),
						
						array(
						   "type" => "attach_image",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('BackGround Image', SH_NAME ),
						   "param_name" => "bg",
						   "description" => __('Choose Background image.', SH_NAME )
						),
					)
				);
$sh_sc['sh_parallax_section_3']	=	array(
					"name" => __("Parallax Section 3", SH_NAME),
					"base" => "sh_parallax_section_3",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('parallax section.', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Title Before", SH_NAME),
						   "param_name" => "title",
						   "description" => __("Enter title before rotating text", SH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Rotating Text", SH_NAME),
						   "param_name" => "rotating_text",
						   "description" => __("Enter comma seprated rotating text, leave blank to hide", SH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Title After", SH_NAME),
						   "param_name" => "title_after",
						   "description" => __("Enter title after rotating text", SH_NAME)
						),
						array(
						   "type" => "attach_image",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Front Image", SH_NAME),
						   "param_name" => "image",
						   "description" => __("Upload front Image", SH_NAME)
						),
						array(
						   "type" => "attach_image",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Background Image", SH_NAME),
						   "param_name" => "bg",
						   "description" => __("Upload Background Image", SH_NAME)
						),
						array(
						   "type" => "checkbox",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("White Overlay", SH_NAME),
						   "param_name" => "overlay",
						   'value' => array(__('Enable White Overlay', SH_NAME)=>true),
						   "description" => __("Either white overlay or not", SH_NAME)
						),
					)
				);
$sh_sc['sh_services_9']	=	array(
					"name" => __("Services One Col", SH_NAME),
					"base" => "sh_services_9",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Services with Title above Icon.', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', SH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter Title of this Section.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Tagline', SH_NAME ),
						   "param_name" => "tagline",
						   "description" => __('Enter Tagline for this Section.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', SH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of Services to Show.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', SH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)sh_get_categories( array( 'taxonomy' => 'services_category', 'hide_empty' => FALSE ), true ) ),
						   "description" => __( 'Choose Category.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", SH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", SH_NAME),
						   "param_name" => "order",
						   'value' => array_flip(array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Text Limit', SH_NAME ),
						   "param_name" => "limit",
						   "description" => __('Enter content limit for services.', SH_NAME )
						),
					)
				);

$sh_sc['sh_services_10']	=	array(
					"name" => __("Services & Features", SH_NAME),
					"base" => "sh_services_10",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Services 3 cols icon in left.', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', SH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter Title of this Section.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Tagline', SH_NAME ),
						   "param_name" => "tagline",
						   "description" => __('Enter Tagline for this Section.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', SH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of Services to Show.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', SH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)sh_get_categories( array( 'taxonomy' => 'services_category', 'hide_empty' => FALSE ), true ) ),
						   "description" => __( 'Choose Category.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", SH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", SH_NAME),
						   "param_name" => "order",
						   'value' => array_flip(array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Text Limit', SH_NAME ),
						   "param_name" => "limit",
						   "description" => __('Enter content limit for services.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Read More Link', SH_NAME ),
						   "param_name" => "link",
						   "description" => __('Enter the read more link', SH_NAME )
						),
					)
				);

$sh_sc['sh_services_11']	=	array(
					"name" => __("Services Benefit", SH_NAME),
					"base" => "sh_services_11",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Services with right image.', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', SH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter Title of this Section.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Tagline', SH_NAME ),
						   "param_name" => "tagline",
						   "description" => __('Enter Tagline for this Section.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', SH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of Services to Show.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', SH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)sh_get_categories( array( 'taxonomy' => 'services_category', 'hide_empty' => FALSE ), true ) ),
						   "description" => __( 'Choose Category.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", SH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", SH_NAME),
						   "param_name" => "order",
						   'value' => array_flip(array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Text Limit', SH_NAME ),
						   "param_name" => "limit",
						   "description" => __('Enter content limit for services.', SH_NAME )
						),
						array(
						   "type" => "attach_image",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Image', SH_NAME ),
						   "param_name" => "image",
						   "description" => __('choose right side image', SH_NAME )
						),
					)
				);

$sh_sc['sh_faqs']	=	array(
					"name" => __("FAQs", SH_NAME),
					"base" => "sh_faqs",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Our History, Show faqs in accordion.', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', SH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter Title of this Section.', SH_NAME )
						),
						array(
						   "type" => "textarea",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Text', SH_NAME ),
						   "param_name" => "content",
						   "description" => __('Enter the text.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', SH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of Posts to Show.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', SH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)sh_get_categories( array('hide_empty' => FALSE, 'taxonomy'=>'faq_category' ), true ) ),
						   "description" => __( 'Choose Category.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", SH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", SH_NAME),
						   "param_name" => "order",
						   'value' => array_flip( array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ) ),
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "attach_image",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Image", SH_NAME),
						   "param_name" => "image",
						   "description" => __("Choose the image.", SH_NAME)
						),
					)
				);


$sh_sc['sh_faqs_2']	=	array(
					"name" => __("FAQs style 2", SH_NAME),
					"base" => "sh_faqs_2",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show faqs in accordion default style.', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', SH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter Title of this Section.', SH_NAME )
						),
						array(
						   "type" => "textarea",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Text', SH_NAME ),
						   "param_name" => "content",
						   "description" => __('Enter content.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', SH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of Posts to Show.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', SH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)sh_get_categories( array('hide_empty' => FALSE, 'taxonomy'=>'faq_category' ), true ) ),
						   "description" => __( 'Choose Category.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", SH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", SH_NAME),
						   "param_name" => "order",
						   'value' => array_flip( array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ) ),
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
					)
				);


$sh_sc['sh_content_with_image']	=	array(
					"name" => __("Content with Image", SH_NAME),
					"base" => "sh_content_with_image",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Add content with image', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', SH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter Title of this Section.', SH_NAME )
						),
						array(
						   "type" => "textarea_html",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Content', SH_NAME ),
						   "param_name" => "content",
						   "description" => __('Enter Content for this Section.', SH_NAME )
						),
						array(
						   "type" => "attach_image",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Image', SH_NAME ),
						   "param_name" => "image",
						   "description" => __('Upload Image for the Section.', SH_NAME )
						),
						
					)
				);
$sh_sc['sh_contact_form']	=	array(
					"name" => __("Contact Form", SH_NAME),
					"base" => "sh_contact_form",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Add Contact Form', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Title", SH_NAME),
						   "param_name" => "title",
						   'value' => '',
						   "description" => __("Enter title or leave blank if don\'t want to show title.", SH_NAME)
						),
						
						array(
						   "type" => "checkbox",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Captcha ?", SH_NAME),
						   "param_name" => "captcha",
						   'value' => array('Show Captcha' => 1 ),
						   "description" => __("Choose either to show Captcha or not.", SH_NAME)
						),
					)
				);

$sh_sc['sh_google_map']	=	array(
					"name" => __("Google Map", SH_NAME),
					"base" => "sh_google_map",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Add google map', SH_NAME),
					"params" => array(
						
						array(
						   "type" => "textarea",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Address", SH_NAME),
						   "param_name" => "content",
						   'value' => '',
						   "description" => __("Enter the address to show on map.", SH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Latitud", SH_NAME),
						   "param_name" => "lat",
						   'value' => '-37.801578',
						   "description" => __("Enter the latitude.", SH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Longitude", SH_NAME),
						   "param_name" => "lang",
						   'value' => '145.060508',
						   "description" => __("Enter the longitude.", SH_NAME)
						),
						array(
						   "type" => "checkbox",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Map Position", SH_NAME),
						   "param_name" => "map_bottom",
						   'value' => array('Map at Bottom' => 1 ),
						   "description" => __("Choose to show map at bottom.", SH_NAME)
						),
						
					)
				);

$sh_sc['sh_contact_details']	=	array(
					"name" => __("Contact Details", SH_NAME),
					"base" => "sh_contact_details",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Add contact Details', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Address', SH_NAME ),
						   "param_name" => "address",
						   "description" => __('Enter Address to show in this Section.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Phone 1', SH_NAME ),
						   "param_name" => "ph1",
						   "description" => __('Enter Phone 1 to show in this Section.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Phone 2', SH_NAME ),
						   "param_name" => "ph2",
						   "description" => __('Enter Phone 2 to show in this Section.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Fax 1', SH_NAME ),
						   "param_name" => "fax1",
						   "description" => __('Enter Fax 1 to show in this Section.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Fax 2', SH_NAME ),
						   "param_name" => "fax2",
						   "description" => __('Enter Fax 2 to show in this Section.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Email 1', SH_NAME ),
						   "param_name" => "email1",
						   "description" => __('Enter Email 1 to show in this Section.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Email 2', SH_NAME ),
						   "param_name" => "email2",
						   "description" => __('Enter Email 2 to show in this Section.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Skype', SH_NAME ),
						   "param_name" => "skype",
						   "description" => __('Enter Skype ID to show in this Section.', SH_NAME )
						),
						
						array(
						   "type" => "attach_image",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Background Image', SH_NAME ),
						   "param_name" => "bg",
						   "description" => __('choose the background image.', SH_NAME )
						),
					)
				);

$sh_sc['sh_blog_posts_grid']	=	array(
					"name" => __("Blog Posts Grid", SH_NAME),
					"base" => "sh_blog_posts_grid",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('2 Column Blog Posts Grid.', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', SH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter Title of this Section.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', SH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of Posts to Show.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', SH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)sh_get_categories( array( 'hide_empty' => FALSE ) ) ),
						   "description" => __( 'Choose Category.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", SH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", SH_NAME),
						   "param_name" => "order",
						   'value' => array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
					)
				);
$sh_sc['sh_product_categories']	=	array(
					"name" => __("Product Categories", SH_NAME),
					"base" => "sh_product_categories",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show Product Categories.', SH_NAME),
					"params" => array(
						
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', SH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number Categories to Show.', SH_NAME )
						),
						
					)
				);

$sh_sc['sh_maintainance_shortcode']	=	array(
					"name" => __("Maintainance Mode", SH_NAME),
					"base" => "sh_maintainance_shortcode",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Maintainance Shortcode.', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Heading", SH_NAME),
						   "param_name" => "heading",
						   "description" => __("Enter Heading for Maintainance Page.", SH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Subscription URL", SH_NAME),
						   "param_name" => "subscribe",
						   "description" => __("Enter the url for subscription.", SH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Launch date", SH_NAME),
						   "param_name" => "date",
						   'description' => __('format: yyyy-mm-dd hh:mm  - Enter the launch date of the site', SH_NAME)
						),
						
						array(
						   "type" => "attach_image",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('LOGO', SH_NAME ),
						   "param_name" => "logo",
						   "description" => __('Upload Logo Image', SH_NAME )
						),
						array(
						   "type" => "attach_image",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('BG', SH_NAME ),
						   "param_name" => "bg",
						   "description" => __('Upload Background Image', SH_NAME )
						),
					)
				);


$sh_sc['sh_about_me'] = array(
			"name" => __("About Me", SH_NAME),
			"base" => "sh_about_me",
			"class" => "",
			"category" => __('Jollyall', SH_NAME),
			"icon" => 'fa-user' ,
			'description' => __('show the data of specific user', SH_NAME),
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Title", SH_NAME),
				   "param_name" => "title",
				   'value' => __('Abou Me', SH_NAME),
				   "description" => __("Enter the title", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("User Name", SH_NAME),
				   "param_name" => "name",
				   "description" => __("Enter the name of user", SH_NAME)
				),
				array(
				   "type" => "attach_image",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Image", SH_NAME),
				   "param_name" => "img",
				   "description" => __("Choose image", SH_NAME)
				),
				array(
				   "type" => "textarea",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Short Text", SH_NAME),
				   "param_name" => "content",
				   "description" => __("Enter content, you can use html tags", SH_NAME)
				),
				
				array(
				   "type" => "checkbox",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Services", SH_NAME),
				   "param_name" => "services",
				   'value' => array_flip( sh_get_posts_array('sh_services') ),
				   "description" => __("choose services", SH_NAME)
				),
				
				
			)
	    );
		
$sh_sc['sh_site_map']	=	array(
					"name" => __("Site Map", SH_NAME),
					"base" => "sh_site_map",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Show Site Map.', SH_NAME),
					"params" => array(
						array(
						   "type" => "checkbox",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Show Site Map?", SH_NAME),
						   "param_name" => "site_map",
						   'value' => array('Show Site Map' => 1 ),
						   "description" => __("Choose either to show Site Map or not.", SH_NAME)
						),
					)
				);


$sh_sc['sh_login']	=	array(
					"name" => __("Login Form", SH_NAME),
					"base" => "sh_login",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Login from Shortcode.', SH_NAME),
					"params" => array(
							array(
							   "type" => "textfield",
							   "holder" => "div",
							   "class" => "",
							   "heading" => __("Title", SH_NAME),
							   "param_name" => "title",
							   'value' => __('Login', SH_NAME),
							   "description" => __("Enter the title of the form, Leave empty if you don't want to show title", SH_NAME)
							),
							array(
							   "type" => "textfield",
							   "holder" => "div",
							   "class" => "",
							   "heading" => __("Redirect URL", SH_NAME),
							   "param_name" => "redirect",
							   'value' => home_url(),
							   "description" => __("Enter the redirect url", SH_NAME)
							),
							array(
							   "type" => "textfield",
							   "holder" => "div",
							   "class" => "",
							   "heading" => __("Button Text", SH_NAME),
							   "param_name" => "button_text",
							   'value' => '',
							   "description" => __("Enter the button text", SH_NAME)
							),
						)
					
				);
$sh_sc['sh_register_form'] = array(
			"name" => __("Register Form", SH_NAME),
			"base" => "sh_register_form",
			"class" => "",
			"category" => __('Jollyall', SH_NAME),
			"icon" => 'icon-wpb-layer-shape-text' ,
			'description' => __('allow users to register', SH_NAME),
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Title", SH_NAME),
				   "param_name" => "title",
				   'value' => __('Create an Account', SH_NAME),
				   "description" => __("Enter the title of the form, Leave empty if you don't want to show title", SH_NAME)
				),
				
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Button Text", SH_NAME),
				   "param_name" => "button_text",
				   'value' => '',
				   "description" => __("Enter the button text", SH_NAME)
				),
			)
	    );
$sh_sc['sh_buttons']	=	array(
					"name" => __("Buttons", SH_NAME),
					"base" => "sh_buttons",
					"class" => "",
					"category" => __('Jollyall', SH_NAME),
					"icon" => 'icon-wpb-layer-shape-text' ,
					'description' => __('Buttons for image hover', SH_NAME),
					"params" => array(
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Link 1 ", SH_NAME),
						   "param_name" => "link1",
						   "description" => __("Enter the link 1", SH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Link 2 ", SH_NAME),
						   "param_name" => "link2",
						   "description" => __("Enter the link 2", SH_NAME)
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Link 3 ", SH_NAME),
						   "param_name" => "link3",
						   "description" => __("Enter the link 3", SH_NAME)
						),
						
						
					)
				);
		
$sh_sc['sh_flexslider'] = array(
			"name" => __("Flexslider", SH_NAME),
			"base" => "sh_flexslider",
			"class" => "",
			"category" => __('Jollyall', SH_NAME),
			"icon" => 'icon-wpb-layer-shape-text' ,
			'description' => __('show images in flex slider', SH_NAME),
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Title", SH_NAME),
				   "param_name" => "title",
				   'value' => __('Flex Slider', SH_NAME),
				   "description" => __("Enter the title of the section, Leave empty if you don't want to show title", SH_NAME)
				),
				
				array(
				   "type" => "attach_images",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Images", SH_NAME),
				   "param_name" => "images",
				   'value' => '',
				   "description" => __("Choose images", SH_NAME)
				),
			)
	    );	
		
$sh_sc['sh_carousel'] = array(
			"name" => __("Bootstrap Carousel", SH_NAME),
			"base" => "sh_carousel",
			"class" => "",
			"category" => __('Jollyall', SH_NAME),
			"icon" => 'icon-wpb-layer-shape-text' ,
			'description' => __('show images in bootstrap carousel', SH_NAME),
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Title", SH_NAME),
				   "param_name" => "title",
				   'value' => __('Flex Slider', SH_NAME),
				   "description" => __("Enter the title of the section, Leave empty if you don't want to show title", SH_NAME)
				),
				
				array(
				   "type" => "attach_images",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Images", SH_NAME),
				   "param_name" => "images",
				   'value' => '',
				   "description" => __("Choose images", SH_NAME)
				),
			)
	    );

$sh_sc['sh_progress_bar'] = array(
			"name" => __("Progress Bar", SH_NAME),
			"base" => "sh_progress_bar",
			"class" => "",
			"category" => __('Jollyall', SH_NAME),
			"icon" => 'icon-wpb-layer-shape-text' ,
			'description' => __('show progress bar', SH_NAME),
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Title", SH_NAME),
				   "param_name" => "title",
				   'value' => __('Progress Bar', SH_NAME),
				   "description" => __("Enter the title of the section, Leave empty if you don't want to show title", SH_NAME)
				),
				array(
				   "type" => "textarea",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Text", SH_NAME),
				   "param_name" => "text",
				   "description" => __("Enter the text of the section, Leave empty if you don't want to show title", SH_NAME)
				),
				
				array(
				   "type" => "textarea",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Progress bars", SH_NAME),
				   "param_name" => "content",
				   'value' => '',
				   "description" => __("Enter one bar per line ( e.g Photoshop|70", SH_NAME)
				),
			)
	    );
$sh_sc['sh_progress_bar_2'] = array(
			"name" => __("Company Skills", SH_NAME),
			"base" => "sh_progress_bar_2",
			"class" => "",
			"category" => __('Jollyall', SH_NAME),
			"icon" => 'icon-wpb-layer-shape-text' ,
			'description' => __('show skills homepage 5', SH_NAME),
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Title", SH_NAME),
				   "param_name" => "title",
				   'value' => __('Progress Bar', SH_NAME),
				   "description" => __("Enter the title of the section, Leave empty if you don't want to show title", SH_NAME)
				),
				array(
				   "type" => "textarea",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Text", SH_NAME),
				   "param_name" => "text",
				   "description" => __("Enter the text of the section, Leave empty if you don't want to show title", SH_NAME)
				),
				
				array(
				   "type" => "textarea",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Progress bars", SH_NAME),
				   "param_name" => "content",
				   'value' => '',
				   "description" => __("Enter one bar per line ( e.g Photoshop|70", SH_NAME)
				),
				array(
				   "type" => "attach_image",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Background", SH_NAME),
				   "param_name" => "bg",
				   'value' => '',
				   "description" => __("choose background image", SH_NAME)
				),
			)
	    );


$sh_sc['sh_circle_chart'] = array(
			"name" => __("Circle Pie Charts", SH_NAME),
			"base" => "sh_circle_chart",
			"class" => "",
			"category" => __('Jollyall', SH_NAME),
			"icon" => 'icon-wpb-layer-shape-text' ,
			'description' => __('show circle charts', SH_NAME),
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Title", SH_NAME),
				   "param_name" => "title",
				   'value' => __('Circle Pie Charts', SH_NAME),
				   "description" => __("Enter the title of the section, Leave empty if you don't want to show title", SH_NAME)
				),
				array(
				   "type" => "textarea",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Text", SH_NAME),
				   "param_name" => "text",
				   "description" => __("Enter the text of the section, Leave empty if you don't want to show title", SH_NAME)
				),
				
				array(
				   "type" => "textarea",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Progress bars", SH_NAME),
				   "param_name" => "content",
				   'value' => '',
				   "description" => __("Enter one bar per line ( e.g Photoshop|70", SH_NAME)
				),
			)
	    );

$sh_sc['sh_pie_chart'] = array(
			"name" => __("Pie Charts", SH_NAME),
			"base" => "sh_pie_chart",
			"class" => "",
			"category" => __('Jollyall', SH_NAME),
			"icon" => 'icon-wpb-layer-shape-text' ,
			'description' => __('show circle charts', SH_NAME),
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Title", SH_NAME),
				   "param_name" => "title",
				   'value' => __('Circle Pie Charts', SH_NAME),
				   "description" => __("Enter the title of the section, Leave empty if you don't want to show title", SH_NAME)
				),
				array(
				   "type" => "textarea",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Text", SH_NAME),
				   "param_name" => "text",
				   "description" => __("Enter the text of the section, Leave empty if you don't want to show title", SH_NAME)
				),
				
				array(
				   "type" => "textarea",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Chart Info", SH_NAME),
				   "param_name" => "content",
				   'value' => '',
				   "description" => __("Enter one bar per line ( e.g Green|70.12|#48CA3B", SH_NAME)
				),
			)
	    );

$sh_sc['sh_sales_chart'] = array(
			"name" => __("Sales Charts", SH_NAME),
			"base" => "sh_sales_chart",
			"class" => "",
			"category" => __('Jollyall', SH_NAME),
			"icon" => 'icon-wpb-layer-shape-text' ,
			'description' => __('show sales charts', SH_NAME),
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Title", SH_NAME),
				   "param_name" => "title",
				   'value' => __('Sales Charts', SH_NAME),
				   "description" => __("Enter the title of the section, Leave empty if you don't want to show title", SH_NAME)
				),
				array(
				   "type" => "textarea",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Text", SH_NAME),
				   "param_name" => "text",
				   "description" => __("Enter the text of the section, Leave empty if you don't want to show title", SH_NAME)
				),
				
				array(
				   "type" => "textarea",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Chart Info", SH_NAME),
				   "param_name" => "content",
				   'value' => '',
				   "description" => __("Enter one bar per line ( e.g Green|70.12|#48CA3B", SH_NAME)
				),
			)
	    );


$sh_sc = apply_filters( '_sh_shortcodes_array', $sh_sc );
	
return $sh_sc;