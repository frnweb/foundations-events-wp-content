<?php

$theme_option = _WSH()->option() ; 
$team_slug = sh_set($theme_option , 'team_permalink' , 'sh_team') ;
$services_slug = sh_set($theme_option , 'services_permalink' , 'sh_services') ;
$speakers_slug = sh_set($theme_option , 'speakers_permalink' , 'conference-speakers') ;
//$projects_slug = sh_set($theme_option , 'projects_permalink' , 'sh_projects') ;
//$gallery_slug = sh_set($theme_option , 'gallery_permalink' , 'sh_gallery');
$testimonials_slug = sh_set($theme_option , 'testimonial_permalink' , 'sh_testimonials');

$options = array();


$options['sh_team'] = array(
								'labels' => array(__('Member', SH_NAME), __('Member', SH_NAME)),
								'slug' => $team_slug ,
								'label_args' => array('menu_name' => __('Teams', SH_NAME)),
								'supports' => array( 'title', 'editor' , 'thumbnail'),
								'label' => __('Member', SH_NAME),
								'args'=>array(
											'menu_icon'=>'dashicons-groups' )
											//'taxonomies'=>array('team_category')
							);
$options['sh_services'] = array(
								'labels' => array(__('Service', SH_NAME), __('Service', SH_NAME)),
								'slug' => $services_slug ,
								'label_args' => array('menu_name' => __('Services', SH_NAME)),
								'supports' => array( 'title' , 'editor' , 'thumbnail' ),
								'label' => __('Service', SH_NAME),
								'args'=>array(
										'menu_icon'=>'dashicons-slides' )
										//'taxonomies'=>array('services_category')
							);

$options['speakers'] = array(
								'labels' => array(__('Speaker', SH_NAME), __('Speaker', SH_NAME)),
								'slug' => $speakers_slug ,
								'label_args' => array('menu_name' => __('Speakers', SH_NAME)),
								'supports' => array( 'title' , 'editor' , 'thumbnail', 'excerpt', 'page-attributes' ),
								'label' => __('Speaker', SH_NAME),
								'args'=>array(
											'menu_icon'=>'dashicons-groups',
											'not_found' => __('No Speakers Found', SH_NAME),
											'not_found_in_trash' => __('No Speakers Found in Trash', SH_NAME),
											'menu_position' => 5,
											//'hierarchical' => true
											//'taxonomies'=>array('speakers_category')
											 )
							);
/*
$options['sh_projects'] = array(
								'labels' => array(__('Project', SH_NAME), __('Project', SH_NAME)),
								'slug' => $projects_slug ,
								'label_args' => array('menu_name' => __('Projects', SH_NAME)),
								'supports' => array( 'title' , 'editor' , 'thumbnail'),
								'label' => __('Project', SH_NAME),
								'args'=>array(
											'menu_icon'=>'dashicons-portfolio' , 
											'taxonomies'=>array('projects_category')
								)
							);
							
$options['sh_gallery'] = array(
								'labels' => array(__('Gallery', SH_NAME), __('Gallery', SH_NAME)),
								'slug' => $gallery_slug ,
								'label_args' => array('menu_name' => __('Gallery', SH_NAME)),
								'supports' => array( 'title' , 'thumbnail'),
								'label' => __('Gallery', SH_NAME),
								'args'=>array(
											'menu_icon'=>'dashicons-format-gallery' , 
											'taxonomies'=>array('gallery_category')
								)
							);
*/

$options['sh_testimonials'] = array(
								'labels' => array(__('Testimonial', SH_NAME), __('Testimonial', SH_NAME)),
								'slug' => $testimonials_slug ,
								'label_args' => array('menu_name' => __('Testimonials', SH_NAME)),
								'supports' => array( 'title' ,'editor', 'thumbnail'),
								'label' => __('Testimonial', SH_NAME),
								'args'=>array(
											'menu_icon'=>'dashicons-testimonial' , 
											'taxonomies'=>array('testimonials_category')
								)
							);
/*
$options['sh_faq'] = array(
								'labels' => array(__('FAQ', SH_NAME), __('FAQs', SH_NAME)),
								'slug' => 'sh_faq' ,
								'label_args' => array('menu_name' => __('FAQs', SH_NAME)),
								'supports' => array( 'title' ,'editor'),
								'label' => __('FAQs', SH_NAME),
								'args'=>array(
											'menu_icon'=>'dashicons-editor-help' , 
											'taxonomies'=>array('faq_category')
								)
							);
*/