<?php
/**
 * The Template for displaying all single products.
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

$meta = _WSH()->get_meta('_sh_layout_settings');

$layout = sh_set( $meta, 'layout', 'full' );
$sidebar = sh_set( $meta, 'sidebar', 'product-sidebar' );

$classes = ( !$layout || $layout == 'full' ) ? ' col-lg-12 col-md-12' : ' col-lg-9 col-md-9';

get_header( 'shop' ); ?>

	<?php get_template_part( 'includes/modules/header/header', 'single' ); ?>


<section class="white-wrapper clearfix">
	<div class="container">
		<div class="module clearfix">
    
    		<div class="row">

				<?php if( $layout == 'left' ): ?>
		
					<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="sidebar">
						<?php dynamic_sidebar( $sidebar ); ?>
					</div>
		
				<?php endif; ?>

				<div class="<?php echo $classes; ?>" id="post-content">
					
					<?php
						/**
						 * woocommerce_before_main_content hook
						 *
						 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
						 * @hooked woocommerce_breadcrumb - 20
						 */
						do_action( 'woocommerce_before_main_content' );
					?>
					<div class="shop-wrap-single">
					
						<?php while ( have_posts() ) : the_post(); ?>
				
							<?php wc_get_template_part( 'content', 'single-product' ); ?>
				
						<?php endwhile; // end of the loop. ?>
					
					</div>
					<?php
						/**
						 * woocommerce_after_main_content hook
						 *
						 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
						 */
						do_action( 'woocommerce_after_main_content' );
					?>

				</div>

				<?php if( $layout == 'right' ): ?>
		
					<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="sidebar">
						<?php dynamic_sidebar( $sidebar ); ?>
						
						<?php
							/**
							 * woocommerce_sidebar hook
							 *
							 * @hooked woocommerce_get_sidebar - 10
							 */
							do_action( 'woocommerce_sidebar' );
						?>
					</div>
		
				<?php endif; ?>
			</div>
		
		</div>
	</div>
	
</section>	

<?php get_footer( 'shop' ); ?>