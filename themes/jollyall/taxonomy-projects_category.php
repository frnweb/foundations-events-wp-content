<?php

get_header(); 

$meta = _WSH()->get_term_meta( '_sh_category_settings' );

_WSH()->page_settings = $meta; 

$layout = sh_set( $meta, 'layout', 'full' );
$sidebar = sh_set( $meta, 'sidebar', 'blog-sidebar' );
$view = sh_set( $meta, 'view', 'list' );

$classes = ( !$layout || $layout == 'full' ) ? ' col-lg-12 col-md-12' : ' col-lg-8 col-md-8';
$cols = 4;
$limit = 20;
$col_md = ( $cols == 4 ) ? 'col-lg-3 col-md-3 ' : 'col-lg-4 col-md-4 ';


?>

<?php get_template_part( 'includes/modules/header/header', 'archive' ); ?>

<section class="white-wrapper">
	
    <div class="container">
    
    	<?php if( $layout == 'left' ): ?>

            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" id="sidebar">
                <?php dynamic_sidebar( $sidebar ); ?>
            </div>

        <?php endif; ?>
		
		
        <div class="<?php echo $classes; ?> col-sm-12 col-xs-12" id="content">
        
			<div class="row">
				
				
				<div class="content-module">
					<div class="content fullwidth">
						<div class="masonry_wrapper margin-top clearfix">
							
							<?php while( have_posts() ): the_post(); 
							
								$first_last = _WSH()->first_last($wp_query->current_post, $cols); ?>
	
								<div class="<?php echo $col_md; ?>col-sm-6 col-xs-12<?php echo $first_last; ?>">
									<?php include( _WSH()->includes('includes/modules/projects_grid.php') ); ?>
								</div>
	
							<?php endwhile; ?>
							
							
						</div><!-- end masonry_wrapper -->           
	
						<div class="clearfix"></div>
																		
						<div class="pagination_wrapper text-center">
							<?php _the_pagination(); ?>
						</div>
								   
					</div><!-- end content -->
				</div><!-- end module --> 
					
				
                
            </div>
        
		</div>
        
        <?php if( $layout == 'right' ): ?>

            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" id="sidebar">        
                <?php dynamic_sidebar( $sidebar ); ?>
            </div>

        <?php endif; ?>
    
    </div>
</section>

<?php get_footer(); ?>