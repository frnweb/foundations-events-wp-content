<?php

get_header(); 

$settings  = sh_set(sh_set(get_post_meta(get_the_ID(), 'sh_page_meta', true) , 'sh_page_options') , 0);

$meta = _WSH()->get_meta('_sh_layout_settings');
_WSH()->page_settings = $meta;
$layout = sh_set( $meta, 'layout', 'full' );
$sidebar = sh_set( $meta, 'sidebar', 'product-sidebar' );

$classes = ( !$layout || $layout == 'full' ) ? ' col-lg-12 col-md-12' : ' col-lg-9 col-md-9';

/** Update the post views counter */
_WSH()->post_views( true );?>

<?php get_template_part( 'includes/modules/header/header', 'single' ); ?>

<section class="white-wrapper clearfix">
	<div class="container">
		<div class="module clearfix">
    
    		<div class="row">

				<?php if( $layout == 'left' ): ?>
		
					<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="sidebar">
						<?php dynamic_sidebar( $sidebar ); ?>
					</div>
		
				<?php endif; ?>
		
				<div class="<?php echo $classes; ?> col-sm-12 col-xs-12" id="post-content">
				
					<?php while( have_posts() ): the_post(); ?>
							
												
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                        	
							<?php get_template_part( 'blog' ); ?>
							<!-- end blog-item -->
							
							<?php wp_link_pages(array('before'=>'<div class="paginate-links">'.__('Pages: ', SH_NAME), 'after' => '</div>', 'link_before'=>'<span>', 'link_after'=>'</span>')); ?>
                        </article>
	
					<?php endwhile; ?>
					
					<div class="clearfix"></div>

<div style="padding-bottom: 50px;"><?php echo do_shortcode('[frn_social type="SHARETHIS" version="BUTTONS" ]'); ?></div>


					<?php comments_template(); ?>
					
				</div>
        
				<?php if( $layout == 'right' ): ?>
		
					<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="sidebar">        
						<?php dynamic_sidebar( $sidebar ); ?>
					</div>
		
				<?php endif; ?>
			</div>
    	</div>
    </div>
</section>

<?php echo do_shortcode( '[sh_brands_section num=10 order="ASC"]' ); ?>

<?php get_footer(); ?>