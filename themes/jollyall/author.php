<?php get_header(); 

$settings  = _WSH()->option(); 

$layout = sh_set( $settings, 'author_page_layout', 'full' );
$sidebar = sh_set( $settings, 'author_page_sidebar', 'blog-sidebar' );
$view = sh_set( $settings, 'author_page_view', 'list' );
_WSH()->page_settings = array('layout'=>$layout, 'view'=> $view, 'sidebar'=>$sidebar);

$classes = ( !$layout || $layout == 'full' ) ? ' col-lg-12 col-md-12' : ' col-lg-9 col-md-9';


?>

<?php get_template_part( 'includes/modules/header/header', 'archive' ); ?>

<section class="white-wrapper clearfix">
	<div class="container">
		<div class="module clearfix">
    
    		<div class="row">

				<?php if( $layout == 'left' ): ?>
		
					<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="sidebar">
						<?php dynamic_sidebar( $sidebar ); ?>
					</div>
		
				<?php endif; ?>
		
				<div class="<?php echo $classes; ?> col-sm-12 col-xs-12" id="post-content">
				
					<?php while( have_posts() ): the_post(); ?>
							
												
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                        	
							<?php if( $view == 2 ) get_template_part( 'blog', 'style' );
							else get_template_part( 'blog' ); ?>
							<!-- end blog-item -->
                        </article>
	
					<?php endwhile; ?>
						
					<div class="clearfix"></div>
					
					<?php _the_pagination(); ?>
					
				</div>
        
				<?php if( $layout == 'right' ): ?>
		
					<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="sidebar">        
						<?php dynamic_sidebar( $sidebar ); ?>
					</div>
		
				<?php endif; ?>
			</div>
    	</div>
    </div>
</section>

<?php echo do_shortcode( '[sh_brands_section num=10 order="ASC"]' ); ?>

<?php get_footer(); ?>


