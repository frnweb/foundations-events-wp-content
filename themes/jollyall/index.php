<?php

get_header(); 

if( $wp_query->is_posts_page ) {
	
	$meta = _WSH()->get_meta('_sh_layout_settings', get_queried_object()->ID);//printr($meta);
	
	$layout = sh_set( $meta, 'layout', 'right' );
	$sidebar = sh_set( $meta, 'sidebar', 'default-sidebar' );
	$view = sh_set( $meta, 'view', 'grid' );
	
} else {
	
	$settings  = _WSH()->option(); 
	
	$layout = sh_set( $settings, 'archive_page_layout', 'right' );
	$sidebar = sh_set( $settings, 'archive_page_sidebar', 'default-sidebar' );
	$view = sh_set( $settings, 'archive_page_view', 'list' );
	
}
$layout = sh_set( $_GET, 'layout' ) ? sh_set( $_GET, 'layout' ) : $layout;
$view = sh_set( $_GET, 'view' ) ? sh_set( $_GET, 'view' ) : $view;
$sidebar = ( $sidebar ) ? $sidebar : 'default-sidebar';
_WSH()->page_settings = array('layout'=>'right', 'view'=> $view, 'sidebar'=>$sidebar);

$classes = ( !$layout || $layout == 'full' ) ? ' col-lg-12 col-md-12' : ' col-lg-9 col-md-9';


?>

<?php get_template_part( 'includes/modules/header/header', 'archive' ); ?>

<section class="white-wrapper clearfix">
	<div class="container">
		<div class="module clearfix">
    
    		<div class="row">

				<?php if( $layout == 'left' ): ?>
		
					<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="sidebar">
						<?php dynamic_sidebar( $sidebar ); ?>
					</div>
		
				<?php endif; ?>
		
				<div class="<?php echo $classes; ?> col-sm-12 col-xs-12" id="post-content">
				
					<?php while( have_posts() ): the_post(); ?>
							
												
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                        	
							<?php if( $view == 2 ) get_template_part( 'blog', 'style' );
							else get_template_part( 'blog' ); ?>
							<!-- end blog-item -->
                        </article>
	
					<?php endwhile; ?>
						
					<div class="clearfix"></div>
					
					<?php _the_pagination(); ?>
					
				</div>
        
				<?php if( $layout == 'right' ): ?>
		
					<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="sidebar">        
						<?php dynamic_sidebar( $sidebar ); ?>
					</div>
		
				<?php endif; ?>
			</div>
    	</div>
    </div>
</section>

<?php echo do_shortcode( '[sh_brands_section num=10 order="ASC"]' ); ?>

<?php get_footer(); ?>

