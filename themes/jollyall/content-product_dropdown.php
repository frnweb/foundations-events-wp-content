<?php if( !function_exists( 'WC' ) ) return; 

$page_tmpl = _sh_get_page_by_template( 'tpl-compare.php' );
if( !$page_tmpl ) return;

$products = get_posts(array('post_type'=>'product', 'showposts'=>-1)); ?>

<div id="compare_products" style="display:none;">
	
    <form action="<?php echo get_permalink( $page_tmpl->ID ); ?>" method="get">
    	
        <input type="hidden" name="page_id" value="<?php echo $page_tmpl->ID; ?>">
    	<label for="product"><?php _e('Product: ', SH_NAME); ?></label>
        <select name="product_id" class="form-control">
        	<?php foreach( $products as $pro ): ?>
            	<?php $selected = ( get_the_id() == $pro->ID ) ? ' selected="selected"' : '' ?>
                <option value="<?php echo $pro->ID; ?>"<?php echo $selected; ?>><?php echo $pro->post_title; ?></option>
            <?php endforeach; ?>
        </select>
        <br>
        <label for="product-2"><?php _e('Compare With: ', SH_NAME); ?></label>
        <select name="product_id2" class="form-control">
        	<?php foreach( $products as $pro ): ?>
            	<option value="<?php echo $pro->ID; ?>"><?php echo $pro->post_title; ?></option>
            <?php endforeach; ?>
        </select>
        <br>
        <input type="submit" class="btn btn-primary pull-right" value="<?php _e('Compare', SH_NAME); ?>" />
    </form>
    
    
</div>