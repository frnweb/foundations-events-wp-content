
<?php $is_full = sh_set( _WSH()->recomm, 'fullwidth' ) ? true : false; 

if( !$is_full ) return; ?>

<div class="col-sm-7">
	<div class="widget">
		
		<h3><?php echo sh_set( _WSH()->recomm, 'rec_title', '<span>5 REASONS</span> WHY WE SUGGEST JOLLYANY?' ); ?></h3>
		
		<?php if( $recomm = sh_set( _WSH()->recomm, 'recomm' ) ): ?>
		
		<div id="accordion-second" class="clearfix">
			<div class="accordion" id="accordion2">
				
				<?php foreach( $recomm as $k => $recom ): ?>
				
				<div class="accordion-group">
					<div class="accordion-heading"> 
						<a class="accordion-toggle <?php echo ( $k == 0 ) ? 'active' : 'collapsed'; ?>" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne<?php echo $k; ?>"> 
							<em class="fa fa-<?php echo ( $k == 0 ) ? 'minus' : 'plus'; ?> icon-fixed-width"></em><?php echo sh_set( $recom, 'title' ); ?> 
						</a> 
					</div>
					
					<div id="collapseOne<?php echo $k; ?>" class="accordion-body<?php if( $k == 0 ) echo ' in'; ?> collapse">
						<div class="accordion-inner"> <?php echo sh_set( $recom, 'content' ); ?> </div>
					</div>
				</div>
				
				<?php endforeach; ?>
				
			</div>
			<!-- end accordion --> 
		</div>
		<!-- end accordion first --> 
		
		<?php endif; ?>
	</div>
	<!-- end widget --> 
</div>
<!-- end col-sm-6 -->