
<?php $meta = _WSH()->meta; 
$type = sh_set( $meta, 'project_type', 'img' ); ?>

<?php if( $type == 'img' ): ?>

	<div class="portfolio_item">
		<div class="entry">
			<?php //get_template_directory('project', $format ); ?>
			<?php the_post_thumbnail('820x548', array('class'=>'img-responsive', 'itemprop'=>'image') ); ?>
			<div class="magnifier">
				<div class="buttons"> 
					<a itemprop="thumbnailUrl" href="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>" class="sf" title="<?php the_title_attribute(); ?>" data-gal="prettyPhoto[product-gallery]" ><span class="fa fa-search"></span></a> 
					<?php if( $demo_url = sh_set( $meta, 'live_demo' ) ): ?>
					<a class="sg" rel="bookmark" href="<?php echo esc_url( $demo_url ); ?>" title="<?php _e('View Demo', SH_NAME); ?>"> <span class="fa fa-eye"></span> </a>
					<?php endif; ?>
				</div>
			</div>
			<!-- end magnifier --> 
		</div>
		<!-- entry --> 
	</div>
	<!-- end portfolio_item --> 
<?php endif; ?>

<?php if( $type == 'gal' ): ?>

		<?php $attachments = get_posts( array('post_type'=>'attachment', 'post_parent'=>get_the_id(), 'showposts'=>-1 ) ); ?>
		
		<?php if( $attachments ): ?>

			<div class="media-element">
			<div id="aboutslider" class="flexslider clearfix">
				<ul itemprop="images" class="slides">
					<?php foreach( $attachments as $att ): ?>
						<li>
							<?php echo wp_get_attachment_image( $att->ID, '880x488', '', array( 'class' => 'img-responsive' ) ); ?>
						</li>
					<?php endforeach; ?>
				</ul><!-- end slides -->
			</div><!-- end slider -->
			</div>

		<?php endif; ?>

<?php endif; ?>


<?php if( $type == 'audio' ): ?>

	<div itemprop="audio"><?php echo sh_set( $meta, 'audio' ); ?></div>
	
<?php endif; ?>


<?php if( $type == 'video' ): ?>

	<div class="portfolio_item">
		<div itemprop="video" class="entry">
			<?php echo sh_set( $meta, 'video' ); ?>
		</div><!-- end entry -->
	</div><!-- end portfolio_item -->

<?php endif; ?>