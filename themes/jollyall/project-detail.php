
<?php $is_full = sh_set( _WSH()->recomm, 'fullwidth' ) ? true : false; 
$meta = _WSH()->meta;?>

<?php if( !$is_full ): ?>
							
<div class="col-sm-7">
	<?php get_template_part('project', 'img' ); ?>
</div>
	
<?php endif; ?>

<div class="col-sm-5">
	<div class="title">
		<h1>
			<?php the_title(); ?>
		</h1>
	</div>
	<!-- end title -->
	<?php the_content(); ?>
	</p>
	<div class="product_details">
		<h3>
			<?php _e('Project Details', SH_NAME); ?>
		</h3>
		<ul>
			<?php if( $variable = sh_set( $meta, 'customer' ) ): ?>
			<li><strong>
				<?php _e('Customer:', SH_NAME); ?>
				</strong> <?php echo $variable; ?></li>
			<?php endif; ?>
			<?php if( $variable = sh_set( $meta, 'live_demo' ) ): ?>
			<li><strong>
				<?php _e('Live demo:', SH_NAME); ?>
				</strong> <a href="<?php echo esc_url( $variable ); ?>"><?php echo esc_url( $variable ); ?></a></li>
			<?php endif; ?>
			<li><strong>
				<?php _e('Category:', SH_NAME); ?>
				</strong>
				<?php the_terms(get_the_id(), 'projects_category', '', ', '); ?>
			</li>
			<?php if( $variable = sh_set( $meta, 'skills' ) ): ?>
			<li><strong>
				<?php _e('Skill:', SH_NAME); ?>
				</strong> <?php echo $variable; ?></li>
			<?php endif; ?>
			<li><strong>
				<?php _e('Date post:', SH_NAME); ?>
				</strong> <?php echo get_the_date(); ?></li>
		</ul>
	</div>
	<!-- end product_details --> 
</div>
<!-- end col-sm-6 -->