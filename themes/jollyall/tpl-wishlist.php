<?php /* Template Name: Wishlist */
get_header(); ?>

<?php global $current_user, $post;
get_currentuserinfo();

$meta = get_user_meta( $current_user->ID, '_ja_product_wishlist', true );//printr($meta);
$meta = array_filter( (array)$meta );
$meta_settings = _WSH()->get_meta('_sh_layout_settings');
$layout = sh_set( $meta_settings, 'layout', 'full' );
$sidebar = sh_set( $meta_settings, 'sidebar', 'page-sidebar' );
$classes = ( $layout == 'full' ) ? ' col-lg-12 col-md-12' : ' col-lg-9 col-md-9'; ?>

<?php get_template_part( 'includes/modules/header/header', 'single' );?>

<section class="blog-wrapper">
	
    <div class="container">
    	
		<div class="module clearfix">
		
			<?php if( $layout == 'left' ): ?>
	
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="sidebar">        
				
					<?php dynamic_sidebar( $sidebar ); ?>
				
				</div>
	
			<?php endif; ?>
        
			<div class="shop_wrapper<?php echo $classes; ?> col-sm-12 col-xs-12 woocommerce">
				
				<?php while( have_posts() ): the_post(); ?>
					<?php the_content();?>
				<?php endwhile;?>
				
				<?php if( is_user_logged_in() ): ?>
						   
					<div class="table-responsive" id="block-history">
						<table class="cart_table table table-bordered table-hover">
							
							<thead style="text-align:center;">
								<tr>
									<th><?php _e('PRODUCT NAME', SH_NAME); ?></th>
									<th><?php _e('Product URL', SH_NAME); ?></th>
									<th><?php _e('UNIT PRICE', SH_NAME); ?></th>
									<th><?php _e('ACTION', SH_NAME); ?></th>
								</tr>
							</thead>
							
							<?php
							foreach( (array)$meta as $met ): $post = get_post( $met );//printr($post);
								$product = new WC_Product( $post ); //printr(get_product( $post ))?>
								<tbody>
									<tr>
										<td>
											<?php echo get_the_post_thumbnail( $met, array(65, 65), array('class'=>'img-responsive alignleft', 'width'=>65) ); ?>
		
		
											<a class="cart_title" href="<?php echo get_permalink( $met ); ?>" title="<?php echo esc_attr(get_the_title( $met )); ?>"><?php echo get_the_title( $met ); ?></a>
											<?php include( 'woocommerce/single-product/rating.php' ); ?>
										</td>
										
										<td><a href="<?php echo get_permalink( $met ); ?>"><?php _e('View', SH_NAME); ?></a></td>
										<td><?php echo $product->get_price_html(); ?></td>
										<td class="wishlist_delete">
											<a class="remove" rel="product_del_wishlist" data-id="<?php echo $met; ?>" href="javascript:;"><?php _e('Delete', SH_NAME); ?></a>
										</td>
									</tr>
								</tbody>
							<?php endforeach; ?>
							
							
						</table>
					</div>
				<?php else: ?>
				
					<?php $acc_page = get_option('user_account_url'); ?>
					<h2><?php printf(__('To view this page sign in at <a href="%s" title="Account Page">Account Page</a>', SH_NAME), $acc_page); ?></h2>
				<?php endif; ?>
			
			</div>
        
			<?php if( $layout == 'right' ): ?>
	
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="sidebar">        
				
					<?php dynamic_sidebar( $sidebar ); ?>
				
				</div>
	
			<?php endif; ?>
			
		</div>
    
    </div>
        
</section>

<?php echo do_shortcode( '[sh_brands_section num=10 order="ASC"]' ); ?>


<?php get_footer(); ?>