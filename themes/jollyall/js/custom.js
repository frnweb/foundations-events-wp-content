(function($) {
 "use strict"

	// DM Top
	jQuery(window).scroll(function(){
		if (jQuery(this).scrollTop() > 1) {
			jQuery('.dmtop').css({bottom:"70px"});
		} else {
			jQuery('.dmtop').css({bottom:"-100px"});
		}
	});
	jQuery('.dmtop').click(function(){
		jQuery('html, body').animate({scrollTop: '0px'}, 800);
		return false;
	});
	
	// Page Preloader
	$(window).load(function() {
		$(".loader").delay(300).fadeOut();
		$(".animationload").delay(600).fadeOut("slow");
	});

	// Count
        function count($this){
        var current = parseInt($this.html(), 10);
        current = current + 1; /* Where 50 is increment */    
        $this.html(++current);
            if(current > $this.data('count')){
                $this.html($this.data('count'));
            } else {    
                setTimeout(function(){count($this)}, 50);
            }
        }            
        $(".stat-count").each(function() {
          $(this).data('count', parseInt($(this).html(), 10));
          $(this).html('0');
          count($(this));
        });
		
// TOOLTIP
    $('.social-icons, .bs-example-tooltips').tooltip({
      selector: "[data-toggle=tooltip]",
      container: "body"
    })

// RIGHT MENU
	$("#showbutton").click(function() {
		$('#slidediv').toggle('slide', { direction: 'right' }, 500);
	});
	$("#hidebutton").click(function(){
		$("#slidediv").hide( "slide", { direction: "right"  }, 500 );
	});

// HEADER MENU
   // Add slideup & fadein animation to dropdown
   $('.dropdown').on('show.bs.dropdown', function(e){
      var $dropdown = $(this).find('.dropdown-menu');
      var orig_margin_top = parseInt($dropdown.css('margin-top'));
      $dropdown.css({'margin-top': (orig_margin_top + 10) + 'px', opacity: 0}).animate({'margin-top': orig_margin_top + 'px', opacity: 1}, 250, function(){
         $(this).css({'margin-top':''});
      });
   });
   // Add slidedown & fadeout animation to dropdown
   $('.dropdown').on('hide.bs.dropdown', function(e){
      var $dropdown = $(this).find('.dropdown-menu');
      var orig_margin_top = parseInt($dropdown.css('margin-top'));
      $dropdown.css({'margin-top': orig_margin_top + 'px', opacity: 1, display: 'block'}).animate({'margin-top': (orig_margin_top + 10) + 'px', opacity: 0}, 250, function(){
         $(this).css({'margin-top':'', display:''});
      });
   });

	$("#header-container").affix({
		offset: {
			top: 100, 
			bottom: function () {
			return (this.bottom = $('#copyrights').outerHeight(true))
			}
		}
	})
	
// Parallax
	$(window).bind('body', function() {
		parallaxInit();
	});
	function parallaxInit() {
		$('.fullscreen').parallax("30%", 0.1);
		$('#two-parallax').parallax("30%", 0.1);
		$('#three-parallax').parallax("30%", 0.1);
		$('#four-parallax').parallax("30%", 0.4);
		$('#five-parallax').parallax("30%", 0.4);
		$('#six-parallax').parallax("30%", 0.4);
		$('#seven-parallax').parallax("30%", 0.4);
	}

	

	
// Accordion Toggle Items
   var iconOpen = 'fa fa-minus',
        iconClose = 'fa fa-plus';

    $(document).on('show.bs.collapse hide.bs.collapse', '.accordion', function (e) {
        var $target = $(e.target)
          $target.siblings('.accordion-heading')
          .find('em').toggleClass(iconOpen + ' ' + iconClose);
          if(e.type == 'show')
              $target.prev('.accordion-heading').find('.accordion-toggle').addClass('active');
          if(e.type == 'hide')
              $(this).find('.accordion-toggle').not($target).removeClass('active');
    });
	

	
})(jQuery);


jQuery(document).ready(function($) {
	"use strict";
	if( $('#rotateme').length ) {
		
		try {
		$("#rotateme").rotateme({ 
			text : ['UNBELIVABLE', 'Rocking', 'Fantastic'], 
			interval : 3000  // in miliseconds
		});	
		} catch (e) {
		}
	}
	
	if( $('.media-element > .flexslider').length ) {
		$('.media-element > .flexslider').flexslider({
			animation: "slide",
			controlNav: false,
			animationLoop: false,
			slideshow: false,
			directionNav: true 
		});
	}
});


if( jQuery('#testimonial-carousel').length )
{
	// Carousel
	(function($) {
	"use strict";
    $(document).ready(function() {
    var owl = $("#testimonial-carousel");
    owl.owlCarousel({
    items : 2, //10 items above 1000px browser width
    itemsDesktop : [1000,2], //5 items between 1000px and 901px
    itemsDesktopSmall : [900,2], // betweem 900px and 601px
    itemsTablet: [600,1], //2 items between 600 and 0
	navigation : false,
	pagination : true,
    });
     
    // Custom Navigation Events
    $(".next").click(function(){
    owl.trigger('owl.next');
    })
    $(".prev").click(function(){
    owl.trigger('owl.prev');
    })
    });
	})(jQuery);
}


if( jQuery('#clients_carousel').length ) {
	
	// Carousel
	(function($) {
	"use strict";
    $(document).ready(function() {
    var owl = $("#clients_carousel");
    owl.owlCarousel({
    items : 4, //10 items above 1000px browser width
    itemsDesktop : [1000,4], //5 items between 1000px and 901px
    itemsDesktopSmall : [900,2], // betweem 900px and 601px
    itemsTablet: [600,2], //2 items between 600 and 0
	navigation : false,
	pagination : false,
	autoPlay : true
    });  
    });
	})(jQuery);
	
}


if( jQuery('#services-carousel').length ) {
	// Carousel
	(function($) {
		"use strict";
		$(document).ready(function() {
			var owl = $("#services-carousel");
			owl.owlCarousel({
				items : 1, //10 items above 1000px browser width
				itemsDesktop : [1000,1], //5 items between 1000px and 901px
				itemsDesktopSmall : [900,1], // betweem 900px and 601px
				itemsTablet: [600,1], //2 items between 600 and 0
				navigation : false,
				autoPlay : true,
				pagination : true,
			});
		 
			// Custom Navigation Events
			$(".next").click(function(){
				owl.trigger('owl.next');
			})
			$(".prev").click(function(){
				owl.trigger('owl.prev');
			})
		});
	})(jQuery);
}


if( jQuery('#testimonials').length ) {
	
	(function($) {
	"use strict";
	$(window).load(function() {
	  // The slider being synced must be initialized first
	  $('#testimonials').flexslider({
		animation: "slide",
		controlNav: false,
		animationLoop: false,
		slideshow: false,
		directionNav: false,    
		asNavFor: '#slider'
	  });
	   
	  
	});
	})(jQuery);
}

jQuery(document).ready(function($) {
	if( $('#slider').length ) {
		$('#slider').flexslider({
		animation: "slide",
		controlNav: true,
		directionNav: false,    
		animationLoop: false,
		slideshow: false,
		sync: "#carousel"
	  });
	}
});

if( jQuery('#services_carousel').length ) {
		// Carousel
		(function($) {
			"use strict";
			$(document).ready(function() {
				var owl = $("#services_carousel");
				try {
				owl.owlCarousel({
					items : 3, //10 items above 1000px browser width
					itemsDesktop : [1000,3], //5 items between 1000px and 901px
					itemsDesktopSmall : [900,3], // betweem 900px and 601px
					itemsTablet: [600,1], //2 items between 600 and 0
					navigation : false,
					pagination : true,
					autoPlay : true
				}); 
				} catch( e ) {
					console.log( 'error' );
				}
			});
		})(jQuery);
}


jQuery(document).ready(function($) {
	$('a[data-gal]').each(function() {
		$(this).attr('rel', $(this).data('gal'));
	});  	
	$("a[data-gal^='prettyPhoto']").prettyPhoto({animationSpeed:'slow',slideshow:false,overlay_gallery: false,theme:'light_square',social_tools:false,deeplinking:false});
});




