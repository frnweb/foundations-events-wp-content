<?php get_header(); 

$settings  = sh_set(sh_set(get_post_meta(get_the_ID(), 'sh_page_meta', true) , 'sh_page_options') , 0);

$meta = _WSH()->get_meta('_sh_layout_settings');
$layout = sh_set( $meta, 'layout', 'full' );
$sidebar = sh_set( $meta, 'sidebar', 'product-sidebar' );

$classes = ( !$layout || $layout == 'full' ) ? ' col-lg-12 col-md-12' : ' col-lg-9 col-md-9';
?>

<?php get_template_part( 'includes/modules/header/header', 'archive' ); ?>

<section class="module">
	
    <div class="container">
    
    	<?php if( $layout == 'left' ): ?>

            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="sidebar">        
                <?php dynamic_sidebar( $sidebar ); ?>
            </div>

        <?php endif; ?>
		
        <div class="shop_wrapper<?php echo $classes; ?> col-sm-12 col-xs-12">
        	<?php echo do_shortcode('[frn_related html="404"]'); ?>
        </div>
        
        <?php if( $layout == 'right' ): ?>

            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" id="sidebar">        
                <?php dynamic_sidebar( $sidebar ); ?>
            </div>

        <?php endif; ?>
    
    </div>
</section>
<?php get_footer(); ?>