
<?php $meta = _WSH()->meta;
$is_full = sh_set( $meta, 'full' ) ? true : false; 

$clients = explode( "\n", sh_set( $meta, 'customer' ) );
$skills = explode( ",", sh_set( $meta, 'skills' ) );

if( $is_full ) return; ?>

							<div class="col-md-9" id="post-content">
                                <div class="single-portfolio blog-item">
                                    <div class="media-object portfolio-item-second">
                                        
										<?php get_template_part( 'project', 'img' ); ?>
										
                                    </div><!-- end media -->
                                    
                                    <div class="title">
                                        <h3 itemprop="name"><?php the_title(); ?></h3>
                                    </div><!-- end title -->
                                    
                                    <div itemprop="text" class="desc">
                                        <?php the_content(); ?>
                                    </div><!-- end desc -->
        
                                    <div class="post_bottom clearfix">
                                        <div class="next_prev text-center">
                                            <ul class="pager">
                                                <li class="previous">
													<?php previous_post_link( '%link',  __( '&larr; Older', SH_NAME ) ); ?>
												</li>
												<li class="next">
													<?php next_post_link( '%link',  __( 'Newer &rarr;', SH_NAME ) ); ?>
												</li>
                                            </ul>
                                        </div><!-- next_prev --> 
                                    </div><!-- post bottom -->
                    
                                </div><!-- end single-portfolio -->
                               
                            </div>
							
							<div class="col-md-3" id="sidebar">
                                
								<?php if( $clients ): ?>
									
									<div class="widget clearfix">
										<div class="title">
											<h3><?php _e('Clients', SH_NAME); ?></h3> 
										</div><!-- end title -->
										
										<ul class="portfolio_ul">
											
											<?php if( is_array( $clients ) ) foreach( $clients as $cl ): 
											
												$xp_cl = explode( '|', $cl );?>
												<li><a itemprop="citation" href="<?php echo esc_url( sh_set( $xp_cl, 1 ) ); ?>" title="<?php echo esc_attr( sh_set( $xp_cl, 0 ) ); ?>" target="_blank"><?php echo sh_set( $xp_cl, 0 ); ?></a></li>					
											
											<?php endforeach; ?>
											
										</ul>
									</div><!-- end widget -->
                                
								<?php endif; ?>
								
								
								<?php if( $skills ): ?>
                                
									<div class="widget clearfix">
										<div class="title">
											<h3><?php _e('Skills', SH_NAME); ?></h3> 
										</div><!-- end title -->
										<ul class="portfolio_ul">
											<?php if( is_array( $skills ) ) foreach( $skills as $skil ): ?>
											<li><a href="#"><?php echo $skil; ?></a></li>
											<?php endforeach; ?>
											
										</ul>
									</div><!-- end widget -->
								
								<?php endif; ?>
    
                                <div class="widget clearfix">
                                    <div class="title">
                                        <h3><?php _e('Project Details', SH_NAME); ?></h3> 
                                    </div><!-- end title -->
                                    <ul class="portfolio_ul">
                                        
										<?php if( $prop = sh_set( $meta, 'date' ) ): ?>
											<li itemprop="datePublished"><?php _e('Date', SH_NAME); ?> - <?php echo date( get_option( 'date_format' ), strtotime($prop) ); ?></li>
										<?php endif; ?>
										
										<?php if( $prop = sh_set( $meta, 'live_demo' ) ): ?>
                                        <li itemprop="publisher" itemtype="http://schema.org/Organization"><?php _e('Url', SH_NAME); ?> - <a itemprop="url" href="<?php echo esc_url( $prop ); ?>" title="<?php the_title_attribute(); ?>"><?php echo $prop; ?> </a></li>			
										<?php endif; ?>
                                    </ul>
                                </div><!-- end widget -->
                                                   
                            </div>