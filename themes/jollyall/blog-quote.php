<?php $page_settings = _WSH()->page_settings; ?>

		<div itemscope itemtype="http://schema.org/BlogPosting" class="blog-carousel">
			
			<?php $meta = _WSH()->get_meta(); 
			$video = sh_set( $meta, 'quote' );
			$prop = ( $video ) ? ' itemprop="quote"' : ' itemprop="image"' ;?>
			
				<div<?php echo $prop; ?> class="entry">
					
					<?php if( $video ): ?>
						
						<div class="quote-post">
							<blockquote>
								<p class="lead"><?php echo $video; ?></p>
								<small>
								Someone famous in
								<cite title="<?php the_title(); ?>"><?php the_author(); ?></cite>
								</small>
							</blockquote>
						</div><!-- end quote-post -->
						
					<?php else: ?>

						<?php the_post_thumbnail( 'full', array( 'class' => 'img-responsive' ) ); ?>
						
						<div class="magnifier">
							<div class="buttons">
								<a class="st" rel="bookmark" href="#" onClick="javascript:bookmark('<?php the_title(); ?>', '<?php the_permalink(); ?>')"><i class="fa fa-heart"></i></a>
							</div><!-- end buttons -->
						</div><!-- end magnifier -->
						
						<div class="post-type">
							<i class="fa fa-picture-o"></i>
						</div><!-- end pull-right -->
					
					<?php endif; ?>
										
				</div>
				<!-- end entry -->

			
			<div class="blog-carousel-header">
				
				<h1 itemprop="headline"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h1>
				
				<div class="blog-carousel-meta"> 
					
					<span itemprop="datePublished"><i class="fa fa-calendar"></i> <time datetime="<?php echo get_the_date( 'Y-m-d' ); ?>"><?php echo get_the_date(); ?></time></span> 
					<span itemprop="discussionUrl"><i class="fa fa-comment"></i> <a href="<?php the_permalink(); ?>#comments"><?php comments_number(); ?></a></span> 
					<span><i class="fa fa-eye"></i> <a href="#"><?php echo _WSH()->post_views(); ?> <?php _e('Views', SH_NAME); ?></a></span> 
					
					<?php if( sh_set( $page_settings, 'view' ) == 'list' ): ?>
						<span itemprop="author">
							<i class="fa fa-user"></i>
							<a rel="author" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>" title="<?php the_author_meta( 'display_name' ); ?>"><?php the_author(); ?></a>
						</span> 
					<?php endif; ?>
					
				</div>
				<!-- end blog-carousel-meta --> 
			</div>
			<!-- end blog-carousel-header -->
			
			<div itemprop="articleBody" class="blog-carousel-desc">
				<?php if( !is_single() ) {
					$excerpt_more = ( sh_set( $page_settings, 'view' ) == 'grid' ) ? 30 : 50;
					echo _sh_trim( get_the_excerpt(), $excerpt_more );
				}
				else the_content(); ?>
			</div>
			<!-- end blog-carousel-desc --> 
		</div>
		<!-- end blog-carousel --> 
